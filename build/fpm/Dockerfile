FROM php:8.2.20-fpm-bookworm as base

ARG USER_ID=1000
ARG GROUP_ID=1000
RUN cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime

RUN groupadd -g $GROUP_ID mgmaps && \
    useradd -b / -m -g ${GROUP_ID} -u ${USER_ID} -s /bin/bash mgmaps
USER mgmaps

WORKDIR /html
RUN mkdir -p /html/vendor /html/bootstrap/cache /mgmaps/.composer

EXPOSE 9000

FROM base as composer

ARG COMPOSER_VERSION
USER root

WORKDIR /mgmaps

ADD ./build/fpm/installComposer.sh /usr/bin/installComposer
RUN chmod go+x /usr/bin/installComposer
RUN installComposer && rm /usr/bin/installComposer

USER mgmaps
WORKDIR /html
VOLUME ["/mgmaps/.composer"]
CMD ["composer", "install"]

FROM composer as modules

USER root

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install --no-install-recommends -y \
    libzip-dev \
    liblz4-dev \
    libzstd-dev \
    libicu-dev \
    && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-configure intl
RUN docker-php-ext-install pcntl zip intl
RUN yes 'y' | pecl install xdebug-3.3.2 igbinary-3.2.15 msgpack-2.2.0 redis-6.0.2
RUN docker-php-ext-enable msgpack igbinary redis
RUN docker-php-ext-enable xdebug

USER mgmaps

FROM modules as development

USER root

# Add common configurations development & production
ADD ./build/fpm/configuration/common/*.ini /usr/local/etc/php/conf.d/
ADD ./build/fpm/configuration/common/*.conf /usr/local/etc/php-fpm.d/

# Add development only configuration
ADD ./build/fpm/configuration/development/*.ini /usr/local/etc/php/conf.d/
ADD ./build/fpm/configuration/development/*.conf /usr/local/etc/php-fpm.d/

ADD ./build/fpm/entrypoint.sh /entrypoint.sh
RUN chmod ug+x /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]

CMD ["php-fpm", "--nodaemonize"]

USER mgmaps

FROM development as production

USER root

# Disable xdebug extension and enable opcache
RUN rm $PHP_INI_DIR/conf.d/xdebug.ini && \
    docker-php-ext-install --ini-name=opcache.ini opcache

ADD ./build/fpm/configuration/production/*.ini /usr/local/etc/php/conf.d/

USER mgmaps

ADD --chown=${USER}:${GROUP} ./app /html