<?php

use App\Http\Controllers\DataController;
use App\Http\Controllers\ImageProxy;
use App\Http\Controllers\LivenessController;
use App\Http\Controllers\RoutingController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SuggestController;
use App\Http\Controllers\Wikidata;
use App\Http\Middleware\Localization;
use GeoIp2\Database\Reader;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

Route::get("health-check/scheduler", [LivenessController::class, "scheduler"]);
Route::get("ip", function (Request $request) {
    $headers = $request->headers;
    dd([$request->ip(), $headers]);
});
Route::get('/last-modified', function (Request $request) {
    //sleep(5);
    // List all Files in app, public, resources and routes
    $appIterator = new RecursiveDirectoryIterator(app_path(), RecursiveDirectoryIterator::SKIP_DOTS);
    $publicIterator = new RecursiveDirectoryIterator(public_path(), RecursiveDirectoryIterator::SKIP_DOTS);
    $resourceIterator = new RecursiveDirectoryIterator(resource_path(), RecursiveDirectoryIterator::SKIP_DOTS);
    $routesIterator = new RecursiveDirectoryIterator(base_path('routes'), RecursiveDirectoryIterator::SKIP_DOTS);
    $iterator = new AppendIterator();
    $iterator->append(new RecursiveIteratorIterator($appIterator));
    $iterator->append(new RecursiveIteratorIterator($publicIterator));
    $iterator->append(new RecursiveIteratorIterator($resourceIterator));
    $iterator->append(new RecursiveIteratorIterator($routesIterator));

    $files = [];
    $maxTime = 0;
    foreach ($iterator as $fileInfo) {
        $mtime = filemtime($fileInfo->getPathname());
        if ($maxTime < $mtime) {
            $maxTime = $mtime;
        }
        $files[] = $mtime;
    }

    $response = Response::make("", 200)
        ->header("last-modified", $maxTime * 1000);

    return $response;
});

Route::get('files/list', function (Request $request) {
    $imageIterator = new RecursiveDirectoryIterator(public_path('img'), RecursiveDirectoryIterator::SKIP_DOTS);
    $jsIterator = new RecursiveDirectoryIterator(public_path('js'), RecursiveDirectoryIterator::SKIP_DOTS);
    $cssIterator = new RecursiveDirectoryIterator(public_path('css'), RecursiveDirectoryIterator::SKIP_DOTS);
    $fontsIterator = new RecursiveDirectoryIterator(public_path('fonts'), RecursiveDirectoryIterator::SKIP_DOTS);
    $iterator = new AppendIterator();
    $iterator->append(new RecursiveIteratorIterator($imageIterator));
    $iterator->append(new RecursiveIteratorIterator($jsIterator));
    $iterator->append(new RecursiveIteratorIterator($cssIterator));
    $iterator->append(new RecursiveIteratorIterator($fontsIterator));

    $files = [];
    $delimiter = "MetaGerMaps/public";
    foreach ($iterator as $file) {
        $filePath = $file->getPathname();
        $fileHash = hash_file('sha256', $filePath);
        $pathname = substr($filePath, stripos($filePath, $delimiter) + strlen($delimiter));
        $files[$pathname] = $fileHash;
    }
    $files["/favicon.ico"] = hash_file('sha256', public_path("favicon.ico"));
    $files["/"] = hash_file('sha256', resource_path('views/map.blade.php'));

    return Response::make(json_encode(["offline-data" => $files]), 200)
        ->header("Content-Type", "application/json");
});

Route::prefix("{locale}")
    ->where(['locale' => implode("|", Localization::SUPPORTED_LOCALES)])->group(function ($locale) {
        Route::get("image", [ImageProxy::class, "proxy"])->name("imageproxy");

        Route::get('fakegps/{bbox}', function ($locale, $bbox) {
            return view('map')->with('css', [mix('css/general.css'), mix('css/mapSearch.css'), mix('css/routing.css')])
                ->with("module", "fakegps")
                ->with("bbox", $bbox);
        });


        Route::get("search/{query}", function ($locale, $query) {
            return view('map')->with('css', [mix('css/general.css'), mix('css/mapSearch.css'), mix('css/routing.css')])
                ->with("query", $query);
        });
        Route::get("wikidata/{id}", [Wikidata::class, "get"])->name("wikidata");

        Route::get("frame/{osm_ids}", function ($locale, $osm_ids) {
            return view("map-frame")->with('css', [mix('css/general.css'), mix('css/mapSearch.css'), mix('css/routing.css')])
                ->with("query", $osm_ids);
        });

        Route::get("reverse/{reverse_long},{reverse_lat},{reverse_zoom}/{bbox}", function ($locale, $reverse_long, $reverse_lat, $reverse_zoom, $bbox) {
            return view('map')->with('css', [mix('css/general.css'), mix('css/mapSearch.css'), mix('css/routing.css')])
                ->with("bbox", $bbox)
                ->with("reverse_lon", $reverse_long)
                ->with("reverse_lat", $reverse_lat)
                ->with('reverse_zoom', $reverse_zoom);
        });

        Route::group(["prefix" => "data"], function () {
            Route::get("tiles", [DataController::class, "tiles"]);
            Route::get("search", [DataController::class, "search"]);
            Route::get("routing", [DataController::class, "routing"]);
        });

        Route::post("suggest/api", [SuggestController::class, "api"]);
        Route::get('search', [SearchController::class, 'search']);

        Route::get("lookup", [SearchController::class, "lookup"]);
        Route::group(['prefix' => 'reverse'], function () {
            Route::get("/", [SearchController::class, "reverse"]);
            Route::get('{lon}/{lat}', function ($locale, $lon, $lat) {
                $link = config("maps.nominatim.host") . "/reverse.php?format=json&lat=$lat&lon=$lon&zoom=18&extratags=1&addressdetails=1&namedetails=1";
                $resContent = file_get_contents($link);
                $response = Response::make($resContent, 200);
                $response->header("Content-Type", "application/json");
                return $response;
            });
        });
        Route::get('hilfe', function () {
            return view('help');
        });

        Route::group(['prefix' => 'route'], function () {
            Route::get("/{vehicle}/{waypoints}/{bbox}", [RoutingController::class, 'map']);
            Route::post("calc", [RoutingController::class, 'calc']);
            Route::get('preview/{vehicle}/{points}', [RoutingController::class, 'routingOverviewGeoJson']);
            Route::get('find/{vehicle}/{points}/{startBearing?}', [RoutingController::class, 'routingGeoJson']);
            Route::get('match/{vehicle}/{points}/{timestamp}/{radiuses}', [RoutingController::class, 'match']);
            Route::get('start/{vehicle}/{points?}', function ($locale, $vehicle, $points = "") {
                $waypoints = "[]";
                if ($points !== "") {
                    // Let's Convert
                    $points = explode(";", $points);
                    $waypoints = "[";
                    foreach ($points as $index => $value) {
                        if ($value === "gps") {
                            $waypoints .= '["gps"],';
                        } else {
                            $pos = explode(',', $value);
                            $waypoints .= "[" . $pos[0] . "," . $pos[1] . "],";
                        }
                    }
                    $waypoints = rtrim($waypoints, ",");
                    $waypoints .= "]";
                }
                return view('map')->with('css', [mix('css/general.css'), mix('css/mapSearch.css'), mix('css/routing.css')])
                    ->with("vars", ["waypoints" => $waypoints, 'vehicle' => $vehicle]);
            });
            Route::get('search/{search}', function ($locale, $search) {
                $url = "https://tiles.metager.de/nominatim/search.php?q=" . urlencode($search) . "&limit=5&polygon_geojson=0&format=json&dedupe=1&extratags=1&addressdetails=1&namedetails=1";
                $content = file_get_contents($url);
                $response = Response::make($content, 200);
                $response->header('Content-Type', 'application/json');
                return $response;
            });
            Route::get('{vehicle}/{points}', [RoutingController::class, 'calcRoute']);
        });

        // Route that serves all maps with all possible parameters except the routing one
        Route::get('/{param1?}/{param2?}/{param3?}', function (Request $request, $locale, $param1 = null, $param2 = null, $param3 = null) {
            $query = null;
            $viewbox = null;
            $bbox = null;
            $geoip = null;

            if ($param1 !== null) {
                // Map called with parameters
                // Three cases:
                //  1. only one parameter => Map called with bbox
                //  2. two parameters => Map called with search query and bbox
                //  3. three parameters => Map called with search query, viewbox and bbox
                if ($param2 === null) {
                    $bbox = $param1;
                } else if ($param3 === null) {
                    $query = $param1;
                    $bbox = $param2;
                } else {
                    $query = $param1;
                    $viewbox = $param2;
                    $bbox = $param3;
                }
            } else {
                // Try to get an initial position from ip
                try {
                    $cityDBReader = new Reader(storage_path("app/public/GeoLite2-City.mmd"));
                    $record = $cityDBReader->city($request->ip());
                    if (!$record->traits->isAnonymous) {
                        $geoip = [
                            "longitude" => $record->location->longitude,
                            "latitude" => $record->location->latitude,
                            "accuracy" => $record->location->accuracyRadius
                        ];
                    }
                } catch (Exception $e) {
                }

            }

            $headers = [
                "Cache-Control" => "public, max-age=3600, must-revalidate"
            ];

            $response = response(view("map", [
                "css" => [
                    mix('css/general.css'),
                    mix('css/mapSearch.css'),
                    mix('css/routing.css')
                ],
                "query" => $query,
                "viewbox" => $viewbox,
                "bbox" => $bbox,
                "geoip" => $geoip
            ]), 200, $headers);

            $etag = md5($response->getContent() . json_encode($headers));
            $response->setEtag($etag);

            // Check if the browser has a cached version of this page
            if ($request->hasHeader("If-None-Match")) {
                $user_etags = explode(",", $request->header("If-None-Match"));
                foreach ($user_etags as $user_etag) {
                    $user_etag = str_replace("\"", "", $user_etag);
                    if ($user_etag === $etag) {
                        // Users Browser has a cached version. Just use that
                        return response("", 304, $headers);
                    }
                }
            }

            return $response;
        });
    });

Route::get("{uri?}", function (Request $request, string $uri = "") {
    $uri = preg_replace("/^map\/?/", "", $uri);
    $locale = App::currentLocale();
    foreach (Localization::SUPPORTED_LOCALES as $supported_locale) {
        if (stripos($uri, $supported_locale) === 0) {
            abort(404);
            return;
        }
    }
    $url = url("/" . $locale . "/" . $uri);
    return redirect($url);
})->where('uri', '.*');