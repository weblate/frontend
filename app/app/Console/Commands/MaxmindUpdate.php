<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use ZipArchive;

class MaxmindUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maxmind:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the maxmind database if necessary';


    private $enabled, $update_url, $update_interval_days, $account_id, $license_key, $target_file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->enabled = config("maps.maxmind.enabled");
        $this->update_url = config("maps.maxmind.update_url");
        $this->update_interval_days = intval(config("maps.maxmind.update_interval_days"));
        $this->account_id = config("maps.maxmind.account_id");
        $this->license_key = config("maps.maxmind.license_key");
        $this->target_file = storage_path("app/public/GeoLite2-City.mmd");
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->enabled) {
            $this->error("Maxmind updates are disabled by configuration.");
            return 1;
        }

        try {
            $this->updateDatabase();
        } catch (\ErrorException $e) {
            $this->error($e->getMessage());
            return 1;
        }
        return 0;
    }

    private function shouldUpdate(): bool
    {
        if (!file_exists($this->target_file))
            return true;
        $fmtime = Carbon::createFromTimestamp(filemtime($this->target_file), "UTC");
        $next_update = $fmtime->clone()->addDays($this->update_interval_days);
        return $next_update->isPast();
    }

    private function updateDatabase()
    {
        if (!$this->shouldUpdate())
            return;
        $this->info(now("Europe/Berlin")->format("[Y-m-d H:i:s]") . " Updating Maxmind Database");
        $tmp_file_name_compressed = tempnam(sys_get_temp_dir(), "maxmind_") . ".tar.gz";
        try {
            $tmp_file = fopen($tmp_file_name_compressed, "w+");
            $ch = curl_init($this->update_url);
            curl_setopt_array($ch, [
                CURLOPT_TIMEOUT => 120,
                CURLOPT_FILE => $tmp_file,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_USERNAME => $this->account_id,
                CURLOPT_PASSWORD => $this->license_key
            ]);

            curl_exec($ch);

            $curl_error = curl_error($ch);
            if (!empty($curl_error)) {
                throw new \ErrorException($curl_error);
            }

            curl_close($ch);
            fclose($tmp_file);

            $tar_archive = new \PharData($tmp_file_name_compressed);
            foreach ($tar_archive as $file) {
                if ($file->isDir()) {
                    $dir = new \PharData($file->getPathname());
                    foreach ($dir as $sub_file) {
                        $this->info($sub_file->getFileName());
                        if ($sub_file->getFileName() == "GeoLite2-City.mmdb") {
                            $this->info("found");
                            $tar_fh = $sub_file->openFile("r");
                            $output_fh = fopen($this->target_file, "w");
                            while (!$tar_fh->eof()) {
                                fwrite($output_fh, $tar_fh->fread(1024));
                            }
                            fclose($output_fh);
                        }
                    }
                }
            }
        } finally {
            unlink($tmp_file_name_compressed);
        }
    }
}
