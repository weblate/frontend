<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Exception;
use GeoIp2\Database\Reader;
use Illuminate\Http\Request;
use Locale;
use ResourceBundle;
use Symfony\Component\HttpFoundation\Response;
use URL;

class Localization
{
    const SUPPORTED_LOCALES = ["de", "en"];

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $locale = "en";
        if ($request->filled("locale") && in_array($request->input("locale"), self::SUPPORTED_LOCALES)) {
            $locale = $request->input("locale");
        } else if ($request->cookie("locale", null) !== null && in_array($request->cookies("locale"), self::SUPPORTED_LOCALES)) {
            $locale = $request->cookie("locale");
        } else if ($request->segment(1) !== null && in_array($request->segment(1), self::SUPPORTED_LOCALES)) {
            $locale = $request->segment(1);
        } else {
            $preferred_language = $request->getPreferredLanguage(self::SUPPORTED_LOCALES);
            if ($preferred_language != null && $preferred_language !== "en") {
                $locale = $preferred_language;
            } else {
                try {
                    $cityDBReader = new Reader(storage_path("app/public/GeoLite2-City.mmd"));
                    $record = $cityDBReader->city($request->ip());
                    if (!$record->traits->isAnonymous) {
                        $country = $record->registeredCountry->isoCode;
                        $lang = $this->getLanguage($country);
                        if (in_array($lang, self::SUPPORTED_LOCALES)) {
                            $locale = $lang;
                        }
                    }
                } catch (Exception $e) {
                }
            }
        }

        App::setLocale($locale);
        URL::defaults(["locale" => $locale]);

        return $next($request);
    }

    public static function getUnlocalizedUri(): string
    {
        $url = \Illuminate\Support\Facades\Request::getRequestUri();
        foreach (self::SUPPORTED_LOCALES as $supported_locale) {
            if (stripos($url, "/$supported_locale") === 0) {
                return str_ireplace("/$supported_locale", "", $url);
            }
        }
        return $url;
    }

    private function getLanguage(string $country): string
    {
        $subtags = ResourceBundle::create('likelySubtags', 'ICUDATA', false);
        $country = Locale::canonicalize('und_' . $country);
        if (($country[0] ?? null) === '_') {
            $country = 'und' . $country;
        }
        $locale = $subtags->get($country) ?: $subtags->get('und');
        return Locale::getPrimaryLanguage($locale . "tes");
    }
}
