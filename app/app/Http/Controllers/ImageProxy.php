<?php

namespace App\Http\Controllers;

use App\Models\CachedFetcher;
use Illuminate\Http\Request;

class ImageProxy extends Controller
{
    function proxy(Request $request)
    {
        if (!$request->hasValidSignature()) {
            abort(401);
        }
        $url = $request->query("url");
        $response = CachedFetcher::FETCH($url, "imageproxy", [CURLOPT_FOLLOWLOCATION => true]);
        return response($response["body"], $response["http_code"], $response["headers"]);
    }
}
