<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class SuggestController extends Controller
{
    public function api(Request $request)
    {
        $photon_host = config("maps.photon.host");
        $url = $photon_host . "/api";

        $parameters = $request->all();
        $parameters["lang"] = App::currentLocale();

        $ch = curl_init($url . "?" . http_build_query($parameters));
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => true
        ]);

        $response = curl_exec($ch);

        // Retudn headers seperatly from the Response Body
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = explode(PHP_EOL, substr($response, 0, $header_size));
        $response_headers = [];
        foreach ($headers as $index => $header) {
            if (empty(trim($header)) || $index === 0)
                continue;
            $header_array = explode(":", $header);
            if (trim($header_array[0]) === "Server")
                continue;
            $response_headers[trim($header_array[0])] = trim($header_array[1]);
        }
        $body = substr($response, $header_size);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return response($body, $response_code, $response_headers);
    }
}
