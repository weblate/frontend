<?php

namespace App\Http\Controllers;

use App;
use Cache;
use Illuminate\Http\Request;
use Log;
use Response;

class RoutingController extends Controller
{

    public function map(Request $request, $locale, $vehicle, $waypoints, $bbox)
    {
        return view('map')->with('css', [mix('css/general.css'), mix('css/mapSearch.css'), mix('css/routing.css')])
            ->with("bbox", $bbox)
            ->with("vehicle", $vehicle)
            ->with("waypoints", $waypoints)
            ->with("navigation_active", $request->has("navigation"));
    }

    public function calc(Request $request)
    {
        $payload = json_decode($request->getContent());
        $payload->locale = App::getLocale();
        $url = config("maps.routingserver.host") . "/route";

        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
            ],
            CURLOPT_POSTFIELDS => json_Encode($payload),
        ]);

        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return response($response, $status, ["Content-Type" => "application/json"]);

    }
    public function calcRoute(Request $request, $locale, $vehicle, $points)
    {
        $debug = false;
        if ($request->has("debug")) {
            $debug = true;
        }
        return view('map')
            ->with('boundings', 'false')
            ->with('getPosition', 'false')
            ->with('vars', ['vehicle' => $vehicle, 'points' => $points, 'debug' => $debug])
            ->with('css', [elixir('/css/routing.css')])
            ->with('scripts', [elixir('js/routing.js')])
            ->with('getPosition', 'true');
    }

    public function routingOverviewGeoJson($locale, $vehicle, $points)
    {
        // This is the function to calculate the Route from $from to $to with the given vehicle
        $port = 0;
        switch ($vehicle) {
            case "bicycle":
                $port = 5001;
                break;
            case "car":
                $port = 5002;
                break;
            default:
                $port = 5000;
        }
        $url = "http://94.130.13.157:$port/route/v1/$vehicle/$points?steps=true&geometries=geojson";

        $cacheHash = md5($url);
        if (Cache::has($cacheHash)) {
            $result = Cache::get($cacheHash);
        } else {
            $result = "";
            try {
                $result = file_get_contents($url);
                $result = base64_encode($result);
                Cache::put($cacheHash, $result, now()->addMinutes(60));
            } catch (\ErrorException $e) {
                Log::error("Konnte den Routing Server nicht erreichen");
            }
        }

        $result = base64_decode($result);

        $result = json_decode($result, true);

        $duration = $result["routes"][0]["duration"];
        $distance = $result["routes"][0]["distance"];

        // If there is no geometry we will return an empty geojson Object
        $geojson = "{coordinates: [],type: \"LineString\"}";
        if ($result["code"] === "Ok" && sizeof($result["routes"]) >= 1) {
            $geojson = $result["routes"][0]["geometry"];
        }

        $result = ['duration' => $duration, 'distance' => $distance, 'geojson' => $geojson];
        $result = json_encode($result);

        $response = Response::make($result, 200);
        $response->header('Content-Type', 'application/json');

        return $response;
    }

    public function routingGeoJson($locale, $vehicle, $points, $bearingStartPoint = "")
    {
        // This is the function to calculate the Route from $from to $to with the given vehicle
        $port = 0;

        switch ($vehicle) {
            case "bicycle":
                $vehicle = "bike";
            case "car":
            case "foot":
                break;
            default:
                abort(404);
        }

        $routing_host = config("maps.routingserver.host");

        // Points are formatted for OSRM... we need to fix them
        $points = explode(";", $points);
        foreach ($points as $index => $point) {
            $point_values = explode(",", $point);
            $points[$index] = $point_values[1] . "," . $point_values[0];
        }
        unset($point_values, $point);

        $query = [
            "profile" => $vehicle,
            "locale" => "de",
            "calc_points" => "true",
            "points_encoded" => "false",
            "algorithm" => "alternative_route",
            "alternative_route.max_paths" => 3,
        ];

        # Maybe we need to add Bearings:
        if ($bearingStartPoint) {
            $query["heading"] = $bearingStartPoint;
        }

        $url = $routing_host . "/route?" . http_build_query($query);

        foreach ($points as $point) {
            $url .= "&point=" . $point;
        }


        $cacheHash = md5($url);
        $status = 0;
        $result = "";
        if (Cache::has($cacheHash) && 1 == 0) {
            $result = Cache::get($cacheHash);
            $status = 200;
        } else {
            try {
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($status === 200) {
                    Cache::put($cacheHash, $result, now()->addMinutes(60));
                }
            } catch (\ErrorException $e) {
                Log::error("Konnte den Routing Server nicht erreichen");
            }
        }


        if ($status == 200) {
            $response = Response::make($result, $status);
            $response->header('Content-Type', 'application/json');

            return $response;
        } else {

            abort(404);
        }

    }

    public function match($locale, $vehicle, $points, $timestamp, $radiuses)
    {
        // This is the function to calculate the Route from $from to $to with the given vehicle
        $port = 0;
        switch ($vehicle) {
            case "bicycle":
                $port = 5001;
                break;
            case "car":
                $port = 5002;
                break;
            default:
                $port = 5000;
        }
        $url = 'http://94.130.13.157:' . $port . '/match/v1/' . $vehicle . '/' . $points . '?steps=true&geometries=geojson&timestamps=' . $timestamp . '&radiuses=' . $radiuses;

        // make request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        $response = Response::make($output, 200);
        $response->header('Content-Type', 'application/json');

        return $response;
    }

}
