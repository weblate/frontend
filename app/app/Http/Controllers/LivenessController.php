<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class LivenessController extends Controller
{
    const LIVENESS_SCHEDULER_KEY = "liveness:scheduler";

    public function scheduler(Request $request)
    {
        $live = filter_var(Redis::get(self::LIVENESS_SCHEDULER_KEY), FILTER_VALIDATE_BOOLEAN);
        if ($live === true) {
            return response("");
        } else {
            return response("", 400);
        }
    }
}
