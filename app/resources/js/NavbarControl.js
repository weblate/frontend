export class NavbarControl {
  onAdd(map) {
    this._map = map;
    let container = document.createElement("label");
    container.classList.add("nav-opener-label");
    container.setAttribute("for", "nav-opener");
    container.textContent = "≡";
    this.container = container;
    return container;
  }
  onRemove(map) {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}
