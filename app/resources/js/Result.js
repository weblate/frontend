import { LngLat, LngLatBounds } from "maplibre-gl";
import { gpsManager } from "./GpsManager";
import { bbox, circle, distance, point } from "@turf/turf";
import { Wikidata } from "./Wikidata";
import opening_hours from "opening_hours"
import { formatDistance, formatDuration } from "./utils";

export class Result {
  load_data = null;

  loaded = false;
  error_message = null;
  error_code = null;

  /** @type {string} */
  id = null;
  /** @type {string} */
  display_name = null;
  /** @type {string} */
  address_details = null;
  /** @type {opening_hours} */
  oh = null;
  /** @type {string} */
  phone = null;
  /** @type {string} */
  email = null;
  /** @type {string} */
  website = null;
  /** @type {string} */
  tag = null;
  /** @type {LngLat} */
  location = null;
  /** @type {number} */
  heading = null;
  /** @type {LngLatBounds} */
  bbox = null;
  geojson = null;

  /** @type {Wikidata} */
  wikidata = null;

  static get ERROR_GEOLOCATION_PERMISSION_DENIED() {
    return 1;
  }
  static get ERROR_GEOLOCATION_INTERNAL_ERROR() {
    return 2;
  }
  static get ERROR_GEOLOCATION_TIMEOUT() {
    return 3;
  }

  static get SOURCE_NOMINATIM() {
    return "nominatim";
  }
  static get SOURCE_PHOTON() {
    return "photon";
  }
  static get SOURCE_LOOKUP() {
    return "lookup";
  }
  static get SOURCE_REVERSE() {
    return "reverse";
  }
  static get SOURCE_GPS() {
    return "gps";
  }

  source = Result.SOURCE_NOMINATIM;
  route = null;

  dom_element = null;
  marker = null;

  actions_enabled = false;

  geolocation_watch_id = null;
  navigation_available = false;

  constructor(
    load_data,
    enable_actions = false,
    source = Result.SOURCE_NOMINATIM,
    route = null
  ) {
    this.load_data = load_data;
    this.actions_enabled = enable_actions;
    this.source = source;
    this.route = route;

    this.#createDomElement();
  }

  async load() {
    return new Promise((resolve, reject) => {
      switch (this.source) {
        case Result.SOURCE_NOMINATIM:
          this.parseNominatim();
          this.#createDomElement();
          this.dom_element.dispatchEvent(new Event("load"));
          return resolve();
        case Result.SOURCE_GPS:
          return this.#getFromGPS().then(() => {
            this.loaded = true;
            this.#createDomElement();
            this.dom_element.dispatchEvent(new Event("load"));
            return resolve();
          });
        default:
          return reject(`Cannot parse ${this.source}`);
      }
    });
  }

  parseNominatim() {
    // location & bbox
    this.location = new LngLat(
      parseFloat(this.load_data.lon),
      parseFloat(this.load_data.lat)
    );
    if (this.load_data.geojson && this.load_data.geojson.type != "Point") {
      this.bbox = bbox(this.load_data.geojson);
    } else {
      this.bbox = bbox(
        circle(point([this.location.lng, this.location.lat]), 0.1)
      );
    }

    this.geojson = this.load_data.geojson;

    // Display name
    let display_name = "";
    if (this.load_data.class && this.load_data.address[this.load_data.class]) {
      display_name = this.load_data.address[this.load_data.class];
    } else if (
      this.load_data.type &&
      this.load_data.address[this.load_data.type]
    ) {
      display_name = this.load_data.address[this.load_data.type];
    } else if (this.load_data.display_name) {
      if (this.load_data.display_name.indexOf(",") > -1) {
        display_name = this.load_data.display_name.substr(
          0,
          this.load_data.display_name.indexOf(",")
        );
      } else {
        display_name = this.load_data.display_name;
      }
    } else {
      display_name = "";
    }
    this.display_name = display_name;

    // Address details
    let address_details = "";
    if (this.load_data && this.load_data.type) {
      if (this.load_data.type != "street") {
        let street_added = false;
        if (this.load_data.address.street) {
          address_details += this.load_data.address.street;
          street_added = true;
        } else if (this.load_data.address.road) {
          address_details += this.load_data.address.road;
          street_added = true;
        }
        if (this.load_data.address.housenumber) {
          address_details += ` ${this.load_data.address.housenumber}`;
        } else if (this.load_data.address.house_number) {
          address_details += ` ${this.load_data.address.house_number}`;
        }
        if (street_added) {
          address_details += ", ";
        }
      }
      if (this.load_data.address.postcode) {
        address_details += ` ${this.load_data.address.postcode}, `;
      }
      if (this.load_data.address.city) {
        address_details += ` ${this.load_data.address.city}, `;
      } else if (this.load_data.address.town) {
        address_details += ` ${this.load_data.address.town}, `;
      }

      if (this.load_data.address.county) {
        address_details += ` ${this.load_data.address.county}, `;
      } else if (this.load_data.address.suburb) {
        address_details += ` ${this.load_data.address.suburb}, `;
      }
      if (this.load_data.address.state) {
        address_details += ` ${this.load_data.address.state}, `;
      }
      if (this.load_data.address.country) {
        address_details += ` ${this.load_data.address.country}, `;
      }
    }
    address_details = address_details.replace(/, $/, "");
    this.address_details = address_details;

    // Tag
    if (this.load_data.type) {
      this.tag = this.load_data.type;
    } else if (this.load_data.class) {
      this.tag = this.load_data.class;
    } else {
      this.tag = "";
    }
    this.loaded = true;

    // ID
    this.id = this.load_data.osm_type[0].toUpperCase() + this.load_data.osm_id;

    // Wikidata
    this.wikidata = this.load_data.extratags.wikidata;

    // opening hours
    if (this.load_data.extratags.opening_hours) {
      this.oh = new opening_hours(this.load_data.extratags.opening_hours, this.load_data);
    }
    // Contact informations
    if (this.load_data.extratags.phone) {
      this.phone = this.load_data.extratags.phone;
    }
    if (this.load_data.extratags["contact:phone"]) {
      this.phone = this.load_data.extratags["contact:phone"];
    }
    if (this.load_data.extratags.email) {
      this.email = this.load_data.extratags.email;
    }
    if (this.load_data.extratags["contact:email"]) {
      this.email = this.load_data.extratags["contact:email"];
    }
    if (this.load_data.extratags.website) {
      this.website = this.load_data.extratags.website;
    }
    if (this.load_data.extratags["contact:website"]) {
      this.website = this.load_data.extratags["contact:website"];
    }

  }

  toString() {
    switch (this.source) {
      case "gps":
        return `gps:${this.location.lng},${this.location.lat}`;
      default:
        return this.id;
    }
  }

  async #getFromGPS() {
    let maximumAge = 500;
    let lastUpdate = Date.now() - maximumAge;
    let min_accuracy = 50; // Min 50m for Navigation
    let min_frequency = 2 * maximumAge; // Minimum Update frequency for navigation

    let frequency_high_enough = false;
    let accuracy_high_enough = false;

    return new Promise((resolve, reject) => {
      let position_callback = (position) => {
        let first_position = this.location == null;
        let coords = position.coords;
        let circle = turf.circle(
          [coords.longitude, coords.latitude],
          coords.accuracy / 1000
        );
        this.display_name = "Eigene Position";
        this.location = new LngLat(coords.longitude, coords.latitude);
        this.bbox = bbox(circle);
        this.geojson = point([this.location.lng, this.location.lat]).geometry;
        this.gps = {
          timestamp: position.timestamp,
          coords: {
            latitude: coords.latitude,
            longitude: coords.longitude,
            altitude: coords.altitude,
            accuracy: coords.accuracy,
            altitudeAccuracy: coords.altitudeAccuracy,
            heading: coords.heading,
            speed: coords.speed,
          },
        };

        if (first_position) {
          return resolve();
        } else {
          if (coords.accuracy <= min_accuracy) {
            accuracy_high_enough = true;
          }
          if (position.timestamp - lastUpdate < min_frequency) {
            frequency_high_enough = true;
          }

          if (
            !this.navigation_available &&
            accuracy_high_enough &&
            frequency_high_enough
          ) {
            this.navigation_available = true;
            this.dom_element.dispatchEvent(new Event("navigation_available"));
          }
          if (position.timestamp - lastUpdate >= maximumAge) {
            lastUpdate = position.timestamp;
            this.dom_element.dispatchEvent(
              new CustomEvent("update", { detail: position })
            );
          }
        }
      };

      let error_callback = (error) => {
        let first_position = this.location == null;
        console.log(error);
        gpsManager.clearWatch(this.geolocation_watch_id);
        this.geolocation_watch_id = null;
        if (this.data == null) {
          switch (error.code) {
            case 1:
              this.error_message = "Zugriff auf Standort abgelehnt";
              break;
            case 2:
              this.error_message =
                "Interner Fehler beim ermitteln der Position";
              break;
            case 3:
              this.error_message =
                "Standort konnte nicht schnell genug ermittelt werden";
              break;
          }
          this.error_code = error.code;
        } else {
          this.dom_element.dispatchEvent(
            new CustomEvent("update", { detail: null })
          );
        }
        if (first_position) {
          return resolve();
        }
      };

      let options = {
        timeout: 10000,
        enableHighAccuracy: false,
        maximumAge: 60000,
      };

      gpsManager.getCurrentPosition(
        (position) => {
          position_callback(position);
          // Try to upgrade to a high accuracy position
          options.enableHighAccuracy = true;
          options.maximumAge = 500;
          this.geolocation_watch_id = gpsManager.watchPosition(
            position_callback.bind(this),
            error_callback,
            options
          );
        },
        error_callback,
        options
      );
    });
  }

  #createDomElement() {
    // Outer result container
    if (!this.dom_element) {
      this.dom_element = document.createElement("div");
      this.dom_element.classList.add("result");
    } else {
      this.dom_element.textContent = "";
    }

    // Create loading content if data is not loaded yet
    if (!this.loaded && !this.error_message) {
      let img = document.createElement("img");
      img.src = "/img/ajax-loader.gif";
      this.dom_element.appendChild(img);
      let text = document.createElement("div");
      text.textContent = "Lade Wegpunkt...";
      this.dom_element.appendChild(text);
      return;
    }

    // Reorder waypoints
    let reorder_container = document.createElement("div");
    reorder_container.classList.add("reorder-waypoints");
    let up_container = document.createElement("a");
    up_container.classList.add("up");
    up_container.title = "Wegpunkt nach oben verschieben";
    up_container.innerHTML = "&#x25B2;";
    reorder_container.appendChild(up_container);
    let down_container = document.createElement("a");
    down_container.classList.add("down");
    down_container.title = "Wegpunkt nach unten verschieben";
    down_container.innerHTML = "&#x25BC;";
    reorder_container.appendChild(down_container);
    this.dom_element.appendChild(reorder_container);

    // Inner Marker
    let marker_container = document.createElement("div");
    marker_container.classList.add("result-marker");
    this.dom_element.appendChild(marker_container);

    let removeWaypoint = document.createElement("a");
    removeWaypoint.classList.add("remove-waypoint");
    removeWaypoint.title = "Wegpunkt löschen";
    removeWaypoint.innerHTML = "&#128465;";
    this.dom_element.appendChild(removeWaypoint);

    if (this.error_message) {
      let warning_container = document.createElement("div");
      warning_container.classList.add("warning-container");
      this.dom_element.appendChild(warning_container);
      let warning_sign = document.createElement("div");
      warning_sign.innerHTML = "&#9888;";
      warning_sign.classList.add("warning-sign");
      warning_container.appendChild(warning_sign);
      let message = document.createElement("div");
      message.classList.add("message");
      message.textContent = this.error_message;
      warning_container.appendChild(message);
    } else {
      let description = document.createElement("div");
      description.classList.add("description");
      let address = document.createElement("div");
      address.classList.add("address");
      // Inner display name
      let display_name_container = document.createElement("div");
      display_name_container.classList.add("display-name");
      display_name_container.textContent = this.display_name;
      address.appendChild(display_name_container);
      // Tag
      let tag_container = document.createElement("div");
      tag_container.classList.add("tag");
      tag_container.textContent = this.tag;
      address.appendChild(tag_container);
      // AdditionalInfos
      let address_details_container = document.createElement("div");
      address_details_container.classList.add("details");
      address_details_container.textContent = this.address_details;
      address.appendChild(address_details_container);
      description.appendChild(address);

      // Opening hours
      let opening_hours_overview_container = document.createElement("div");
      opening_hours_overview_container.classList.add("opening-hours");
      if (this.oh) {
        let state = this.oh.getState();
        let state_class = state.toString();
        let state_label = state ? "geöffnet" : "geschlossen";


        let current_container = document.createElement("div");
        current_container.classList.add("current");
        current_container.innerHTML = "Aktuell <span class=\"state\" data-open=\"" + state_class + "\">" + state_label + "</span>";
        opening_hours_overview_container.appendChild(current_container);

        let next_state_label = state ? "Schließt" : "Öffnet";
        let state_change = this.oh.getNextChange();
        if (state_change != undefined) {
          let state_change_ms = (new Date()).getTime() - state_change.getTime();
          let next_container = document.createElement("div");
          next_container.classList.add("next");
          next_container.innerHTML = next_state_label + " in <span data-open=\"" + !state_class + "\" title=\"" + state_change.toLocaleDateString(undefined, { weekday: "short", year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit", second: "2-digit" }) + "\">" + formatDuration(state_change_ms) + "</span>";
          opening_hours_overview_container.appendChild(next_container);
        }
      }

      if (gpsManager.last_location != null && this.location != null) {
        // We can calculate the distance
        let distance_container = document.createElement("div");
        distance_container.classList.add("distance");

        let user_point = point([gpsManager.last_location.coords.longitude, gpsManager.last_location.coords.latitude]);
        let result_point = point([this.location.lng, this.location.lat]);
        let result_distance = distance(user_point, result_point) * 1000;
        distance_container.textContent = formatDistance(result_distance) + " entfernt";

        opening_hours_overview_container.appendChild(distance_container);
      }
      description.appendChild(opening_hours_overview_container);

      // Contact information
      let contact_container = document.createElement("div");
      contact_container.classList.add("contact");
      if (this.phone) {
        let phone_container = document.createElement("div");
        phone_container.classList.add("contact-info", "phone");

        let phone_icon = document.createElement("i");
        phone_icon.innerHTML = "&#128241;";
        phone_container.appendChild(phone_icon);

        let phone_link = document.createElement("a");
        phone_link.target = "_blank";
        phone_link.href = "tel:" + this.phone;
        phone_link.textContent = this.phone;
        phone_container.appendChild(phone_link);

        contact_container.appendChild(phone_container);
      }
      if (this.email) {
        let email_container = document.createElement("div");
        email_container.classList.add("contact-info", "email");

        let email_icon = document.createElement("i");
        email_icon.innerHTML = "&#128231;";
        email_container.appendChild(email_icon);

        let email_link = document.createElement("a");
        email_link.target = "_blank";
        email_link.href = "mailto:" + this.email;
        email_link.textContent = this.email;
        email_container.appendChild(email_link);

        contact_container.appendChild(email_container);
      }
      if (this.website) {
        let website_container = document.createElement("div");
        website_container.classList.add("contact-info", "website");

        let website_icon = document.createElement("i");
        website_icon.innerHTML = "&#127760;";
        website_container.appendChild(website_icon);

        let website_link = document.createElement("a");
        website_link.target = "_blank";
        website_link.rel = "noopener";
        website_link.href = this.website;
        website_link.textContent = this.website;
        website_container.appendChild(website_link);

        contact_container.appendChild(website_container);
      }
      description.appendChild(contact_container);

      this.dom_element.appendChild(description);
    }



    let actions_container = document.createElement("div");
    actions_container.classList.add("actions");

    let details_button = document.createElement("a");
    details_button.classList.add("open-details");
    if (!this.wikidata || typeof this.wikidata == "object") {
      details_button.classList.add("hidden");
    }
    details_button.textContent = "Details";
    actions_container.appendChild(details_button);

    let addWaypointButton = document.createElement("a");
    if (!this.actions_enabled) {
      addWaypointButton.classList.add("hidden");
    }
    addWaypointButton.classList.add("button", "add-waypoint");
    actions_container.appendChild(addWaypointButton);

    this.dom_element.appendChild(actions_container);

    if (
      this.wikidata &&
      typeof this.wikidata == "object" &&
      this.actions_enabled
    ) {
      let details = document.createElement("div");
      details.classList.add("details");

      if (this.wikidata.description) {
        let description = document.createElement("div");
        description.classList.add("wikidata-description");
        description.textContent = this.wikidata.description;
        details.appendChild(description);
      }

      // Add Flag image
      if (this.wikidata.images.flag) {
        let flag_container = Wikidata.GENERATE_IMAGE_CONTAINER(
          this.wikidata.images.flag.image,
          this.wikidata.images.flag.attribution
        );
        details.appendChild(flag_container);
      }

      let data_table = document.createElement("ul");

      if (this.wikidata.website) {
        let website_container = document.createElement("li");
        website_container.classList.add("wikidata-website");

        let website_label = document.createElement("div");
        website_label.classList.add("wikidata-label");
        website_label.textContent = "Webseite";
        website_container.appendChild(website_label);

        let website_value = document.createElement("a");
        website_value.classList.add("wikidata-value");
        website_value.href = this.wikidata.website;
        website_value.target = "_blank";
        website_value.rel = "noopener";
        website_value.textContent = this.wikidata.website;
        website_container.appendChild(website_value);

        data_table.appendChild(website_container);
      }

      if (this.wikidata.population) {
        let population_container = document.createElement("li");
        population_container.classList.add("wikidata-population");

        let population_label = document.createElement("div");
        population_label.classList.add("wikidata-label");
        population_label.textContent = "Einwohner";
        population_container.appendChild(population_label);

        let population_value = document.createElement("div");
        population_value.classList.add("wikidata-value");
        population_value.textContent =
          this.wikidata.population.toLocaleString(undefined);
        population_container.appendChild(population_value);

        data_table.appendChild(population_container);
      }

      if (this.wikidata.area) {
        let area_container = document.createElement("li");
        area_container.classList.add("wikidata-area");

        let area_label = document.createElement("div");
        area_label.classList.add("wikidata-label");
        area_label.textContent = "Fläche";
        area_container.appendChild(area_label);

        let area_value = document.createElement("div");
        area_value.classList.add("wikidata-value");
        area_value.textContent =
          this.wikidata.area.toLocaleString(undefined) + " km²";
        area_container.appendChild(area_value);

        data_table.appendChild(area_container);
      }

      details.appendChild(data_table);

      // Colage
      if (this.wikidata.images.collage) {
        let collage_container = Wikidata.GENERATE_IMAGE_CONTAINER(
          this.wikidata.images.collage.image,
          this.wikidata.images.collage.attribution
        );
        details.appendChild(collage_container);
      }
      // Aerial
      if (this.wikidata.images.aerial) {
        let aerial_container = Wikidata.GENERATE_IMAGE_CONTAINER(
          this.wikidata.images.aerial.image,
          this.wikidata.images.aerial.attribution
        );
        details.appendChild(aerial_container);
      }

      this.dom_element.appendChild(details);
    }
  }

  addMarkerToDom(rank) {
    if (!this.loaded && !this.error_message) return;
    if (!this.location) return;
    let chr = String.fromCharCode(65 + rank);
    let huerotate = Math.round(rank * 12.5) + 30;
    let el = document.createElement("span");
    el.id = `result-marker-result-${chr}`;
    el.classList.add("marker");
    el.textContent = chr;
    if (huerotate) {
      el.style.filter = `hue-rotate(${huerotate}deg)`;
    }

    this.dom_element.querySelector(".result-marker").innerHTML = "";
    this.dom_element.querySelector(".result-marker").appendChild(el);

    return el;
  }
}
