import { androidConnector } from "./AndroidConnector";

export class GpsManager {
  last_location = null;

  #location_possible = null;
  #location_gps_available = null;

  #success_callback = null;
  #error_callback = null;

  last_location_storage_key = "latestlocation";
  last_location_storage_timeout = null;

  #default_options = {
    enableHighAccuracy: true,
    timeout: 10000,
    maximumAge: 60000,
  };

  constructor() {
    // Try to load initial location from localstorage
    try {
      let location = localStorage.getItem(this.last_location_storage_key);
      if (location != null) location = JSON.parse(location);
      if (location.hasOwnProperty("coords")) {
        this.last_location = location;
      }
    } catch (e) {
      // Local storage not available
    }
  }

  #watch_callback = (e) => {
    if (e.key == "location") {
      let new_location = e.newValue;
      if (new_location) new_location = JSON.parse(new_location);
      if (new_location.coords.heading == "NaN")
        new_location.coords.heading = NaN;
      this.last_location = new_location;
      this.#success_callback(new_location);
    }
  };

  watchPosition(success_callback, error_callback, options) {
    let location = null;
    try {
      location = localStorage.getItem("location");
      if (location) location = JSON.parse(location);
    } catch (e) {
      // Local storage not available
    }

    if (location && Date.now() - location.timestamp < 300000) {
      if (location.coords.heading == "NaN") location.coords.heading = NaN;
      this.#success_callback = success_callback;
      this.#error_callback = error_callback;
      window.addEventListener("storage", this.#watch_callback);
      this.setLastLocation(location);
      success_callback(location);
      return "localstorage";
    } else if (androidConnector.is_connected) {
      // Android App is connected. We can use the devices Geolocation
      let timeout = options.timeout && options.timeout != Infinity ? setTimeout(() => {
        console.error("timeout");
        error_callback({
          code: 3,
          message: "Geolocation Request timed out."
        });
        timeout = null;
      }, options.timeout) : null;
      let watch_id = androidConnector.watchPosition(
        (location) => {
          if (timeout != null) clearTimeout(timeout);
          else if (options.timeout && options.timeout != Infinity) {
            this.clearWatch(watch_id);
            return; // In case a location is received after the timeout
          }
          this.setLastLocation(location);
          success_callback(location);
        },
        error_callback,
        { ...this.#default_options, ...options }
      );
      return watch_id;
    } else {
      if (!navigator.geolocation) {
        error_callback({
          code: 2,
          message: "Geolocation is not available in this browser",
        });
        return;
      }
      return navigator.geolocation.watchPosition(
        (location) => {
          this.setLastLocation(location);
          success_callback(location);
        },
        error_callback,
        { ...this.#default_options, ...options }
      );
    }
  }

  clearWatch(id) {
    if (id == "localstorage") {
      window.removeEventListener("storage", this.#watch_callback);
    } else if (androidConnector.is_connected) {
      androidConnector.clearWatch(id);
    } else if (navigator.geolocation) {
      navigator.geolocation.clearWatch(id);
    }
  }

  getCurrentPosition(success_callback, error_callback, options) {
    // Return cached position if newer than 1s
    let cache_timeout = options.maximumAge ? options.maximumAge : 1000;
    if (
      this.last_location &&
      Date.now() - this.last_location.timestamp <= cache_timeout
    ) {
      success_callback(this.last_location);
      return;
    }

    let location = null;
    try {
      location = localStorage.getItem("location");
      if (location) location = JSON.parse(location);
    } catch (e) {
      // Local Storage not available
    }

    if (location && Date.now() - location.timestamp < 300000) {
      // Valid location from localstorage
      if (location.coords.heading == "NaN") location.coords.heading = NaN;
      this.setLastLocation(location);
      success_callback(location);
    } else if (androidConnector.is_connected) {
      // Android App is connected. We can use the devices Geolocation
      let timeout = options.timeout && options.timeout != Infinity ? setTimeout(() => {
        error_callback({
          code: 3,
          message: "Geolocation Request timed out."
        });
        timeout = null;
      }, options.timeout) : null;
      androidConnector.getCurrentPosition(
        (location) => {
          if (timeout != null) clearTimeout(timeout);
          else if (options.timeout && options.timeout != Infinity) return; // In case a location is received after the timeout
          this.setLastLocation(location);
          success_callback(location);
        },
        error_callback,
        options
      );
    } else {
      if (!navigator.geolocation) {
        error_callback({
          code: 2,
          message: "Geolocation is not available in this browser",
        });
        return;
      }
      navigator.geolocation.getCurrentPosition(
        (location) => {
          this.setLastLocation(location);
          success_callback(location);
        },
        error_callback,
        { ...this.#default_options, ...options }
      );
    }
  }

  /**
   * Setter for the last location
   * it will store the latest location into local storage
   * so it can be reused at a later point
   */
  setLastLocation(new_location) {
    this.last_location = new_location;
    if (this.last_location_storage_timeout == null) {
      setTimeout(() => {
        try {
          let storedLocation = {
            timestamp: this.last_location.timestamp,
            coords: {}
          };
          for (let key in this.last_location.coords) {
            storedLocation.coords[key] = this.last_location.coords[key];
          }

          localStorage.setItem(this.last_location_storage_key, JSON.stringify(storedLocation));
        } catch (e) {
          // Local storage not available
        }
      }, 5000);
    }
  }

  async isGeolocationPossible() {
    if (this.#location_possible != null) return this.#location_possible;
    return new Promise((resolve) => {
      this.getCurrentPosition(
        (location) => {
          this.#location_possible = true;
          if (location.coords.accuracy < 150) {
            this.#location_gps_available = true;
          } else {
            this.#location_gps_available = false;
          }
          resolve(true);
        },
        (error) => {
          this.#location_possible = false;
          this.#location_gps_available = false;
          resolve(false);
        },
        {
          timeout: 10000,
          enableHighAccuracy: true,
        }
      );
    });
  }

  /**
   * 
   * @returns {Promise<boolean>}
   */
  async hasPermissions() {
    if (androidConnector.is_connected) {
      return androidConnector.hasGeolocationPermission();
    } else {
      return navigator.permissions.query({ name: "geolocation" }).then(status => {
        if (status.state != "granted") return false;
        return true;
      });
    }
  }

  async isGPSAvailable() {
    if (this.#location_gps_available != null)
      return this.#location_gps_available;
    return this.isGeolocationPossible().then(() => {
      return this.#location_gps_available;
    });
  }
}

export const gpsManager = new GpsManager();
