import { LngLat, LngLatBounds } from "maplibre-gl";
import { map } from "./maps/MetaGerMapModule";
import { formatDistance, formatDuration } from "./utils";
import { along, bbox, length, lineString } from "@turf/turf";
import { UrlGenerator } from "./UrlGenerator";

export class Route {
  data = null;
  profile = "guess";
  selected = null;

  waypoint_from_id = null;
  waypoint_to_id = null;

  dom_node = null;
  popups = [];

  constructor(profile = "guess", data = null) {
    this.#createDomNode();

    this.profile = profile;
    if (data) {
      this.data = data;
      this.selected = 0;
      this.#createDomNode();
    }
  }

  #createDomNode() {
    if (!this.dom_node) {
      this.dom_node = document.createElement("details");
      this.dom_node.classList.add("route");
    } else {
      this.dom_node.textContent = "";
    }

    let summary = document.createElement("summary");
    let loading = document.createElement("div");
    loading.classList.add("loading");
    let img = document.createElement("img");
    img.src = "/img/ajax-loader.gif";
    img.alt = "Loading...";
    loading.appendChild(img);
    let loading_text = document.createElement("div");
    loading_text.classList.add("text");
    loading_text.textContent = "Route wird geladen...";
    loading.appendChild(loading_text);
    summary.appendChild(loading);

    if (this.data) {
      let route_summary = document.createElement("div");
      route_summary.classList.add("summary");
      let route_zoom = document.createElement("a");
      route_zoom.classList.add("focus-route");
      let img = document.createElement("img");
      img.src = "/img/lupe.svg";
      route_zoom.appendChild(img);
      route_zoom.onclick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.#showRouteOnMap();
      };
      route_summary.appendChild(route_zoom);
      let route_description = document.createElement("div");
      route_description.classList.add("description");
      if (
        this.data.paths[this.selected].description &&
        this.data.paths[this.selected].description.length > 0
      ) {
        route_description.textContent =
          "über " + this.data.paths[this.selected].description.join(" und ");
      }
      route_summary.appendChild(route_description);

      let route_duration = document.createElement("div");
      route_duration.classList.add("duration");
      route_duration.textContent = formatDuration(
        this.data.paths[this.selected].time
      );
      route_summary.appendChild(route_duration);

      let route_distance = document.createElement("div");
      route_distance.classList.add("distance");
      route_distance.textContent = formatDistance(
        this.data.paths[this.selected].distance
      );
      route_summary.appendChild(route_distance);

      summary.appendChild(route_summary);

      // Append the route steps
      let route_steps = document.createElement("div");
      route_steps.classList.add("route-steps");
      for (const instruction of this.data.paths[this.selected].instructions) {
        let step = document.createElement("div");
        step.classList.add("step");

        let direction = document.createElement("div");
        direction.classList.add("direction");
        direction.classList.add(`direction-${instruction.sign}`);
        step.appendChild(direction);

        let text = document.createElement("div");
        text.classList.add("text");
        text.textContent = instruction.text;
        step.appendChild(text);

        let distance = document.createElement("div");
        distance.classList.add("distance");
        distance.textContent = formatDistance(instruction.distance);
        step.appendChild(distance);

        route_steps.appendChild(step);

        step.onclick = () => {
          let bbox_array = bbox(
            lineString(
              this.data.paths[this.selected].points.coordinates.slice(
                instruction.interval[0],
                instruction.interval[1] + 1
              )
            )
          );
          map.fitBounds(new LngLatBounds(bbox_array));
        };
      }
      this.dom_node.appendChild(route_steps);
    }
    this.dom_node.appendChild(summary);
  }

  showGeoJson(sourcename_active, sourcename_inactive) {
    if (!this.data) return;
    // Print active path
    if (map.map.getSource(sourcename_active) == undefined) {
      throw new Error(
        `Source "${sourcename_active}" does not exist on the map.`
      );
    }
    if (map.map.getSource(sourcename_inactive) == undefined) {
      throw new Error(
        `Source "${sourcename_inactive}" does not exist on the map.`
      );
    }
    let activeFeatures = [];
    let inactiveFeatures = [];

    for (const [index, path] of this.data.paths.entries()) {
      let feature = {
        type: "Feature",
        properties: {},
        geometry: path.points,
      };
      if (this.selected == index) {
        activeFeatures.push(feature);
      } else {
        inactiveFeatures.push(feature);
      }
    }
    let active_data = map.map.getSource(sourcename_active)._data;
    active_data.features = active_data.features.concat(activeFeatures);
    map.map.getSource(sourcename_active).setData(active_data);

    let inactive_data = map.map.getSource(sourcename_inactive)._data;
    inactive_data.features = inactive_data.features.concat(inactiveFeatures);
    map.map.getSource(sourcename_inactive).setData(inactive_data);
  }

  async calculateRoute(waypoint_from, waypoint_to) {
    if (this.data) return;

    this.waypoint_from_id = waypoint_from.toString();
    this.waypoint_to_id = waypoint_to.toString();

    if (
      this.profile == "guess" ||
      !["car", "bike", "foot"].includes(this.profile)
    ) {
      // Guess a default profile based on the distance between both points
      let from_point = new LngLat(
        waypoint_from.location.lng,
        waypoint_from.location.lat
      );
      let to_point = new LngLat(
        waypoint_to.location.lng,
        waypoint_to.location.lat
      );
      let distance = from_point.distanceTo(to_point);
      if (distance <= 1000) {
        this.profile = "foot";
      } else if (distance <= 7500) {
        this.profile = "bike";
      } else {
        this.profile = "car";
      }
    }

    let base_url = "/route/calc";

    this.dom_node.querySelector("summary").classList.add("loading");

    let post_data = {
      profile: this.profile,
      points: [
        [waypoint_from.location.lng, waypoint_from.location.lat],
        [waypoint_to.location.lng, waypoint_to.location.lat],
      ],
      locale: "de",
      points_encoded: false,
      "ch.disable": this.profile == "car" ? false : true,
      details: [
        "street_name",
        "street_ref",
        "street_destination",
        "roundabout",
        "time",
        "distance",
        "max_speed",
        "road_class",
        "road_class_link",
      ],
      algorithm: "alternative_route",
    };
    if (waypoint_from.heading) {
      // Heading is defined for this waypoint
      if (this.profile != "foot") {
        post_data.headings = [`${waypoint_from.heading}`];
        post_data["ch.disable"] = true;
        if (this.profile == "car") {
          post_data["lm.disable"] = true;
          post_data.heading_penalty = 600;
        }
        if (this.profile == "bike") {
          post_data.heading_penalty = 20; // Do not force heading that much on bike
        }
      }
    }

    return fetch(UrlGenerator.to(base_url), {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(post_data),
    })
      .then((response) => response.json())
      .then((response) => {
        this.data = response;
        this.selected = 0;
        this.#createPopups();
        this.dom_node.querySelector("summary").classList.remove("loading");
        this.#createDomNode();
        return;
      });
  }

  removeCoordinates(n) {
    if (n < 1) return;
    let active_path = this.data.paths[this.selected];
    if (n > active_path.points.coordinates.length)
      n = active_path.points.coordinates.length;
    active_path.points.coordinates.splice(0, n);
    let old_distance = active_path.distance;
    let new_linestring = lineString(active_path.points.coordinates);
    active_path.distance = Math.round(length(new_linestring) * 1000000) / 1000;
    active_path.time = Math.round(
      (active_path.distance / old_distance) * active_path.time
    );
    active_path.bbox = bbox(new_linestring);

    let splice_count = 0;
    for (const [index, instruction] of active_path.instructions.entries()) {
      // Update interval
      instruction.interval = [
        Math.max(instruction.interval[0] - n, 0),
        Math.max(instruction.interval[1] - n, 0),
      ];
      if (instruction.interval[1] == 0) {
        splice_count++;
        continue;
      }
      if (instruction.interval[0] > 0) continue;

      let line_string_feature = lineString(
        active_path.points.coordinates.slice(
          instruction.interval[0],
          instruction.interval[1] + 1
        )
      );
      old_distance = instruction.distance;

      instruction.distance =
        Math.round(length(line_string_feature) * 1000000) / 1000;
      if (old_distance > 0) {
        instruction.time = Math.round(
          (instruction.distance / old_distance) * instruction.time
        );
      } else {
        instruction.time = 0;
      }
      active_path.instructions[index] = instruction;
    }
    if (splice_count > 0) active_path.instructions.splice(0, splice_count);
    this.data.paths[this.selected] = active_path;
  }
  // Used by navigation will update the route data to reflect a moving user position
  replaceFirstPointByUserPosition(user_position) {
    let activeRoute = this.data.paths[this.selected];
    if (activeRoute.points.coordinates.length < 2) return;
    activeRoute.points.coordinates[0] = user_position.geometry.coordinates;

    // Update the first instruction
    let instruction_distance_old = activeRoute.instructions[0].distance;
    let instruction_time_old = activeRoute.instructions[0].time;
    let instructionLineString = lineString(
      activeRoute.points.coordinates.slice(
        activeRoute.instructions[0].interval[0],
        activeRoute.instructions[0].interval[1] + 1
      )
    );

    let instruction_distance_new =
      Math.round(length(instructionLineString) * 1000000) / 1000; // Round distance to 3 digits
    activeRoute.instructions[0].distance = instruction_distance_new;

    if (instruction_distance_old > 0) {
      activeRoute.instructions[0].time = Math.round(
        (instruction_distance_new / instruction_distance_old) *
        instruction_time_old
      );
    } else {
      activeRoute.instructions[0].time = 0;
    }

    // Update total route distance and time
    let distance_line_string = lineString(activeRoute.points.coordinates);
    let distance_old = activeRoute.distance;
    let distance_new =
      Math.round(length(distance_line_string) * 1000000) / 1000; // Round distance to 3 digits
    activeRoute.distance = distance_new;

    let time_old = activeRoute.time;
    if (distance_old > 0) {
      activeRoute.time = Math.round((distance_new / distance_old) * time_old);
    } else {
      activeRoute.time = 0;
    }

    let new_bbox = bbox(distance_line_string);
    activeRoute.bbox = new_bbox;
    this.data.paths[this.selected] = activeRoute;
  }

  #showRouteOnMap() {
    let bbox = this.data.paths[this.selected].bbox;
    let paddingInline = 150;
    if (window.screen.width < 800) {
      paddingInline = 50;
    }
    map.fitBounds(new LngLatBounds(bbox), {
      duration: 250,
      padding: {
        top: paddingInline,
        bottom: paddingInline,
        right: paddingInline,
        left: paddingInline,
      },
    });
  }

  #createPopups() {
    if (!this.data) return;
    this.#parseIntersectionpoints();
    this.#removePopups();
    for (const [index, path] of this.data.paths.entries()) {
      if (index == this.selected) continue;
      let description = "";
      if (path.description && path.description.length > 0) {
        description = "über " + path.description.join(" und ");
      }
      let time_difference = formatDuration(
        path.time - this.data.paths[this.selected].time
      );
      if (path.time > this.data.paths[this.selected].time) {
        time_difference += " länger";
      } else {
        time_difference += " kürzer";
      }
      let popup_dom_element = document.createElement("div");
      popup_dom_element.classList.add("alt-route-popup");
      let popup_label = document.createElement("label");
      popup_label.textContent = "Alternative Route";
      popup_dom_element.appendChild(popup_label);
      let popup_description = document.createElement("label");
      popup_description.textContent = description;
      popup_dom_element.appendChild(popup_description);
      let popup_time = document.createElement("div");
      popup_time.textContent = time_difference;
      popup_dom_element.appendChild(popup_time);
      let popup = map.createPopup(
        popup_dom_element,
        new LngLat(path.intersection_point[0], path.intersection_point[1])
      );

      popup_dom_element.addEventListener("click", (e) => {
        this.selected = index;
        this.#createDomNode();
        this.#createPopups();
        this.dom_node.dispatchEvent(new Event("change"));
      });

      this.popups.push(popup);
    }
  }

  showPopups() {
    this.#createPopups();
  }

  hidePopups() {
    for (const [index, popup] of this.popups.entries()) {
      map.removePopup(popup);
    }
  }

  #removePopups() {
    this.hidePopups();
    this.popups = [];
  }

  /**
   * Go through all alternative routes and calculate the first point where it differs
   * from the currently selected route
   * Store that point in the route data
   */
  #parseIntersectionpoints() {
    if (!this.data) return;
    for (const [index, path] of this.data.paths.entries()) {
      // Skip currently selected path and delete previously calculated point
      if (index == this.selected) {
        path.intersection = null;
        continue;
      }
      let intersection_index = 0;
      // Now find the first point that differs in comparison to the active route
      for (const [index, point] of path.points.coordinates.entries()) {
        let active_point =
          this.data.paths[this.selected].points.coordinates[index];
        if (point[0] != active_point[0] || point[1] != active_point[1]) break;

        intersection_index = index;
      }

      // Put the popup a few meters further into the alternative route
      let inset_m = 25;

      let lineStringCoordinates = path.points.coordinates;
      lineStringCoordinates.splice(0, intersection_index);

      let target_point = along(
        lineString(lineStringCoordinates),
        inset_m / 1000
      );

      path.intersection_point = target_point.geometry.coordinates;
    }
  }
}
