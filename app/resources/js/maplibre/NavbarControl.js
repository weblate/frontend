export class NavbarControl {
  onAdd(map) {
    this._map = map;
    let container = document.createElement("div");
    container.classList.add("maplibregl-ctrl", "maplibregl-ctrl-group", "control-navbar");

    let button = document.createElement("button");
    let span = document.createElement("span");
    span.textContent = "≡";
    button.appendChild(span);
    container.appendChild(button);

    button.onclick = e => {
      let opener = document.getElementById("nav-opener");
      opener.checked = !opener.checked;
    };

    this.container = container;
    return container;
  }
  onRemove(map) {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}
