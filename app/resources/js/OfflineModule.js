const EXTRACT_BOX_COLOR = "#00ff00";

function OfflineModule(interactiveMap) {
  this.areaSelectionText =
    "Bewege die Karte, sodass das herunterzuladende Gebiet angezeigt wird und klicke rechts auf download.";
  this.interactiveMap = interactiveMap;
  this.areas = [];
  this.downloadMaxLon = 4;
  this.downloadMaxLat = 2.5;

  this.initializeInterface(this);
  this.addListeners(this);

  this.androidConnector = new OfflineModuleAndroidConnector();
}

OfflineModule.prototype.addListeners = function (offlineModule) {
  $("#offline-addon .exit").off();
  $("#offline-addon .exit").click(function () {
    offlineModule.interactiveMap.switchModule("search");
  });

  $("#offline-addon .add-area").off();
  $("#offline-addon .add-area").click(function () {
    offlineModule.startAreaSelection(offlineModule);
  });
};

function androidHasFunction(functionName) {
  return (
    typeof android != "undefined" && typeof android[functionName] == "function"
  );
}

OfflineModule.prototype.loadExtractList = function (offlineModule) {
  $("#offline-addon .exit").off();
  $("#offline-addon .exit").click(function () {
    offlineModule.interactiveMap.switchModule("search");
  });
  $("#offline-addon .add-area").show("slow");
  $(".loading-areas").show();

  $(".downloaded-areas > .area")
    .not("#area-template")
    .remove();

  var extractListJSON = offlineModule.androidConnector.getExtractList();
  var extracts = JSON.parse(extractListJSON);

  $(".loading-areas").hide();

  if (extracts.length == 0) {
    $("#offline-addon .no-areas").show();
    return;
  } else {
    $("#offline-addon .auto-updates").show();
    $("#offline-addon .no-areas").hide();
  }

  extracts.forEach(function (extract) {
    var sizeString = filesizeToString(extract.fileSize);
    var lastModifiedString = dateToString(new Date(extract.lastModified));

    var extractDomElement = offlineModule.createExtractDomElement(
      offlineModule,
      extract.name,
      sizeString,
      lastModifiedString
    );

    extractDomElement.click(function () {
      offlineModule.focusExtract(
        offlineModule,
        extract.boundingBox,
        extractDomElement
      );
    });

    extractDomElement.find(".rename").click(function () {
      offlineModule.renameExtract(offlineModule, extract.name);
    });

    extractDomElement.find(".remove").click(function () {
      offlineModule.removeExtract(offlineModule, extract.name);
    });

    $(".downloaded-areas").append(extractDomElement);
  }, offlineModule);
};

OfflineModule.prototype.renameExtract = function (offlineModule, oldName) {
  var newName = prompt("Geben Sie einen neuen Namen für dieses Gebiet ein:");
  if (newName != null) {
    if (!offlineModule.androidConnector.renameExtract(oldName, newName)) {
      // TODO
    }

    offlineModule.clearBoundingBoxes(offlineModule);

    offlineModule.loadExtractList(offlineModule);
  }
};

OfflineModule.prototype.removeExtract = function (offlineModule, name) {
  if (confirm("Soll das ausgewählte Gebiet wirklich gelöscht werden?")) {
    if (!offlineModule.androidConnector.removeExtract(name)) {
      // TODO
    }

    offlineModule.androidConnector.updateOfflineData();
    offlineModule.clearBoundingBoxes(offlineModule);

    $("#offline-addon .exit").off();
    $("#offline-addon .exit").click(function () {
      offlineModule.interactiveMap.switchModule("search");
    });
    $("#offline-addon .add-area").show("slow");

    offlineModule.loadExtractList(offlineModule);
  }
};

OfflineModule.prototype.drawBoundingBox = function (
  offlineModule,
  boundingBox,
  color
) {
  var boundingBoxGeoJSON = [];
  boundingBoxGeoJSON.push([boundingBox.minLon, boundingBox.minLat]);
  boundingBoxGeoJSON.push([boundingBox.maxLon, boundingBox.minLat]);
  boundingBoxGeoJSON.push([boundingBox.maxLon, boundingBox.maxLat]);
  boundingBoxGeoJSON.push([boundingBox.minLon, boundingBox.maxLat]);
  boundingBoxGeoJSON.push(boundingBoxGeoJSON[0]);

  offlineModule.interactiveMap.map.addLayer({
    id: "extractBox", // TODO use different ids so multiple bounding boxes can be shown at once
    type: "fill",
    layout: {},
    paint: {
      "fill-color": color, // TODO pick different color for each bounding box
      "fill-opacity": 0.3,
      "fill-outline-color": color,
    },
    source: {
      type: "geojson",
      data: {
        type: "Feature",
        geometry: {
          type: "Polygon",
          coordinates: [boundingBoxGeoJSON]
        }
      }
    }
  });
};

OfflineModule.prototype.clearBoundingBoxes = function (offlineModule) {
  if (offlineModule.interactiveMap.map.getLayer("extractBox") != undefined)
    offlineModule.interactiveMap.map.removeLayer("extractBox"); // TODO adjust for multiple boxes with different ids
  if (offlineModule.interactiveMap.map.getSource("extractBox") != undefined)
    offlineModule.interactiveMap.map.removeSource("extractBox"); // TODO adjust for multiple boxes with different ids
};

function filesizeToString(filesize) {
  var unit = "B";
  if (filesize > 1024) {
    filesize /= 1024;
    unit = "KB";
    if (filesize > 1024) {
      filesize /= 1024;
      unit = "MB";
      if (filesize > 1024) {
        filesize /= 1024;
        unit = "GB";
      }
    }
  }
  return Number(filesize).toFixed(2) + " " + unit;
}

function dateToString(date) {
  var month = date.getMonth();
  month += 1;
  month = month < 10 ? "0" + month : month;
  var day = date.getDate();
  day = day < 10 ? "0" + day : day;
  var lastModified = day + "." + month + "." + date.getFullYear();
  return lastModified;
}

OfflineModule.prototype.createExtractDomElement = function (
  offlineModule,
  name,
  sizeString,
  lastModifiedString
) {
  var extractDomElement = $("#area-template").clone();
  extractDomElement.removeAttr("id");
  extractDomElement.removeClass("hidden");
  extractDomElement.removeAttr("style");

  extractDomElement.find(".size").html(sizeString);
  extractDomElement.find(".last-modified").html(lastModifiedString);
  extractDomElement.find(".name").html(name);

  extractDomElement.find(".inspect").attr("data-name", name);
  extractDomElement.find(".rename").attr("data-name", name);
  extractDomElement.find(".remove").attr("data-name", name);

  return extractDomElement;
};

OfflineModule.prototype.focusExtract = function (
  offlineModule,
  boundingBox,
  extractDomElement
) {
  // Turn off action listeners
  extractDomElement.off();

  // Switch displayed control elements
  extractDomElement.find(".inspect").hide("slow");
  extractDomElement.find(".rename").show("slow");
  extractDomElement.find(".remove").show("slow");

  var name = extractDomElement.find(".name").html();

  hideAllExtractsExcept(name);

  $("#offline-addon .exit").off();
  $("#offline-addon .exit").click(function () {
    offlineModule.unfocusExtract(offlineModule, extractDomElement, boundingBox);
  });

  $("#offline-addon .add-area, #offline-addon .auto-updates").hide("slow");

  offlineModule.drawBoundingBox(offlineModule, boundingBox, EXTRACT_BOX_COLOR);

  offlineModule.fitToBoundingBox(offlineModule, boundingBox);
};

function hideAllExtractsExcept(shownAreaName) {
  $(".downloaded-areas > .area").each(function (index, extractDomElement) {
    // Has to be turned into a jquery object, because each delivers vanilla js dom objects
    extractDomElement = $(extractDomElement);

    var areaName = extractDomElement.find(".name").html();
    if (areaName != shownAreaName) {
      extractDomElement.addClass("hidden");
      return true;
    }
  });
}

OfflineModule.prototype.unfocusExtract = function (
  offlineModule,
  extractDomElement,
  extractBoundingBox
) {
  $(".downloaded-areas > div.area").show("slow");
  $(".downloaded-areas > div.area .inspect").show("slow");
  $(".downloaded-areas > div.area .rename").hide("slow");
  $(".downloaded-areas > div.area .remove").hide("slow");

  $("#offline-addon .add-area").show("slow");
  $("#offline-addon .auto-updates").show();

  $("#offline-addon .exit").off();
  $("#offline-addon .exit").click(function () {
    offlineModule.interactiveMap.switchModule("search");
  });

  offlineModule.clearBoundingBoxes(offlineModule);

  offlineModule.loadExtractList(offlineModule);

  extractDomElement.click(function () {
    offlineModule.focusExtract(
      offlineModule,
      extractBoundingBox,
      extractDomElement
    );
  });
};

OfflineModule.prototype.fitToBoundingBox = function (
  offlineModule,
  boundingBox
) {
  var padding = {
    top: $("#offline-addon").outerHeight() + 10,
    right: 10,
    bottom: 10,
    left: 10,
  };
  offlineModule.interactiveMap.map.fitBounds(
    [
      [boundingBox.minLon, boundingBox.minLat],
      [boundingBox.maxLon, boundingBox.maxLat]
    ],
    {
      padding: padding,
      linear: true
    }
  );
};

OfflineModule.prototype.startAreaSelection = function (offlineModule) {
  if (!offlineModule.androidConnector.downloadFonts()) {
    return;
  }

  $("#offline-addon .downloaded-areas").hide("slow");
  $("#offline-addon .area-selection").show("slow");

  $("#start-download").show();


  if (typeof offlineModule.selectedAreaChangedListener == "undefined") {
    offlineModule.selectedAreaChangedListener = function (event) {
      offlineModule.selectedAreaChanged(offlineModule);
    }
  }
  offlineModule.interactiveMap.map.on("dragend", offlineModule.selectedAreaChangedListener);
  offlineModule.interactiveMap.map.on("touchend", offlineModule.selectedAreaChangedListener);

  $("#offline-addon .exit").off();
  $("#offline-addon .exit").click(function () {
    offlineModule.stopAreaSelection(offlineModule);
  });

  $("#start-download").off();
  $("#start-download").click(function () {
    if (typeof offlineModule.selectedAreaChangedListener != "undefined") {
      offlineModule.interactiveMap.map.off("dragend", offlineModule.selectedAreaChangedListener);
      offlineModule.interactiveMap.map.off("touchend", offlineModule.selectedAreaChangedListener);
      offlineModule.selectedAreaChangedListener = undefined;
    }
    offlineModule.startDownload(offlineModule);
  });

  offlineModule.selectedAreaChanged(offlineModule);
};

OfflineModule.prototype.stopAreaSelection = function (offlineModule) {
  $("#offline-addon .downloaded-areas").show("slow");
  $("#offline-addon .area-selection").hide("slow");
  offlineModule.clearBoundingBoxes(offlineModule);
  offlineModule.loadExtractList(offlineModule);

  offlineModule.addListeners(offlineModule);
  if (typeof offlineModule.selectedAreaChangedListener != "undefined") {
    offlineModule.interactiveMap.map.off("dragend", offlineModule.selectedAreaChangedListener);
    offlineModule.interactiveMap.map.off("touchend", offlineModule.selectedAreaChangedListener);
    offlineModule.selectedAreaChangedListener = undefined;
  }
};

OfflineModule.prototype.selectedAreaChanged = function (offlineModule) {
  offlineModule.updateDownloadSizeExpectation(offlineModule);
};

OfflineModule.prototype.updateDownloadSizeExpectation = function (
  offlineModule
) {
  boundingBox = offlineModule.getBoundingBoxFromMap(offlineModule);
  offlineModule.clearBoundingBoxes(offlineModule);
  if (boundingBox.maxLon - boundingBox.minLon == offlineModule.downloadMaxLon
    || boundingBox.maxLat - boundingBox.minLat == offlineModule.downloadMaxLat) {
    offlineModule.drawBoundingBox(offlineModule, boundingBox, EXTRACT_BOX_COLOR);
  }

  let url = "https://tileextractor.metager.de/api/extract-size/" + boundingBox.minLon + "/" + boundingBox.maxLon + "/" + boundingBox.minLat + "/" + boundingBox.maxLat;
  if (typeof this.downloadSizeUpdate != "undefined") {
    this.downloadSizeUpdate.abort();
    this.downloadSizeUpdate = null;
  }
  $("#offline-addon .download-information > .size").html("<img src='/img/ajax-loader.gif' alt='loading'></img>");
  $("#offline-addon #start-download").hide("fast");
  this.downloadSizeUpdate = $.get(url, function (data) {
    $("#offline-addon #start-download").show("fast");
    var size = parseInt(data);
    if (size > 0) {
      var expectedDownloadSizeString = filesizeToString(size);
      $(".download-information > .size").html(expectedDownloadSizeString);
    } else {
      $(".download-information > .size").html("Error");
    }
  });
};

OfflineModule.prototype.startDownload = function (offlineModule, selectedArea) {
  if (!offlineModule.androidConnector.isInternetAvailable) {
    alert("Sie haben zurzeit kein Internet.");
    return;
  }

  if (!offlineModule.reassureDownloadIfNoWireless(offlineModule)) {
    return;
  }

  var extractName = offlineModule.askForExtractName(offlineModule);
  if (extractName == null || extractName == "") {
    return;
  }

  $(".exit").hide("fast");
  $(".area-selection").hide();
  $(".area-downloading").show();
  $(".download-progress").show();

  $("#offline-addon .progress-bar").attr("aria-valuenow", 0);
  $("#offline-addon .progress-bar").css("width", "0%");
  $("#offline-addon .progress-label").html("0%");

  var boundingBox = offlineModule.getBoundingBoxFromMap(offlineModule);
  var downloadStarted = offlineModule.androidConnector.downloadTilesInBoundingBox(
    boundingBox,
    extractName
  );

  if (downloadStarted) {
    $("#offline-addon .download-progress .abort").off();
    $("#offline-addon .download-progress .abort").click(function () {
      $("#offline-addon .download-progress .abort").off();
      offlineModule.cancelDownload(offlineModule);
    });

    offlineModule.updateDownloadStatus(offlineModule);
  } else {
    offlineModule.cancelDownload(offlineModule);
  }
};

OfflineModule.prototype.askForExtractName = function (offlineModule) {
  var extractName = prompt("Bitte Namen für Kartenausschnitt eingeben.");
  return extractName;
};

OfflineModule.prototype.reassureDownloadIfNoWireless = function (offlineModule) {
  if (offlineModule.androidConnector.isWireless()) {
    return true;
  } else {
    var prompt =
      "Die Download-Größe beträgt " +
      $(".download-information > .size").html() +
      ". Ohne WLan fortfahren?";
    var confirmation = confirm(prompt);
    return confirmation;
  }
};

OfflineModule.prototype.getBoundingBoxFromMap = function (offlineModule) {
  var bbox_raw = offlineModule.interactiveMap.map.getBounds();
  if (bbox_raw.getEast() - bbox_raw.getWest() > 4) {
    //|| ) {
    var tooMuch = bbox_raw.getEast() - bbox_raw.getWest() - 4;
    bbox_raw.setNorthEast([bbox_raw.getEast() - (tooMuch / 2.0), bbox_raw.getNorth()]);
    bbox_raw.setSouthWest([bbox_raw.getWest() + (tooMuch / 2.0), bbox_raw.getSouth()]);
  }
  if (bbox_raw.getNorth() - bbox_raw.getSouth() > 2.5) {
    var tooMuch = bbox_raw.getNorth() - bbox_raw.getSouth() - 2.5;
    bbox_raw.setNorthEast([bbox_raw.getEast(), bbox_raw.getNorth() - (tooMuch / 2.0)]);
    bbox_raw.setSouthWest([bbox_raw.getWest(), bbox_raw.getSouth() + (tooMuch / 2.0)]);
  }
  var bbox = {
    minLon: bbox_raw.getWest(),
    maxLon: bbox_raw.getEast(),
    minLat: bbox_raw.getSouth(),
    maxLat: bbox_raw.getNorth()
  };
  return bbox;
};

OfflineModule.prototype.updateDownloadStatus = function (offlineModule) {
  var error;
  if ((error = offlineModule.androidConnector.getError()) != "") {
    $("#offline-addon .download-progress").before(
      $('\
			<div class="download-failed alert alert-danger">' + error + "</div>")
    );

    window.setTimeout(function () {
      $("#offline-addon .download-failed").remove();
      offlineModule.cancelDownload(offlineModule);
    }, 5000);

    return;
  }

  var downloadedPercentage = offlineModule.androidConnector.getDownloadPercentage();
  if (offlineModule.androidConnector.isDownloadFinished()) {
    offlineModule.exitDownload(offlineModule);
    offlineModule.androidConnector.updateOfflineData();
  } else {
    $("#offline-addon .download-progress .progress-bar").attr(
      "aria-valuemax",
      1
    );
    $("#offline-addon .download-progress .progress-bar").attr(
      "aria-valuenow",
      downloadedPercentage
    );
    $("#offline-addon .download-progress .progress-bar").css(
      "width",
      downloadedPercentage * 100 + "%"
    );
    $("#offline-addon .progress-label").html(
      (downloadedPercentage * 100).toFixed(1) + " %"
    );

    window.setTimeout(function () {
      offlineModule.updateDownloadStatus(offlineModule);
    }, 100);
  }
};

OfflineModule.prototype.cancelDownload = function (offlineModule) {
  offlineModule.androidConnector.cancelDownload();
  offlineModule.exitDownload(offlineModule);
};

OfflineModule.prototype.exitDownload = function (offlineModule) {
  $(".exit").show("fast");
  $(".area-selection").show();
  $(".area-downloading").hide();
  $(".download-progress").hide();
  offlineModule.stopAreaSelection(offlineModule);
};

OfflineModule.prototype.initializeInterface = function (offlineModule) {
  // Hide everything from Map that is not needed:
  $(".ol-zoom, .ol-zoomslider").hide("slow");
  $("#offline-addon .no-areas").hide();
  $("#offline-addon .loading-areas").show();
  offlineModule.interactiveMap.reversePositionManager.setActive(false);
  $("#offline-addon").show(
    "slow",
    $.proxy(function () {
      offlineModule.loadExtractList(offlineModule);
    }, offlineModule)
  );

  // this.interactiveMap.map.addLayer(this.layer); // TODO
};

OfflineModule.prototype.exit = function () {
  $("#offline-addon .placeholder.area-selection-info").hide();
  $("#offline-addon").hide("slow");

  $("#offline-addon .exit").off();
  $("#start-download").off();

  //$("#offline-addon .downloaded-areas > div:not(.placeholder)").remove();

  $(".ol-zoom, .ol-zoomslider").show();
  $(".downloaded-areas .placeholder").show();

  this.interactiveMap.reversePositionManager.setActive(true);
};

OfflineModule.prototype.enableGps = function (offlineModule) { };

OfflineModule.prototype.disableGps = function (offlineModule) { };
