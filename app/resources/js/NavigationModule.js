import { LngLat, LngLatBounds } from "maplibre-gl";
import { androidConnector } from "./AndroidConnector";
import { gpsManager } from "./GpsManager";
import { routeModule } from "./RouteFinder";
import { MetaGerMap } from "./maps/MetaGerMap";
import { map } from "./maps/MetaGerMapModule";
import { compareBearings, formatDistance, formatDuration } from "./utils";
import {
  bearingToAzimuth,
  destination,
  lineString,
  distance,
  nearestPointOnLine,
  point,
  length,
  pointToLineDistance,
  booleanEqual,
  bearing,
  circle,
  lineIntersect,
  bbox,
  along,
} from "@turf/turf";

class NavigationModule {
  htmlmodule = document.querySelector("#navigation");
  container_next_step = this.htmlmodule.querySelector("#next-step");
  container_additional_step = this.htmlmodule.querySelector("#additional-step");
  container_route_information = this.htmlmodule.querySelector(
    "#general-information"
  );

  geolocation_options = {
    enableHighAccuracy: true,
    maximumAge: 500,
    timeout: Infinity,
  };
  geolocation_watch_id = null;
  gps_lost_timeout = null;
  user_moved = false;
  user_moved_timestamp = Date.now() - 3000; // Triggered when user moves the map so we do not refocus position afterwards
  user_moved_callback = (e) => {
    if (
      !e.hasOwnProperty("auto") &&
      (!e.hasOwnProperty("0") ||
        !ResizeObserverEntry.prototype.isPrototypeOf(e[0]))
    ) {
      this.user_moved = true;
      this.htmlmodule.classList.add("user-moved");
      this.#setUserMoved(Date.now());
    }
  };

  progress_queued = null; // Progress update is queued (setTimeout)
  progress_interval_ms = 250; // How often to update the progress on route. Will be updated dynamically
  latest_location = null;

  current_location = null; // Point where we currently expect the user to be. Can be snapped to the route
  current_heading = null; // Latest heading we calculated. Can be either from GPS information or snapped to the route heading
  current_speed = null;
  gps_heading = null;
  is_on_route = false;

  route_recalculation_timer = {
    start: null,
    count: 0,
    distance_from_route: null,
  };
  last_progress = null;

  constructor() {
    let exit_handler = (e) => {
      this.exit();
    };
    this.htmlmodule.querySelector("#general-information .exit").onclick = () =>
      exit_handler();
    this.htmlmodule.querySelector("#finish-navigation").onclick = () => {
      routeModule.removeWaypoint(1);
      exit_handler();
      routeModule.updateInterface();
      routeModule.fitRoutesOnMap();
    };
    this.htmlmodule.querySelector("#next-waypoint").onclick = () => {
      this.htmlmodule.classList.remove("multi-finished");
      let new_route = routeModule.waypoints[1].route;
      let waypoint_pos = new LngLat(
        routeModule.waypoints[1].location.lng,
        routeModule.waypoints[1].location.lat
      );
      routeModule.removeWaypoint(1);
      // Update GPS Position to match start of new route
      routeModule.waypoints[0].route = new_route;
      routeModule.waypoints[0].location = waypoint_pos;

      // Update Position
      routeModule.updateInterface();
      routeModule.updateState();
      this.progress();
    };
    this.htmlmodule.querySelector("#center-position").onclick = (e) => {
      this.#setUserMoved(Date.now() - 3000);
      this.user_moved = false;
      this.progress();
    };
  }

  /** Parses all current location data and updates the route and view accordingly */
  progress() {
    let current_location = this.current_location;
    this.processLocation();
    this.updateRouteInformation();
    this.updateNextStep();
    this.fitMap(current_location);
    this.calculateProgressInterval();
    this.last_progress = new Date().getTime();
  }

  // Calculates Progress interval based on distance to current and next turn
  calculateProgressInterval() {
    let new_interval_ms = 1000;
    if (
      !this.current_speed ||
      !this.is_on_route ||
      !this.current_location ||
      !this.current_location.properties
    ) {
      this.progress_interval_ms = new_interval_ms;
      return;
    }

    let dist_to_end =
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ].instructions[0].distance;
    if (dist_to_end / this.current_speed > 20) {
      // We can increase the update interval
      this.progress_interval_ms = Math.min(
        this.progress_interval_ms * 1.1,
        1500
      );
    } else {
      this.progress_interval_ms = Math.max(
        this.progress_interval_ms * 0.75,
        250
      );
    }
  }

  /**
   * Updates the map position according to the current heading and route status
   */
  fitMap(last_location) {
    if (!this.current_heading || !routeModule.waypoints[0].route.data) return; // Route is probably getting recalculated
    let active_route =
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ];

    let distance_to_view = Math.max(
      Math.min(active_route.instructions[0].distance, 5000),
      100
    );
    // Round the distance so the map is not constantly zooming
    if (distance_to_view > 1000) {
      // Round to the next thousand
      distance_to_view = Math.ceil(distance_to_view / 1000) * 1000;
    } else if (distance_to_view > 250) {
      // Round to the next hundred
      distance_to_view = Math.ceil(distance_to_view / 100) * 100;
    }

    let view_point = destination(
      this.current_location,
      distance_to_view / 1000,
      compareBearings(0, this.current_heading)
    );

    let bbox_geom = bbox(
      lineString([
        this.current_location.geometry.coordinates,
        view_point.geometry.coordinates,
      ])
    );

    let to = this.current_location;
    let line =
      last_location && to
        ? lineString([
            last_location.geometry.coordinates,
            to.geometry.coordinates,
          ])
        : null;
    let length = last_location && to ? distance(last_location, to) : null;
    let options = {
      bearing: compareBearings(0, this.current_heading),
      pitch: 50,
      duration: 0,
      easing: (number) => {
        return number;
        if (length != null) {
          let new_location = turf.along(line, length * number);
          if (map.user_marker) {
            if (active_route.finished) map.user_marker.addClassName("finish");
            else map.user_marker.removeClassName("finish");
          }
          map.showUserPosition(
            new_location,
            this.current_heading,
            this.latest_location.coords.accuracy
          );
        }
        return number;
      },
    };

    let camera = map.cameraForBounds(new LngLatBounds(bbox_geom), options);
    camera.bearing = this.current_heading; // For some reason maplibre does not set the bearing of the request in the camera response

    // Check if we should animate the movement
    // If the change to the current view is not that large we can skip animation
    let animation_duration = Math.round(this.progress_interval_ms);
    let current_bearing = map.getBearing();
    let zoom_diff = camera.zoom - map.getZoom();
    let bearing_change = Math.max(
      Math.abs(compareBearings(current_bearing, camera.bearing)),
      0.1
    ); // Make sure bearing change is never 0
    // Animate Bearing change no faster than 25° / s
    let change_in_animation_duration =
      (50 / 1000) *
      animation_duration *
      (bearing_change / Math.abs(bearing_change));
    if (Math.abs(bearing_change) > 5) {
      options.duration =
        (Math.abs(bearing_change) / Math.abs(change_in_animation_duration)) *
        1000;
    }
    if (Math.abs(zoom_diff) > 1) {
      // Ease any zoom change that is quite large
      let zoom_duration = (1 / 2) * Math.abs(zoom_diff) * 1000;
      options.duration = Math.max(options.duration, zoom_duration);
    } else {
      // Check if the new position will cover a considerable distance on the current viewport
      let canvas_height = map.getHeight();
      let center_point = map.project(map.getCenter());
      let user_point = map.project(
        new LngLat(
          this.current_location.geometry.coordinates[0],
          this.current_location.geometry.coordinates[1]
        )
      );
      let dist = center_point.dist(user_point);
      if (
        dist / canvas_height < 0.1 ||
        dist / canvas_height > 0.45 ||
        this.#isCloseToTurn()
      ) {
        options.duration = animation_duration;
      }
    }

    camera = { ...camera, ...options };
    let move_map = false;
    if (!this.user_moved) {
      move_map = true;
    } else if (Date.now() - this.user_moved > 2000) {
      // Check if the map was moved for a large enough amount
      let canvas_height = map.getHeight();
      let current_point = map.project(map.getCenter());
      let new_point = map.project(camera.center);

      let point_moved_height = current_point.dist(new_point) / canvas_height;
      if (point_moved_height < 0.1) {
        move_map = true;
      }
    }
    if (this.current_location) {
      map.showUserPosition(
        new LngLat(
          this.current_location.geometry.coordinates[0],
          this.current_location.geometry.coordinates[1]
        ),
        this.current_heading,
        this.latest_location.coords.accuracy
      );
    }
    if (move_map) {
      this.htmlmodule.classList.remove("user-moved");
      if (camera.duration > 0) {
        let padding = Math.round(map.getHeight() * 0.15);
        map.easeTo({ ...camera }, { auto: true });
      }
    }
  }

  /**
   * Takes the latest GPS location and tries to snap it to the active route
   */
  processLocation() {
    if (!this.latest_location) return;
    let active_route =
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ];

    let snapped_route_point = this.#snapLocationToRoutePoints();
    if (snapped_route_point) {
      this.is_on_route = true;
      this.route_recalculation_timer.start = null;
      this.route_recalculation_timer.count = 0;
      this.route_recalculation_timer.distance_from_route = null;

      // New User position will be on the route
      // Check if we passed another segment

      this.#updateRoute(snapped_route_point);
      this.current_location = snapped_route_point;
      if (
        this.#isCloseToTurn() &&
        this.gps_heading &&
        this.current_speed &&
        this.current_speed > 5.55556
      ) {
        // Set heading to GPS heading if speed is larger than 20 km/h
        this.current_heading = this.gps_heading;
      } else {
        // Set heading to route heading
        let heading = this.#getRouteHeading();
        if (heading) {
          this.current_heading = heading;
        }
      }
    } else {
      this.is_on_route = false;

      // No snapped route point. the user has probably left the route
      this.current_location = point([
        this.latest_location.coords.longitude,
        this.latest_location.coords.latitude,
      ]);

      if (!this.current_heading && this.gps_heading)
        this.current_heading = this.gps_heading;
      if (this.gps_heading) {
        let bearing_diff = compareBearings(
          this.current_heading,
          this.gps_heading
        );
        let time_for_bearing_change =
          Math.min(Math.abs(bearing_diff) / 90, 1) * 3000;
        this.current_heading =
          this.current_heading +
          Math.min(this.progress_interval_ms / time_for_bearing_change, 1) *
            bearing_diff;
      }
      // Start Timer to recalculate route
      // When the user has 5 Locations outside of route within at least 3 seconds we recalculate

      if (!active_route.finished) {
        let route_line = lineString(active_route.points.coordinates);
        let route_distance_m = Math.round(
          pointToLineDistance(this.current_location, route_line) * 1000
        );

        let accuracy = this.latest_location.coords.accuracy;

        if (this.route_recalculation_timer.start == null) {
          this.route_recalculation_timer.start = Date.now();
          this.route_recalculation_timer.count = 1;
          this.route_recalculation_timer.distance_from_route = route_distance_m;
        } else if (
          this.route_recalculation_timer.distance_from_route <
          route_distance_m + accuracy
        ) {
          this.route_recalculation_timer.count++;
        }

        if (this.route_recalculation_timer.count >= 3) {
          this.htmlmodule.classList.add("route-calculation");
          routeModule.calculateRoutes().then(() => {
            this.htmlmodule.classList.remove("route-calculation");
            routeModule.updateInterface();
            this.route_recalculation_timer.date = null;
            this.route_recalculation_timer.count = 0;
          });
        } else if (accuracy >= 25) {
          // Do not update user position
          this.current_heading = null;
          this.current_location = null;
        }
      }
    }
    if (this.current_location != null && this.current_heading != null) {
      if (map.user_marker == null) {
        map.showUserPosition(
          new LngLat(
            this.current_location.geometry.coordinates[0],
            this.current_location.geometry.coordinates[1]
          ),
          this.current_heading,
          0
        );
      }
      // Update GPS position in waypoint
      routeModule.waypoints[0].location.lng =
        this.current_location.geometry.coordinates[0];
      routeModule.waypoints[0].location.lat =
        this.current_location.geometry.coordinates[1];
      routeModule.waypoints[0].heading = Math.round(
        bearingToAzimuth(this.current_heading)
      );
      routeModule.waypoints[0].gps = {
        timestamp: this.latest_location.timestamp,
        coords: {
          latitude: this.latest_location.coords.latitude,
          longitude: this.latest_location.coords.longitude,
          altitude: this.latest_location.coords.altitude,
          accuracy: this.latest_location.coords.accuracy,
          altitudeAccuracy: this.latest_location.coords.altitudeAccuracy,
          heading: this.latest_location.coords.heading,
          speed: this.latest_location.coords.speed,
        },
      };
    }
  }

  #isCloseToTurn() {
    // Assume the update interval is set to correspond to the relative distance to the next turn
    if (this.progress_interval_ms < 1000) {
      return true;
    } else {
      return false;
    }
  }

  // Will make changes to the current route object according to the new user position
  #updateRoute(snapped_point) {
    if (
      snapped_point.properties &&
      snapped_point.properties.index &&
      snapped_point.properties.index > 0
    ) {
      // Remove previous coordinates
      routeModule.waypoints[0].route.removeCoordinates(
        snapped_point.properties.index
      );
    }
    // User is still on the first segment
    // Replace the first point with the new one
    routeModule.waypoints[0].route.replaceFirstPointByUserPosition(
      snapped_point
    );
    let active_route =
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ];
    if (active_route.distance <= 100) {
      // Routing finished for this waypoint
      active_route.finished = true;
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ] = active_route;
    }
  }

  #getRouteHeading() {
    let active_route =
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ];
    if (active_route.points.coordinates.length < 2) return 0;
    let point_a = point(active_route.points.coordinates[0]);
    let point_b = point(active_route.points.coordinates[1]);
    if (booleanEqual(point_a, point_b)) return null;
    return bearingToAzimuth(bearing(point_a, point_b));
  }

  #snapLocationToRoutePoints() {
    let acceptable_deviation_m = 15; // How much distance to a segment of the route is allowed to be considered valid. The gps accuracy can expand this range
    let result = [];
    if (!this.latest_location) return result;
    let latest_gps_point = point([
      this.latest_location.coords.longitude,
      this.latest_location.coords.latitude,
    ]);
    let latest_accuracy = this.latest_location.coords.accuracy;
    let acceptable_distance = latest_accuracy + acceptable_deviation_m;

    // Check if the first line segment matches
    let active_route =
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ];
    if (active_route.points.coordinates.length < 2) return result; // The Route is not long enough to have segments
    let line = lineString(active_route.points.coordinates);
    let circle_feature = circle(latest_gps_point, acceptable_distance / 1000);
    let intersectionPoints = lineIntersect(line, circle_feature);
    let intersectionPointsArray = intersectionPoints.features.map((d) => {
      return d.geometry.coordinates;
    });
    let point_on_segment = null;
    // If there is only one intersection our own position
    if (intersectionPointsArray.length == 1) {
      point_on_segment = nearestPointOnLine(line, latest_gps_point);
      if (point_on_segment.properties.dist > acceptable_distance) {
        point_on_segment = null;
      } else if (
        active_route.points.coordinates.length >
        point_on_segment.properties.index + 1
      ) {
        // Move the point on segment backwards by the amount of accuracy
        let reverse_line = lineString(
          active_route.points.coordinates
            .slice(0, point_on_segment.properties.index + 2)
            .reverse()
        );
        reverse_line.geometry.coordinates[0] =
          point_on_segment.geometry.coordinates;
        let backwards_point =
          reverse_line.geometry.coordinates[
            reverse_line.geometry.coordinates.length - 1
          ];
        if (length(reverse_line) > latest_accuracy / 1000)
          backwards_point = along(reverse_line, latest_accuracy / 1000);
        point_on_segment = nearestPointOnLine(line, backwards_point);
      }
    } else {
      for (let intersection_point of intersectionPointsArray) {
        intersection_point = point(intersection_point);
        let point_on_route = nearestPointOnLine(line, intersection_point);
        if (
          point_on_segment == null ||
          point_on_segment.properties.location >
            point_on_route.properties.location
        ) {
          point_on_segment = point_on_route;
        }
      }
    }

    return point_on_segment;
  }

  updateRouteInformation() {
    if (!routeModule.waypoints[0].route.data) return; // Route is probably getting recalculated
    // Updates remaining time/distance for the whole leg/route
    let active_route =
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ];

    if (active_route.finished) {
      if (routeModule.waypoints.length > 2) {
        this.htmlmodule.classList.add("multi-finished");
      } else {
        this.htmlmodule.classList.add("finished");
      }
    }

    let time = active_route.time;
    let distance = active_route.distance;
    let arrival = new Date(Date.now() + time);

    let options = { hour: "2-digit", minute: "2-digit" };
    this.container_route_information.querySelector(".time").textContent =
      formatDuration(time);
    this.container_route_information.querySelector(".distance").textContent =
      formatDistance(distance);
    this.container_route_information.querySelector(".arrival").textContent =
      arrival.toLocaleTimeString(navigator.language, options);
  }

  /**
   * Takes information from the current route and displays the information for the next step
   */
  updateNextStep() {
    if (!routeModule.waypoints[0].route.data) return; // Route is probably getting recalculated

    let next_action_time_before_ms = 80000; // How many seconds before a turn to show the description of the next step to take
    let additional_step_if_too_quick = 30000; // If the step after the next step follows quicker that this time show a preview of the step
    let additional_step_if_too_long = 3600000; // If the current step is long enough show the next step additionally instead of just "follow the road"
    let active_route =
      routeModule.waypoints[0].route.data.paths[
        routeModule.waypoints[0].route.selected
      ];

    let current_instruction = active_route.instructions[0];
    let sign = current_instruction.sign;
    let instruction_text = current_instruction.text;
    let distance = current_instruction.distance;

    // Should we show the next instruction instead of the current one?
    // This happens right before a turn
    if (
      current_instruction.time < next_action_time_before_ms &&
      active_route.instructions.length >= 2
    ) {
      let next_instruction = active_route.instructions[1];
      sign = next_instruction.sign;
      instruction_text = next_instruction.text;

      // Should we show an additional instruction to the current one?
      // Happens if there is another turn right after the current turn is finished
      if (
        next_instruction.time < additional_step_if_too_quick &&
        active_route.instructions.length >= 3
      ) {
        let additional_instruction = active_route.instructions[2];
        let additional_sign = additional_instruction.sign;
        let additional_instruction_text = additional_instruction.text;
        let additional_distance = next_instruction.distance;
        this.container_additional_step.classList.add("active");
        this.container_additional_step
          .querySelector(".direction")
          .setAttribute("class", "direction");
        this.container_additional_step
          .querySelector(".direction")
          .classList.add(`direction-${additional_sign}`);
        this.container_additional_step.querySelector(
          ".instruction"
        ).textContent = additional_instruction_text;
        this.container_additional_step.querySelector(".distance").textContent =
          formatDistance(additional_distance);
      } else {
        this.container_additional_step.classList.remove("active");
      }
    } else {
      let instruction_street_modifier = "";
      if (
        current_instruction.street_name &&
        current_instruction.street_name.length > 0
      ) {
        instruction_street_modifier = `von ${current_instruction.street_name} `;
      } else if (
        current_instruction.street_ref &&
        current_instruction.street_ref.length > 0
      ) {
        instruction_street_modifier = `von ${current_instruction.street_ref} `;
      }
      instruction_text = `Dem Straßenverlauf ${instruction_street_modifier}folgen`;
      sign = 0;
      if (
        current_instruction.time < additional_step_if_too_long &&
        active_route.instructions.length >= 2
      ) {
        // If the current step follows a road for a long time show the turn
        // which follows after following
        let additional_instruction = active_route.instructions[1];
        let additional_sign = additional_instruction.sign;
        let additional_instruction_text = additional_instruction.text;
        this.container_additional_step.classList.add("active");
        this.container_additional_step
          .querySelector(".direction")
          .setAttribute("class", "direction");
        this.container_additional_step
          .querySelector(".direction")
          .classList.add(`direction-${additional_sign}`);
        this.container_additional_step.querySelector(
          ".instruction"
        ).textContent = additional_instruction_text;
        this.container_additional_step.querySelector(".distance").textContent =
          "";
      } else {
        this.container_additional_step.classList.remove("active");
      }
    }

    this.container_next_step
      .querySelector(".direction")
      .setAttribute("class", "direction");
    this.container_next_step
      .querySelector(".direction")
      .classList.add(`direction-${sign}`);
    this.container_next_step.querySelector(".instruction").textContent =
      instruction_text;
    this.container_next_step.querySelector(".distance").textContent =
      formatDistance(distance);
  }

  #setUserMoved(user_moved_timestamp) {
    this.user_moved = user_moved_timestamp;
  }

  gpsUpdate(location) {
    this.latest_location = location;
    if (location.coords.speed != null) {
      this.current_speed = location.coords.speed;
    }
    if (!isNaN(location.coords.heading)) {
      this.gps_heading = location.coords.heading;
    }

    if (
      this.progress_interval_ms == null ||
      this.last_progress == null ||
      new Date().getTime() - this.last_progress > this.progress_interval_ms
    ) {
      this.progress();
    } else if (!this.progress_queued) {
      let timeout_duration =
        new Date().getTime() - this.last_progress - this.progress_interval_ms;
      this.progress_queued = setTimeout(() => {
        try {
          this.progress();
        } catch (error) {
          console.error(error);
        } finally {
          this.progress_queued = null;
        }
      }, timeout_duration);
    }
    if (this.gps_lost_timeout) {
      clearTimeout(this.gps_lost_timeout);
      this.gps_lost_timeout = null;
      this.htmlmodule.classList.remove("gps-lost");
    }
    if (
      this.latest_location.coords.speed != null &&
      this.latest_location.coords.speed > 0
    ) {
      this.gps_lost_timeout = setTimeout(() => {
        this.htmlmodule.classList.add("gps-lost");
      }, 7000);
    }
  }

  enable() {
    map.on("movestart", this.user_moved_callback);

    // Disable the Display Timeout if App is used
    if (
      typeof android !== "undefined" &&
      typeof android["toggleDisplayTimeout"] == "function"
    ) {
      android.toggleDisplayTimeout(false);
    } else if (androidConnector.is_connected) {
      // New Android App is connected. We can toggle the display timeout there
      androidConnector.toggleDisplayTimeout(false); // Disable Displaytimeout
    }
    let gps_position = routeModule.waypoints[0].gps;

    this.gpsUpdate(gps_position);
    this.progress();
    this.geolocation_watch_id = gpsManager.watchPosition(
      this.gpsUpdate.bind(this),
      (error) => this.exit(),
      this.geolocation_options
    );
    this.htmlmodule.classList.add("active");
    document.querySelector("label[for=nav-opener]").style.display = "none";
    map.toggleControl(MetaGerMap.CONTROL_NAVIGATION, false);
    map.toggleControl(MetaGerMap.CONTROL_SCALE, false);
    map.toggleControl(MetaGerMap.CONTROL_ATTRIBUTION, false);
    map.toggleControl(MetaGerMap.CONTROL_GEOLOCATE, false);
    map.toggleControl(MetaGerMap.CONTROL_NAVBAR, false);
  }

  exit() {
    if (this.geolocation_watch_id != null) {
      gpsManager.clearWatch(this.geolocation_watch_id);
      this.geolocation_watch_id = null;
    }
    // Enable the Display Timeout if using the app
    if (
      typeof android !== "undefined" &&
      typeof android["toggleDisplayTimeout"] == "function"
    ) {
      android.toggleDisplayTimeout(true);
    } else if (androidConnector.is_connected) {
      // New Android App is connected. We can toggle the display timeout there
      androidConnector.toggleDisplayTimeout(true); // Disable Displaytimeout
    }

    map.off("movestart", this.user_moved_callback);
    this.htmlmodule.classList.remove("active", "finished", "multi-finished");
    document.querySelector("label[for=nav-opener]").style.display = "grid";
    map.hideUserPosition();
    map.toggleControl(MetaGerMap.CONTROL_NAVIGATION, true);
    map.toggleControl(MetaGerMap.CONTROL_SCALE, true);
    map.toggleControl(MetaGerMap.CONTROL_ATTRIBUTION, true);
    map.toggleControl(MetaGerMap.CONTROL_GEOLOCATE, true);
    map.toggleControl(MetaGerMap.CONTROL_NAVBAR, true);
    map.resetNorthPitch();
    routeModule.stopNavigation();
    routeModule.updateInterface();
  }
}

export const navigationModule = new NavigationModule();
