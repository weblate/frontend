import { map } from "./maps/MetaGerMapModule";

/**
 * Checks if the user has defined a custom theme option light|dark
 * and returns the current setting.
 * 
 * Also applies the theme value
 * 
 * @returns {string}
 */
export function detectTheme() {
    // Check if the user has overwritten the color-scheme
    let theme = "light";
    if (localStorage)
        theme = localStorage.getItem("color-scheme");

    if (theme == null || !["light", "dark"].includes(theme)) {
        theme = getDefaultTheme();
    }
    document.querySelector("meta[name=color-scheme]").content = theme;
    document.querySelector("body").dataset.theme = theme;
    let switch_light = document.getElementById("theme-switch").querySelector("#theme-light");
    let switch_dark = document.getElementById("theme-switch").querySelector("#theme-dark");
    if (switch_light && switch_dark) {
        if (theme == "light") {
            switch_light.classList.add("hidden");
            switch_dark.classList.remove("hidden");
        } else {
            switch_light.classList.remove("hidden");
            switch_dark.classList.add("hidden");
        }
    }

    return theme;
}

function getDefaultTheme() {
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        return "dark";
    } else {
        return "light";
    }
}

function toggleTheme() {
    let theme = detectTheme();
    if (theme == "light") {
        theme = "dark";
    } else {
        theme = "light";
    }
    if (getDefaultTheme() == theme) {
        localStorage.removeItem("color-scheme");
    } else {
        localStorage.setItem("color-scheme", theme);
    }
    map.switchTheme(theme).then(() => {
        detectTheme();
    });
}

export function initializeThemeSwitch() {
    if (!localStorage) return;
    let switch_container = document.getElementById("theme-switch");

    if (!switch_container) return;

    let switch_light = switch_container.querySelector("#theme-light");
    let switch_dark = switch_container.querySelector("#theme-dark");
    if (!switch_light || !switch_dark) return;

    let current_theme = detectTheme();
    if (current_theme == "light") {
        switch_light.classList.remove("hidden");
        switch_dark.classList.add("hidden");
    } else {
        switch_light.classList.add("hidden");
        switch_dark.classList.remove("hidden");
    }

    switch_light.onclick = e => {
        e.preventDefault();
        toggleTheme();
    };
    switch_dark.onclick = e => {
        e.preventDefault();
        toggleTheme();
    };


    switch_container.classList.remove("hidden");
}