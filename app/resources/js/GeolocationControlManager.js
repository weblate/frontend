import { LngLat, LngLatBounds } from "maplibre-gl";
import { gpsManager } from "./GpsManager";
import { map } from "./maps/MetaGerMapModule";

/**
 * Class to wrap the logic around our geolocation
 * control so maplibre and openlayers can use it as backend
 */
export class GeolocationControlManager {
  #container;
  #button;
  #watch_id = null;
  #latest_location = null;

  #high_accuracy_threshold_m = 500; // Threshold after which we accept a location as high accuracy

  /** Geolocation Control is turned off */
  static STATE_OFF = "off";
  /** Geolocation control is turned on (location is updated) but the map is not following the users position */
  static STATE_ON = "on";
  /** Geolocation control is turned on and the map follows the users position */
  static STATE_LOCKED = "locked";
  /** Geolocation control is turned on but has not yet received a geolocation */
  static STATE_LOADING = "loading";
  /** Geolocation control will be disabled because a geolocation error occured */
  static STATE_ERROR = "error";

  #state = GeolocationControlManager.STATE_OFF;

  /**
   * Disables following of user position
   */
  #unlock = ((e) => {
    this.#switchState(GeolocationControlManager.STATE_ON);
  }).bind(this);

  #map = document.getElementById("map");
  /**
   * @param {HTMLButtonElement} button Button that controls geolocation status
   */
  constructor(button) {
    this.#button = button;
    this.#container = button.parentNode;

    this.#button.onclick = this.#click.bind(this);
  }

  /**
   * Handles a click on the geolocation control
   * and toggles the geolocation state
   */
  #click(e) {
    e.stopPropagation();
    // The next state for the geolocation control depends on the current state
    switch (this.#state) {
      case GeolocationControlManager.STATE_OFF:
        this.#switchState(GeolocationControlManager.STATE_LOADING);
        break;
      case GeolocationControlManager.STATE_LOCKED:
        this.#switchState(GeolocationControlManager.STATE_OFF);
        break;
      case GeolocationControlManager.STATE_ON:
        this.#switchState(GeolocationControlManager.STATE_LOCKED);
        break;
    }
  }

  #switchState(new_state) {
    this.#container.classList.remove(this.#state);

    // Turn off Geolocation watch if needed
    if (
      new_state == GeolocationControlManager.STATE_OFF &&
      this.#watch_id != null
    ) {
      this.#stopWatchPosition();
    }

    if (new_state == GeolocationControlManager.STATE_LOCKED) {
      this.#toggleUnlockListeners(true);
    } else {
      this.#toggleUnlockListeners(false);
    }

    this.#container.classList.add(new_state);
    this.#state = new_state;

    if (new_state == GeolocationControlManager.STATE_LOCKED) {
      this.#focusLocation(); // This needs to be called after the state has already changed
    }
    // Turn on Geolocation watch if needed
    if (new_state == GeolocationControlManager.STATE_LOADING) {
      this.#watchPosition();
    }
  }

  #watchPosition() {
    gpsManager.getCurrentPosition(
      (location) => {
        if (this.#state == GeolocationControlManager.STATE_OFF) return;
        this.#watch_position_callback(location);
        this.#watch_id = gpsManager.watchPosition(
          this.#watch_position_callback.bind(this),
          (e) => this.#disableModule(),
          { enableHighAccuracy: true, maximumAge: 1000, timeout: Infinity }
        );
      },
      (e) => this.#disableModule(),
      { enableHighAccuracy: false, maximumAge: 60000, timeout: 20000 }
    );
    // If there is no high accuracy location after ten seconds we'll stop requesting new positions
    setTimeout(() => {
      if (!this.#latest_location || this.#latest_location.coords.accuracy > this.#high_accuracy_threshold_m) {
        this.#switchState(GeolocationControlManager.STATE_OFF);
      }
    }, 10000);
  }

  /**
   * Called when geolocation error occured
   * will disable this control
   */
  #disableModule() {
    this.#switchState(GeolocationControlManager.STATE_ERROR);
    this.#button.removeEventListener("click", this.#click.bind(this));
  }

  /**
   * Centers the view on the latest location
   */
  #focusLocation() {
    if (
      this.#latest_location == null ||
      this.#state != GeolocationControlManager.STATE_LOCKED
    )
      return;
    map.fitBounds(
      LngLatBounds.fromLngLat(
        new LngLat(
          this.#latest_location.coords.longitude,
          this.#latest_location.coords.latitude
        ),
        this.#latest_location.coords.accuracy
      ),
      { duration: 250, maxZoom: 16 }
    );
  }

  #watch_position_callback(location) {
    if (this.#state == "loading") {
      this.#switchState(GeolocationControlManager.STATE_LOCKED);
    }
    this.#latest_location = location;
    // Feature to draw on map
    if (this.#latest_location.coords.accuracy < this.#high_accuracy_threshold_m) {
      map.showUserPosition(
        new LngLat(
          this.#latest_location.coords.longitude,
          this.#latest_location.coords.latitude
        ),
        this.#latest_location.coords.heading,
        this.#latest_location.coords.accuracy
      );
    } else {
      map.hideUserPosition();
    }
    this.#focusLocation();
  }

  #stopWatchPosition() {
    if (this.#watch_id != null) {
      gpsManager.clearWatch(this.#watch_id);
      this.#watch_id = null;
      map.hideUserPosition();
    }
  }

  /**
   * Toggles whether or not geolocation control will be unlocked
   * on user interaction
   * @param {boolean} active
   */
  #toggleUnlockListeners(active) {
    if (active) {
      this.#toggleUnlockListeners(false); // Make sure listeners are not already enabled
      map.once("drag", this.#unlock);
      this.#map.addEventListener("keyup", this.#unlock, {
        once: true,
      });
      this.#map.addEventListener("wheel", this.#unlock, {
        once: true,
      });
      this.#map.addEventListener("click", this.#unlock, {
        once: true,
      });
    } else {
      map.off("drag", this.#unlock);
      this.#map.removeEventListener("keyup", this.#unlock, {
        once: true,
      });
      this.#map.removeEventListener("wheel", this.#unlock, {
        once: true,
      });
      this.#map.removeEventListener("click", this.#unlock, {
        once: true,
      });
    }
  }
}
