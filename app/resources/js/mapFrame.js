import { Search } from "./Search"
import { map } from "./maps/MetaGerMapModule";

// OSM IDS
loadResults().then(results => {
    console.log("success", results);
    map
})

async function loadResults() {
    let osm_ids = document.querySelector("meta[name=start-query]");
    if (!osm_ids || !osm_ids.content.match(/^[NWR\d,]+/)) {
        return Promise.reject(`${osm_ids} are no valid OSM Objects`);
    }

    return Search.LOOKUP(osm_ids.content.split(","));
}