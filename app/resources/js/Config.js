import { bbox, bboxPolygon, circle } from "@turf/turf";
import { LngLatBounds, CenterZoomBearing, LngLat } from "maplibre-gl";
import { gpsManager } from "./GpsManager";

export const config = {
  localization: {
    current_locale: document.querySelector("html").getAttribute("lang"),
    fallback_locale: "en",
    supported_locales: JSON.parse(atob(document.querySelector("meta[name=supported-locales]").content))
  },
  init: {
    /** @type {LngLatBounds} */
    bbox: null,
    viewbox: null,
    bounded: null,
    query: null,
    /** @type {CenterZoomBearing} */
    reverse: null,
    vehicle: null,
    waypoints: null,
    navigation_active: false,
    module: null,
  },
};

let start_bbox = document.querySelector("meta[name=start-bbox]");
if (start_bbox) {
  try {
    start_bbox = JSON.parse(start_bbox.content);
    config.init.bbox = new LngLatBounds(start_bbox[0].concat(start_bbox[1]));
  } catch (error) {
    // Probably no valid JSON
    console.error(`${start_bbox.content} is not a valid JSON boundingbox`);
  }
} else {
  // No starting bbox given. We will try to figure out a good
  // map position: Check if we already have permission for geolocation
  await new Promise((resolve, reject) => {
    gpsManager.hasPermissions().then(has_permissions => {
      if (!has_permissions) return reject("No Geolocation Permission");
      gpsManager.getCurrentPosition(location => {
        let min_radius = location.coords.accuracy < 500 ? 500 : 20000;
        let radius = Math.max(min_radius, location.coords.accuracy) / 1000.0;
        let bbox_polygon = bbox(circle([location.coords.longitude, location.coords.latitude], radius));
        config.init.bbox = new LngLatBounds(bbox_polygon);
        resolve();
      }, error => {
        return reject(error);
      }, {
        enableHighAccuracy: false,  // High accuracy location results usually take too long. We want a quick and rough position
        timeout: 500,         // As this is a blocking request we won't wait for too long for a location result
        maximumAge: 3600000,  // Accept old position up to one hour
      });
    });
  }).catch(error => {
    // No geolocation permission or timeout probably
    // Maybe we have a maxmind geoip parsed
    let start_geoip = document.querySelector("meta[name=start-geoip]");
    if (start_geoip) {
      try {
        let geoip = JSON.parse(start_geoip.content);
        let radius = Math.max(20000, geoip.accuracy) / 1000.0;
        let bbox_polygon = bbox(circle([geoip.longitude, geoip.latitude], radius));
        config.init.bbox = new LngLatBounds(bbox_polygon);
      } catch (error) {
        console.error(error);
      }

    } else {
      console.error(error);
    }
  });
}

let viewbox = document.querySelector("meta[name=start-viewbox]");
if (viewbox) {
  // Parse viewbox
  try {
    viewbox = JSON.parse(viewbox.content);
    viewbox = LngLatBounds.convert(viewbox);
    config.init.viewbox = viewbox;
  } catch (e) { }
}

let bounded = document.querySelector("meta[name=start-bounded]");
if (bounded) {
  config.init.bounded = bounded.content;
}

let query = document.querySelector("meta[name=start-query]");
if (query) {
  config.init.query = query.content;
}
// Parse reverse
let reverse_lon = document.querySelector("meta[name=start-reverselon]");
let reverse_lat = document.querySelector("meta[name=start-reverselat]");
let reverse_zoom = document.querySelector("meta[name=start-reversezoom]");

if (reverse_lon && reverse_lat && reverse_zoom) {
  config.init.reverse = {
    bearing: 0,
    center: new LngLat(parseFloat(reverse_lon.content), parseFloat(reverse_lat.content)),
    zoom: parseFloat(reverse_zoom.content),
  };
}
if (reverse_lon) {
  config.init.reverse_lon = parseFloat(reverse_lon.content);
}
if (reverse_lat) {
  config.init.reverse_lat = parseFloat(reverse_lat.content);
}
if (reverse_zoom) {
  config.init.reverse_zoom = reverse_zoom.content;
}
let vehicle = document.querySelector("meta[name=start-vehicle]");
if (vehicle) {
  config.init.vehicle = vehicle.content;
}
let waypoints = document.querySelector("meta[name=start-waypoints]");
if (waypoints) {
  config.init.waypoints = waypoints.content;
}

let navigation_active = document.querySelector("meta[name=navigation_active]");
if (navigation_active) {
  config.init.navigation_active = true;
}

let module = document.querySelector("meta[name=module]");
if (module) {
  config.init.module = module.content;
}
