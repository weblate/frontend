import { feature, featureCollection, lineString, point } from "@turf/turf";
import { gpsManager } from "./GpsManager";
import { navigationModule } from "./NavigationModule";
import { Result } from "./Result";
import { switchModule } from "./app";
import { map } from "./maps/MetaGerMapModule";
import { Route } from "./Route";
import { LngLat, LngLatBounds } from "maplibre-gl";
import { formatDistance, formatDuration } from "./utils";
import { RenderOptions } from "./maps/RenderOptions";
import { Maplibre } from "./maps/Maplibre";
import { Search } from "./Search";
import { config } from "./Config";

class RouteFinder {
  htmlmodule = document.getElementById("route-finder-addon");
  searchinput = this.htmlmodule.querySelector("input#search-waypoint-input");
  navigation_active = false;
  navigation_available = false;
  // Callbacks
  moveend_callback = this.updateState.bind(this);
  click_callback = this.handleMapClick.bind(this);

  lastQuery = "";

  vehicle = "guess";
  waypoints = [];
  routes = [];

  map_layer_id = "routefinder"; // Source where all Features of the Routefinder are displayed in
  map_layer_id_route_active = `${this.map_layer_id}_active`;
  map_layer_id_route_inactive = `${this.map_layer_id}_inactive`;

  markers = [];

  constructor() {
    this.searchinput.onkeyup = (e) => {
      let query = this.htmlmodule.querySelector(
        "input#search-waypoint-input"
      ).value;
      switchModule("search", {
        query: query,
        search: true,
        stateupdates: false,
        exit_callback: () => {
          switchModule("route-finding");
        },
      });
    };

    this.htmlmodule.querySelector(".total-info .focus").onclick =
      this.fitRoutesOnMap.bind(this);

    this.htmlmodule
      .querySelectorAll("input[name=vehicle]")
      .forEach((element) => {
        element.onchange = (event) => {
          this.vehicle = element.value;
          this.calculateRoutes()
            .then(() => this.updateInterface())
            .then(() => this.fitRoutesOnMap());
        };
      });

    this.htmlmodule.querySelector("#start-navigation").onclick = (e) =>
      this.startNavigation();

    // Add EventListener to Exit Button
    this.htmlmodule.querySelector("#vehicle-chooser >button.close").onclick = (
      e
    ) => {
      // Remove all waypoints
      while (this.waypoints.length > 0) {
        this.removeWaypoint(0);
      }
      switchModule("search");
    };
  }

  async enable(
    vehicle = null,
    waypoints = [],
    mapposition = null,
    append = true,
    navigation_active = false
  ) {
    this.htmlmodule.classList.add("active");
    this.searchinput.value = "";

    map.off("moveend", this.moveend_callback);
    map.on("moveend", this.moveend_callback);

    map.off("click", this.click_callback);
    map.on("click", this.click_callback);

    if (vehicle) this.vehicle = vehicle;
    this.navigation_active = navigation_active;
    // supplied waypoints will be added to the existing list unless append is defined and false
    if (!append) {
      while (this.waypoints.length > 0) {
        this.removeWaypoint(0);
      }
    }

    let waypoint_load_promises = [];
    for (const [index, waypoint] of waypoints.entries()) {
      let new_waypoint = null;

      if (waypoint instanceof Result) {
        new_waypoint = waypoint;
      } else if (waypoint.startsWith("gps")) {
        new_waypoint = new Result(waypoint, false, Result.SOURCE_GPS);
      } else if (typeof waypoint == "object" && waypoint.load_data) {
        new_waypoint = new Result(
          waypoint.load_data,
          false,
          waypoint.source,
          waypoint.route
        );
      } else {
        let detail_match = waypoint.match(/^(N|W|R)(\d+)$/);
        if (detail_match) {
          let result = await Search.LOOKUP([waypoint], false);
          if (result.length > 0) new_waypoint = result[0];
        }
      }
      if (new_waypoint) {
        this.addWaypoint(new_waypoint);
        if (!new_waypoint.loaded) {
          waypoint_load_promises.push(new_waypoint.load()); // Now that all event listeners are registered we can load the waypoint
        }
      }
    }

    // Add GPS options
    if (
      this.waypoints.length == 1 &&
      !this.waypoints[0].source.startsWith("gps")
    ) {
      let new_waypoint = new Result("gps", false, Result.SOURCE_GPS);
      this.addWaypoint(new_waypoint, true);
      waypoint_load_promises.push(new_waypoint.load());
    }

    if (waypoint_load_promises.length == 0) {
      return this.calculateRoutes().then((all_routes_calculated) => {
        this.updateInterface();
        if (!mapposition) {
          this.fitRoutesOnMap();
        }
        if (this.navigation_active && all_routes_calculated) {
          this.startNavigation();
        }
      });
    } else {
      Promise.all(waypoint_load_promises).then(() => {
        return this.calculateRoutes().then((all_routes_calculated) => {
          this.updateInterface();
          if (!mapposition) {
            this.fitRoutesOnMap();
          }
          if (this.navigation_active && all_routes_calculated) {
            this.startNavigation();
          }
        });
      });
    }

    if (mapposition) {
      mapposition = new LngLatBounds(mapposition._sw, mapposition._ne);
      let pos = map.cameraForBounds(mapposition);
      map.easeTo(
        {
          center: pos.center,
          zoom: pos.zoom,
        },
        { user: false }
      );
    }
  }

  removeWaypoint(index) {
    if (this.waypoints.length < index - 1) return;

    if (this.waypoints[index].geolocation_watch_id) {
      gpsManager.clearWatch(this.waypoints[index].geolocation_watch_id);
    }
    if (this.waypoints[index].route) this.waypoints[index].route.hidePopups();
    if (
      index > 0 &&
      this.waypoints[index - 1].route &&
      this.waypoints[index - 1].route.data
    ) {
      this.waypoints[index - 1].route.hidePopups();
      this.waypoints[index - 1].route = null;
    }
    this.waypoints.splice(index, 1);
  }

  addWaypoint(waypoint, first = false) {
    waypoint.dom_element.classList.add("route-finder");
    if (first) {
      this.waypoints.unshift(waypoint);
    } else {
      this.waypoints.push(waypoint);
    }
    this.updateInterface();
    // Register Event handler for load event
    waypoint.dom_element.addEventListener(
      "load",
      async (e) => {
        this.updateInterface();
      },
      { once: true }
    );
    // Register Event handler for gps updates
    if ((first || this.waypoints.length == 1) && waypoint.source == "gps") {
      waypoint.dom_element.addEventListener(
        "update",
        this.gpsUpdate.bind(this)
      );
      waypoint.dom_element.addEventListener("navigation_available", (e) => {
        if (map instanceof Maplibre) {
          // Disable Navigation for other backends than maplibre
          // as they won't support pitch/rotation that well
          this.navigation_available = true;
          this.updateInterface();
        }
      });
      // Check if Navigation already is set to available
      if (waypoint.navigation_available) {
        waypoint.dom_element.dispatchEvent(new Event("navigation_available"));
      }
    }
  }

  gpsUpdate(e) {
    this.updateInterface();
    // Check if the route needs to be recalculated
  }

  async calculateRoutes() {
    let routes_calculated = 0;
    for (const [index, waypoint] of this.waypoints.entries()) {
      // If waypoint is the last make sure there is no route calculated
      if (index == this.waypoints.length - 1) {
        delete waypoint.route;
        continue;
      }
      // Check if the waypoint has a route calculated and is correct
      if (
        waypoint.route &&
        waypoint.route.profile == this.vehicle &&
        waypoint.route.waypoint_from_id == waypoint.toString() &&
        waypoint.route.waypoint_to_id == this.waypoints[index + 1].toString()
      ) {
        routes_calculated++;
        continue;
      }

      // Check if both waypoints are already loaded
      if (!waypoint.loaded || !this.waypoints[index + 1].loaded) {
        delete waypoint.route;
        continue;
      }
      if (waypoint.route) waypoint.route.hidePopups();
      waypoint.route = new Route(this.vehicle);

      await waypoint.route
        .calculateRoute(this.waypoints[index], this.waypoints[index + 1])
        .then(() => {
          if (this.vehicle == "guess") {
            this.vehicle = waypoint.route.profile;
            this.htmlmodule.querySelector(
              `input[type=radio][value=${this.vehicle}]`
            ).checked = true;
          }
        });
      routes_calculated++;
    }
    if (routes_calculated == this.waypoints.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  updateInterface() {
    this.clearRouteContainer();
    this.updateRouteContainer();
    this.updateMap();
  }

  updateRouteContainer() {
    if (this.waypoints.length > 2) {
      // Update total route information
      let route_count = 0;
      let duration = 0;
      let distance = 0;
      for (const waypoint of this.waypoints) {
        if (!waypoint.route || !waypoint.route.data) continue;
        route_count++;
        duration += waypoint.route.data.paths[waypoint.route.selected].time;
        distance += waypoint.route.data.paths[waypoint.route.selected].distance;
      }
      this.htmlmodule.querySelector(".total-info .duration").textContent =
        formatDuration(duration);
      this.htmlmodule.querySelector(".total-info .distance").textContent =
        formatDistance(distance);
      if (route_count >= 2) this.htmlmodule.classList.add("multiroute");
      if (route_count >= 1 && this.navigation_available) {
        this.htmlmodule.classList.add("navigation-enabled");
      }
    } else {
      this.htmlmodule.classList.remove("multiroute");
    }

    // Enable Navigation button if conditions are met
    if (this.waypoints.length >= 2) {
      let route_count = 0;

      for (const waypoint of this.waypoints) {
        if (!waypoint.route || !waypoint.route.data) continue;
        route_count++;
      }
      if (route_count >= 1 && this.navigation_available) {
        this.htmlmodule.classList.add("navigation-enabled");
      }
    }

    // Update vehicle
    if (this.vehicle != "guess")
      this.htmlmodule.querySelector(
        `input[name=vehicle][value=${this.vehicle}`
      ).checked = true;

    let route_interface_container = this.htmlmodule.querySelector(
      "#waypoint-list-container"
    );

    // Add a button to add GPS Position if available
    let gps_added = false;
    for (const waypoint of this.waypoints) {
      if (waypoint.source == "gps") gps_added = true;
    }
    if (!gps_added) {
      let add_gps_position_container = document.createElement("a");
      add_gps_position_container.classList.add("add-gps");
      add_gps_position_container.textContent = "+ Eigene Position hinzufügen";
      route_interface_container.appendChild(add_gps_position_container);
      add_gps_position_container.onclick = (e) => {
        let gps_waypoint = new Result("gps", false, Result.SOURCE_GPS);
        this.addWaypoint(gps_waypoint, true);
        gps_waypoint
          .load()
          .then(() => this.calculateRoutes())
          .then(() => {
            this.updateInterface();
            this.updateState();
          });
      };
    }

    for (const [index, waypoint] of this.waypoints.entries()) {
      if (
        waypoint.error_code == Result.ERROR_GEOLOCATION_PERMISSION_DENIED ||
        waypoint.error_code == Result.ERROR_GEOLOCATION_TIMEOUT
      ) {
        // User denied Geolocation Access. Remove GPS waypoint
        this.waypoints.splice(index, 1);
        this.updateInterface();
        this.updateState();
        return;
      }
      route_interface_container.appendChild(waypoint.dom_element);
      if (waypoint.loaded) {
        // Delete Waypoint callback
        waypoint.dom_element.querySelector(".remove-waypoint").onclick = (
          e
        ) => {
          this.removeWaypoint(index);
          this.calculateRoutes()
            .then(() => this.updateInterface())
            .then(() => this.fitRoutesOnMap())
            .then(() => this.updateState());
        };
        waypoint.dom_element
          .querySelectorAll(".reorder-waypoints > a")
          .forEach((element) => {
            element.onclick = (click) => {
              let up = click.target.classList.contains("down") ? false : true;
              if (up && index == 0) return; // Do not swap up if first element
              if (!up && index == this.waypoints.length - 1) return; // Do not swap down if last element
              // Swap waypoint indexes
              if (up) {
                [this.waypoints[index - 1], this.waypoints[index]] = [
                  this.waypoints[index],
                  this.waypoints[index - 1],
                ];
              } else {
                [this.waypoints[index], this.waypoints[index + 1]] = [
                  this.waypoints[index + 1],
                  this.waypoints[index],
                ];
              }
              this.calculateRoutes()
                .then(() => this.updateInterface())
                .then(() => this.fitRoutesOnMap())
                .then(() => this.updateState());
            };
          });
      }
      if (waypoint.route && waypoint.route.data) {
        route_interface_container.appendChild(waypoint.route.dom_node);
        waypoint.route.dom_node.onchange = () => {
          this.updateInterface();
        };
      }
    }
  }
  clearRouteContainer() {
    this.htmlmodule.classList.remove("navigation-enabled");
    let route_interface_container = this.htmlmodule.querySelector(
      "#waypoint-list-container"
    );
    route_interface_container.textContent = "";
  }

  /**
   * Prints all waypoints and routes on the map
   */
  updateMap() {
    // Add the markers to the map
    let geojson_features = [];
    let geojson_route_active_features = [];
    let geojson_route_inactive_features = [];

    for (const [index, waypoint] of this.waypoints.entries()) {
      if (!waypoint.loaded) continue;
      let show_marker = !this.navigation_active || index > 0;
      let chr = String.fromCharCode(65 + index);
      let new_feature = waypoint.geojson
        ? feature(waypoint.geojson, { text: chr })
        : null;

      if (
        new_feature == null ||
        !["Point", "MultiPoint"].includes(new_feature.geometry.type)
      ) {
        // Add A marker
        let new_point = point(
          [
            parseFloat(waypoint.location.lng),
            parseFloat(waypoint.location.lat),
          ],
          { text: chr }
        );
        if (new_feature != null) geojson_features.push(new_feature);
        if (show_marker) geojson_features.push(new_point);
      } else if (show_marker && new_feature != null) {
        geojson_features.push(new_feature);
      }
      waypoint.addMarkerToDom(index);
      if (waypoint.route && waypoint.route.data) {
        for (const [index, path] of waypoint.route.data.paths.entries()) {
          let feature = lineString(path.points.coordinates);
          if (waypoint.route.selected == index) {
            geojson_route_active_features.push(feature);
          } else {
            geojson_route_inactive_features.push(feature);
          }
        }
      }
    }
    // Draw the waypoints
    map.drawFeatures(
      this.map_layer_id,
      featureCollection(geojson_features),
      new RenderOptions(
        {
          point: {
            "text-translate": [0, -19],
            "text-color": "#ffffff",
          },
          line: {
            "line-color": "rgb(255,127,0)",
            "line-width": 5,
          },
          fill: {
            "fill-color": "#593008",
            "fill-outline-color": "rgb(255,127,0)",
            "fill-opacity": 0.3,
          },
        },
        {
          point: {
            "icon-image": "result-marker",
            "icon-size": 1,
            "icon-anchor": "bottom",
            "icon-ignore-placement": true,
            "icon-rotate": 0,
            "text-field": ["get", "text"],
            "text-line-height": 1,
            "text-padding": 0,
            "text-anchor": "bottom",
            "text-justify": "center",
            "text-ignore-placement": true,
            "text-rotate": 0,
          },
        },
        true
      )
    );
    // Draw Inactive Routes
    map.drawFeatures(
      this.map_layer_id_route_inactive,
      featureCollection(geojson_route_inactive_features),
      new RenderOptions(
        {
          line: {
            "line-color": "rgb(177, 180, 185)",
            "line-width": 5,
            "line-opacity": 0.6,
          },
        },
        {}
      )
    );
    map.drawFeatures(
      this.map_layer_id_route_active,
      featureCollection(geojson_route_active_features),
      new RenderOptions(
        {
          line: {
            "line-color": "#2e8bc0",
            "line-width": 5,
            "line-opacity": 1,
          },
        },
        {}
      )
    );
  }

  clearMap() {
    map.removeFeatures(this.map_layer_id);
    map.removeFeatures(this.map_layer_id_route_active);
    map.removeFeatures(this.map_layer_id_route_inactive);
  }

  startNavigation() {
    if (this.waypoints[0].source != "gps" || !this.waypoints[0].route) return;
    gpsManager.clearWatch(this.waypoints[0].geolocation_watch_id);
    this.waypoints[0].geolocation_watch_id = null;

    this.navigation_active = true;
    this.htmlmodule.classList.remove("active"); // Route Overview is not shown while navigating
    map.off("click", this.click_callback); // No reverse searches whilst navigating
    map.off("moveend", this.moveend_callback); // No state updates whilst navigating
    this.updateState(); // But a final state update
    navigationModule.enable();
    this.updateInterface();
  }
  stopNavigation() {
    this.navigation_active = false;
    this.htmlmodule.classList.add("active"); // Route Overview is not shown while navigating
    map.on("click", this.click_callback); // No reverse searches whilst navigating
    map.on("moveend", this.moveend_callback); // No state updates whilst navigating
    this.updateState(); // But a final state update
  }
  handleMapClick(event) {
    if (event.coordinate) {
      event.lngLat = new LngLat(event.coordinate[0], event.coordinate[1]);
    }
    let search_params = {
      reverse: {
        center: event.lngLat,
        zoom: Math.round(map.getZoom()),
      },
      stateupdates: false,
      search: true,
      exit_callback: () => {
        switchModule("route-finding");
      },
    };
    switchModule("search", search_params);
  }

  fitRoutesOnMap() {
    if (this.waypoints.length <= 1) return;
    let bbox_collection = null;
    for (const waypoint of this.waypoints) {
      if (!waypoint.route) continue;
      console.log(waypoint.route.data.paths[waypoint.route.selected].bbox);
      let bbox = waypoint.route.data.paths[waypoint.route.selected].bbox;
      bbox = turf.bboxPolygon([bbox[0], bbox[1], bbox[2], bbox[3]]);
      if (bbox_collection)
        bbox_collection = turf.featureCollection([
          ...bbox_collection.features,
          bbox,
        ]);
      else bbox_collection = turf.featureCollection([bbox]);
    }
    if (bbox_collection != null) {
      let bbox = turf.bbox(bbox_collection);
      map.fitBounds(new LngLatBounds(bbox), {
        duration: 250,
        padding: 25,
      });
    }
  }

  clearInterface() {
    this.clearRouteContainer();
    this.clearMap();
  }
  /**
   * Function called when map is loaded and GPS Features become available
   */
  enableGps() { }

  /**
   * Cleanup Code when module is closed
   */
  exit() {
    this.htmlmodule.classList.remove("active");

    this.clearInterface();

    map.off("moveend", this.moveend_callback);
    map.off("click", this.click_callback);
  }

  updateState(event) {
    if (
      event &&
      ((event.hasOwnProperty("user") && event.user == false) ||
        (event.hasOwnProperty("0") &&
          ResizeObserverEntry.prototype.isPrototypeOf(event[0])))
    ) {
      return;
    }
    let url_vehicle = this.vehicle !== undefined ? this.vehicle : "guess";
    let url = window.location.origin + `/${config.localization.current_locale}/route/${url_vehicle}/`;

    let waypoint_strings = [];
    for (const waypoint of this.waypoints) {
      waypoint_strings.push(waypoint.toString());
    }
    url += waypoint_strings.join(";");

    let current_pos = map.getBounds();
    // Check if current_pos changed enough
    if (
      window.history &&
      window.history.state &&
      window.history.state.module == "route-finder" &&
      window.history.state.navigation_active == this.navigation_active
    ) {
      let old_pos = new LngLatBounds(
        window.history.state.mapposition._sw,
        window.history.state.mapposition._ne
      );
      let old_pos_camera = map.cameraForBounds(old_pos);
      let new_pos_camera = map.cameraForBounds(current_pos);
      if (
        old_pos_camera.center.distanceTo(new_pos_camera.center) <= 250 &&
        Math.abs(old_pos_camera.zoom - new_pos_camera.zoom) < 0.5
      ) {
        current_pos = old_pos;
      }
    }

    url += `/${JSON.stringify(current_pos.toArray())}`;

    if (this.navigation_active) {
      url += "?navigation";
    }

    // Make Waypoints serializable i.e. strip any dom nodes
    let waypoints = [];
    for (const waypoint of this.waypoints) {
      let route = null;
      if (waypoint.route) {
        route = {
          data: waypoint.route.data,
          profile: waypoint.route.profile,
        };
      }
      waypoints.push({
        data: waypoint.load_data,
        route: route,
        source: waypoint.source,
      });
    }

    let state = {
      module: "route-finder",
      mapposition: current_pos,
      waypoints: waypoints,
      vehicle: this.vehicle,
      navigation_active: this.navigation_active,
      timestamp: Date.now(),
    };

    if (
      window.history.state == undefined ||
      window.location.href == url ||
      Date.now() - window.history.state.timestamp < 100
    ) {
      window.history.replaceState(state, "", url);
    } else {
      window.history.pushState(state, "", url);
    }
  }
}

export const routeModule = new RouteFinder();
