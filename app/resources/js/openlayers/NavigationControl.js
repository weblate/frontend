import Control from "ol/control/Control";

export class NavigationControl extends Control {
    constructor(opt_options) {
        const options = opt_options || {};

        let container = document.createElement("div");
        container.classList.add("nav-opener", "ol-unselectable", "ol-control");

        let button = document.createElement("button");
        button.textContent = "≡";
        container.appendChild(button);

        button.onclick = e => {
            let opener = document.querySelector("#nav-opener");
            if (opener) {
                if (opener.checked) {
                    opener.checked = false;
                } else {
                    opener.checked = true;
                }
            }
        }

        super({
            element: container,
            target: options.target
        });
    }
}