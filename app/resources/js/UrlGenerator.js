import { config } from "./Config";

export class UrlGenerator {
    /**
     * Generates a localized URL to the specified path
     * 
     * @param {string} path 
     * @param {string} locale
     * 
     * @returns {string} The localized URL
     */
    static to(path, locale = config.localization.current_locale) {
        if (!config.localization.supported_locales.includes(locale)) {
            locale = config.localization.fallback_locale;
        }
        for (let supported_locale of config.localization.supported_locales) {
            supported_locale = supported_locale.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"); // Regex escape
            let reg = new RegExp(`^\/${supported_locale}`);
            path = path.replace(reg, "");
        }
        return `/${locale}${path}`;
    }
}