/**
 * Holds render options for drawing features on the map
 * i.e. line color etc
 */

export class RenderOptions {
    paint = { point: {}, line: {}, fill: {} };
    layout = { point: {}, line: {}, fill: {} };   // Layout Property (https://maplibre.org/maplibre-style-spec/layers/#layout-property)
    polygonLines = false;
    constructor(paint, layout, polygonLines = false) {
        this.polygonLines = polygonLines;
        this.paint = { ...this.paint, ...paint };
        this.layout = { ...this.layout, ...layout };
    }

    static SYMBOL_DEFAULTS() {
        let options = new RenderOptions({}, {
            "icon-image": "marker",
        });
        return this;
    }
}