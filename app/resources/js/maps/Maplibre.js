import { config } from "../Config";
import { GeolocationControl } from "../maplibre/GeolocationControl";
import { MetaGerMap } from "./MetaGerMap";
import {
  AttributionControl,
  LngLat,
  Map,
  NavigationControl,
  Popup,
  ScaleControl,
  setRTLTextPlugin,
} from "maplibre-gl";
import { RenderOptions } from "./RenderOptions";
import { circle, featureCollection, point } from "@turf/turf";
import { NavbarControl } from "../maplibre/NavbarControl";
import { detectTheme } from "../Theme";

export class Maplibre extends MetaGerMap {
  /** @type {Map} */
  _map = null;
  #map_params = {
    container: "map",
    hash: false,
    style: `${this._tileserver_host}/styles/osm-bright/style.json?optimize=true`,
    attributionControl: false, // We'll add our own
    locale: "en",
  };

  #layer_types = ["fill", "line", "point"];

  #navigationControl = null;
  #geolocationControl = null;
  #navbarControl = null;
  #scaleControl = null;
  #attributionControl = null;

  constructor() {
    super();
    setRTLTextPlugin(`${this._tileserver_host}/mapbox-gl-rtl-text.js`);

    if (config.init.bbox != null) {
      this.#map_params.bounds = config.init.bbox;
    } else {
      // Set default bbox to world view
      this.#map_params.bounds = [
        [-186.98256361380203, -50.455768996590855],
        [182.64311007683932, 76.29018812097286],
      ];
    }
  }

  async load() {
    if (detectTheme() == "dark") {
      this.#map_params.style = `${this._tileserver_host}/styles/osm-dark/style.json?optimize=true`;
    }

    this._map = new Map(this.#map_params);

    // Load additional images used in the map
    // Marker Icon
    let image = await this._map.loadImage("/img/marker-icon.png");
    this._map.addImage("result-marker", image.data);

    image = await this._map.loadImage("/img/nav.png");
    this._map.addImage("userposition", image.data);
    this.#addControls();
    return new Promise((resolve, reject) => {
      if (!this._map.loaded()) {
        this._map.once("load", (e) => {
          this.#switchLanguage(config.localization.current_locale);
          resolve();
        });
      } else {
        this.#switchLanguage(config.localization.current_locale);
        resolve();
      }
    });
  }

  /**
   * Switches preferred language for map labels
   * 
   * @param {string} language_code two letter language code
   */
  #switchLanguage(language_code) {
    let layers = [
      'place-continent',
      "place-country-1",
      "place-country-2",
      "place-country-3",
      'place-country-other',
      'place-city-capital',
      'place-city',
      'place-town',
      'place-village',
      'place-other',
      'airport-label-major',
      'highway-name-major',
      'highway-name-minor',
      'highway-name-path',
      'poi-railway',
      'poi-level-1',
      'poi-level-2',
      'poi-level-3',
      'water-name-other',
      'water-name-ocean',
      'water-name-lakeline',
      'waterway-name',
    ];

    for (let layer_id of layers) {
      try {
        let spacer = '\n';
        if (['highway', 'waterway-name'].some(element => layer_id.includes(element))) {
          spacer = ' ';
        }
        this._map.setLayoutProperty(layer_id, 'text-field',
          ['concat', ['coalesce', ['get', `name:${language_code}`], ['get', 'name:latin']], spacer, ['get', 'name:nonlatin']],
        );
      } catch (error) { }
    }
  }

  #addControls() {
    this.#navigationControl = new NavigationControl({
      showCompass: true,
      showZoom: true,
      visualizePitch: true,
    });
    this._map.addControl(this.#navigationControl, "bottom-left");
    // Geolocation Control
    this.#geolocationControl = new GeolocationControl();
    this._map.addControl(this.#geolocationControl, "bottom-left");
    this.#navbarControl = new NavbarControl();
    this._map.addControl(this.#navbarControl, "top-right");
    this.#attributionControl = new AttributionControl();
    this._map.addControl(this.#attributionControl, "bottom-right");
    this.#scaleControl = new ScaleControl();
    this._map.addControl(this.#scaleControl, "bottom-right");
  }

  async switchTheme(theme) {
    let theme_name = theme == "light" ? "osm-bright" : "osm-dark";
    this._map.setStyle(`${this._tileserver_host}/styles/${theme_name}/style.json?optimize=true`);
  }

  getBounds() {
    return this._map.getBounds();
  }

  /**
   * @returns {number}
   */
  getZoom() {
    return this._map.getZoom();
  }

  /**
   * @returns {LngLat}
   */
  getCenter() {
    return this._map.getCenter();
  }

  /**
   * @returns {number}
   */
  getBearing() {
    return this._map.getBearing();
  }

  /**
   * Returns the map viewport height in pixels
   *
   * @returns {number}
   */
  getHeight() {
    return this._map.getCanvas().height;
  }

  /**
   * Returns Pixel coordinates for specified geographic coordinates
   *
   * @param {LngLat} coordinates
   */
  project(coordinates) {
    return this._map.project(coordinates);
  }

  /**
   *
   * @param {import("maplibre-gl").LngLatBoundsLike} bounds
   * @param {import("maplibre-gl").FitBoundsOptions} options
   */
  fitBounds(bounds, options) {
    return this._map.fitBounds(bounds, options);
  }

  /**
   *
   * @param {import("maplibre-gl").AnimationOptions|CenterZoomBearing} options
   * @param {*} eventData
   */
  easeTo(options, eventData) {
    return this._map.easeTo(options, eventData);
  }

  /**
   *
   * @param {import("maplibre-gl").JumpToOptions} options
   * @param {*} eventData
   */
  jumpTo(options, eventData) {
    return this._map.jumpTo(options, eventData);
  }

  /**
   * resets pitch and rotation of the map
   */
  resetNorthPitch() {
    this._map.resetNorthPitch();
  }

  cameraForBounds(boundingBox, options) {
    return this._map.cameraForBounds(boundingBox, options);
  }

  removeFeatures(layer_id) {
    for (let layer_type of this.#layer_types) {
      let layer_type_id = `${layer_id}_${layer_type}`;
      // Remove source and layer
      if (this._map.getLayer(layer_type_id) != undefined)
        this._map.removeLayer(layer_type_id);
    }
    if (this._map.getSource(layer_id) != undefined)
      this._map.removeSource(layer_id);
  }

  /**
   * Draws a Featurecollection onto the map under a given id
   * used to be able to remove those features from the map again
   *
   * @param {string} layer_id
   * @param {*} featureCollection
   * @param {RenderOptions} render_options
   */
  drawFeatures(layer_id, featureCollection, render_options, cluster = false) {
    // Possible Feature Types: Point, Multipoint, Line, MultiLine, Polygon, Multipolygon
    let source = this._map.getSource(`${layer_id}`);
    if (source == undefined) {
      this._map.addSource(`${layer_id}`, {
        type: "geojson",
        data: featureCollection,
        cluster: cluster
      });
      source = this._map.getSource(`${layer_id}`);
    } else {
      source.setData(featureCollection);
    }

    let fillLayer = this._map.getLayer(`${layer_id}_fill`);
    let fill_type = "fill";
    if (render_options.polygonLines) {
      fill_type = "line";
    }
    if (fillLayer == undefined) {

      this._map.addLayer({
        id: `${layer_id}_fill`,
        type: fill_type,
        source: source.id,
        paint: fill_type == "fill" ? render_options.paint.fill : render_options.paint.line,
        filter: ["==", "$type", "Polygon"],
      });
      fillLayer = this._map.getLayer(`${layer_id}_fill`);
    } else {
      for (let [key, value] of Object.entries(fill_type == "fill" ? render_options.paint.fill : render_options.paint.line)) {
        this._map.setPaintProperty(`${layer_id}_fill`, key, value);
      }
    }

    let lineStringLayer = this._map.getLayer(`${layer_id}_line`);
    if (lineStringLayer == undefined) {
      this._map.addLayer({
        id: `${layer_id}_line`,
        type: "line",
        source: source.id,
        filter: ["==", "$type", "LineString"],
        paint: render_options.paint.line,
        layout: render_options.layout.line,
      });
      lineStringLayer = this._map.getLayer(`${layer_id}_line`);
    } else {
      for (let [key, value] of Object.entries(render_options.paint.line)) {
        this._map.setPaintProperty(`${layer_id}_line`, key, value);
      }
      for (let [key, value] of Object.entries(render_options.layout.line)) {
        this._map.setLayoutProperty(`${layer_id}_line`, key, value);
      }
    }

    // Points will be drawn as Markers
    // The feature can have a property `text` to change the label on the marker
    let pointLayer = this._map.getLayer(`${layer_id}_point`);
    if (pointLayer == undefined) {
      this._map.addLayer({
        id: `${layer_id}_point`,
        type: "symbol",
        source: source.id,
        filter: ["==", "$type", "Point"],
        paint: render_options.paint.point,
        layout: render_options.layout.point,
      });
      pointLayer = this._map.getLayer(`${layer_id}_point`);
    } else {
      for (let [key, value] of Object.entries(render_options.paint.point)) {
        this._map.setPaintProperty(`${layer_id}_point`, key, value);
      }
      for (let [key, value] of Object.entries(render_options.layout.point)) {
        this._map.setLayoutProperty(`${layer_id}_point`, key, value);
      }
    }
  }

  on(event_name, callback) {
    this._map.on(event_name, callback);
  }

  once(event_name, callback) {
    this._map.once(event_name, callback);
  }

  off(event_name, callback) {
    this._map.off(event_name, callback);
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleDragpan(enabled) {
    if (enabled) {
      this._map.dragPan.enable();
    } else {
      this._map.dragPan.disable();
    }
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleScrollzoom(enabled) {
    if (enabled) {
      this._map.scrollZoom.enable();
    } else {
      this._map.scrollZoom.disable();
    }
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleDragRotate(enabled) {
    if (enabled) {
      this._map.dragRotate.enable();
    } else {
      this._map.dragRotate.disable();
    }
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleKeyboard(enabled) {
    if (enabled) {
      this._map.keyboard.enable();
    } else {
      this._map.keyboard.disable();
    }
  }

  /**
   *
   * @param {LngLat} point
   * @param {number} bearing
   * @param {number} accuracy
   */
  showUserPosition(user_point, bearing, accuracy) {
    let layer_id = "userposition";
    let circle_polygon = circle(
      [user_point.lng, user_point.lat],
      accuracy / 1000.0,
      { properties: { class: layer_id } }
    );
    let user_marker = point([user_point.lng, user_point.lat], {
      class: layer_id,
      bearing: bearing,
    });

    let style = new RenderOptions(
      {
        fill: {
          "fill-color": "#593008",
          "fill-outline-color": "rgb(255,127,0)",
          "fill-opacity": 0.3,
        },
      },
      {
        point: {
          "icon-image": "userposition",
          "icon-allow-overlap": true,
          "icon-rotate": bearing ? bearing : 0,
          "icon-rotation-alignment": "map",
          "icon-size": ['interpolate', ['linear'], ['zoom'], 10, 0.15, 18, 0.75]
        },
      }
    );
    this.drawFeatures(
      layer_id,
      featureCollection([circle_polygon, user_marker]),
      style
    );
  }

  /**
   *
   * @param {LngLat} point
   * @param {number} bearing
   * @param {number} accuracy
   */
  hideUserPosition() {
    let layer_id = "userposition";
    this.removeFeatures(layer_id);
  }

  /**
   *
   * @param {HTMLDivElement} dom_element
   * @param {LngLat} position
   */
  createPopup(dom_element, position) {
    let new_popup = new Popup({
      closeButton: false,
      closeOnClick: false,
    });
    new_popup.setDOMContent(dom_element);
    new_popup.setLngLat(position);
    new_popup.addTo(this._map);
    return new_popup;
  }

  /**
   * Removes a popup previously created by createPopup
   * the popup argument is the result of the createPopup Method
   * @param {Popup} popup
   */
  removePopup(popup) {
    popup.remove();
  }

  /**
   * Toggles visibility of specified control
   *
   * @param {string} control
   * @param {boolean} enabled
   */
  toggleControl(control, enabled) {
    /** @type {import("maplibre-gl").IControl} */
    let map_control = null;
    let position = "";
    switch (control) {
      case MetaGerMap.CONTROL_ATTRIBUTION:
        map_control = this.#attributionControl;
        position = "bottom-right";
        break;
      case MetaGerMap.CONTROL_GEOLOCATE:
        map_control = this.#geolocationControl;
        position = "bottom-left";
        break;
      case MetaGerMap.CONTROL_NAVBAR:
        map_control = this.#navbarControl;
        position = "top-right";
        break;
      case MetaGerMap.CONTROL_NAVIGATION:
        map_control = this.#navigationControl;
        position = "bottom-left";
        break;
      case MetaGerMap.CONTROL_SCALE:
        map_control = this.#scaleControl;
        position = "bottom-right";
        break;
      default:
        return;
    }
    if (enabled) {
      this._map.addControl(map_control, position);
    } else {
      this._map.removeControl(map_control);
    }
  }
}
