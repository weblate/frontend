import Map from "ol/Map";
import View from "ol/View";
import { MetaGerMap } from "./MetaGerMap";
import TileLayer from "ol/layer/Tile";
import { config } from "../Config";
import {
  bboxPolygon,
  center,
  circle,
  degreesToRadians,
  featureCollection,
  point,
  radiansToDegrees,
} from "@turf/turf";
import XYZ from "ol/source/XYZ";
import { fromLonLat, useGeographic } from "ol/proj";
import VectorLayer from "ol/layer/Vector";
import { LngLat, LngLatBounds } from "maplibre-gl";
import * as Color from "ol/color";
import { RenderOptions } from "./RenderOptions";
import Style from "ol/style/Style";
import Fill from "ol/style/Fill";
import GeoJSON from "ol/format/GeoJSON";
import VectorSource from "ol/source/Vector";
import Stroke from "ol/style/Stroke";
import Icon from "ol/style/Icon";
import Text from "ol/style/Text";
import Zoom from "ol/control/Zoom";
import Attribution from "ol/control/Attribution";
import { NavigationControl } from "../openlayers/NavigationControl";
import ScaleLine from "ol/control/ScaleLine";
import { GeolocationControl } from "../openlayers/GeolocationControl";
import DragPan from "ol/interaction/DragPan";
import MouseWheelZoom from "ol/interaction/MouseWheelZoom";
import KeyboardPan from "ol/interaction/KeyboardPan";
import KeyboardZoom from "ol/interaction/KeyboardZoom";
import DragRotate from "ol/interaction/DragRotate";
import Popup from "ol-popup";
import Control from "ol/control/Control";
import { detectTheme } from "../Theme";

export class Openlayers extends MetaGerMap {
  /** @type {Map} */
  _map = null;
  map_params = null;

  _marker_image = null;
  _base_layer = null;
  _drawn_layers = [];

  constructor() {
    super();
  }

  async load() {
    this._marker_image = await new Promise((resolve) => {
      let image = new Image();
      image.onload = () => {
        resolve();
      };
      image.src = "/img/marker-icon.png";
    });
    // Add Openlayers styles
    return this.#createBaseLayer()
      .then(() => {
        useGeographic();
        this._map = new Map({
          target: "map",
          controls: [],
          layers: [
            this._base_layer,
          ],
          view: new View({
            center: [52.3758916, 9.7320104],
            zoom: 11,
            minZoom: 0,
            maxZoom: 20,
          }),
        });

        this.#addControls();
        if (config.init.bbox != null) {
          this._map
            .getView()
            .fit(
              config.init.bbox
                .toArray()[0]
                .concat(config.init.bbox.toArray()[1])
            );
        } else {
          // Set default bbox to world view
          this._map
            .getView()
            .fit([
              -186.98256361380203, -50.455768996590855, 182.64311007683932,
              76.29018812097286,
            ]);
        }
        return new Promise((resolve) => {
          this._map.once("rendercomplete", (e) => {
            document.getElementById("map").focus(); // Make sure map has initial focus
            resolve();
          });
        });
      });
  }

  #addControls() {
    this._controls.zoomControl = new Zoom({ className: "mg-ol-control-zoom" });
    this._map.addControl(this._controls.zoomControl);

    this._controls.geolocationControl = new GeolocationControl();
    this._map.addControl(this._controls.geolocationControl);

    this._controls.attributionControl = new Attribution({ collapsible: true });
    this._map.addControl(this._controls.attributionControl);

    this._controls.navigationControl = new NavigationControl();
    this._map.addControl(this._controls.navigationControl);

    this._controls.scaleControl = new ScaleLine();
    this._map.addControl(this._controls.scaleControl);
  }

  async #createBaseLayer() {
    let current_theme = detectTheme();
    let theme_name = current_theme == "light" ? "osm-bright" : "osm-dark";
    // Add Openlayers styles
    return fetch(`https://tileserver.metager.de/styles/${theme_name}.json`)
      .then((response) => response.json())
      .then((response) => {
        this._base_layer = new TileLayer({
          source: new XYZ({
            extent: [-180, -90, 180, 90],
            url: response.tiles[0],
            tilesize: [512, 512],
            attributions: response.attribution,
          }),
          preload: Infinity,
          minZoom: response.minzoom,
          maxZoom: 20,
        });
      });
  }

  async switchTheme(theme) {
    this._map.removeLayer(this._base_layer);
    this.#createBaseLayer().then(() => {
      this._map.addLayer(this._base_layer);
    });
  }

  /**
   * Toggles visibility of specified control
   *
   * @param {string} control
   * @param {boolean} enabled
   */
  toggleControl(control, enabled) {
    /** @type {Control} */
    let map_control = null;
    switch (control) {
      case MetaGerMap.CONTROL_ATTRIBUTION:
        map_control = this._controls.attributionControl;
        break;
      case MetaGerMap.CONTROL_GEOLOCATE:
        map_control = this._controls.geolocationControl;
        break;
      case MetaGerMap.CONTROL_NAVBAR:
        map_control = this._controls.navigationControl;
        break;
      case MetaGerMap.CONTROL_NAVIGATION:
        map_control = this._controls.zoomControl;
        break;
      case MetaGerMap.CONTROL_SCALE:
        map_control = this._controls.scaleControl;
        break;
      default:
        return;
    }
    if (enabled) {
      this._map.addControl(map_control);
    } else {
      this._map.removeControl(map_control);
    }
  }

  /**
   *
   * @param {import("maplibre-gl").LngLatBounds} bounds
   * @param {import("maplibre-gl").FitBoundsOptions} options
   */
  fitBounds(bounds, options) {
    let supported_options = ["duration", "maxZoom", "padding"];
    let openlayers_fit_options = {};
    for (let [key, value] of Object.entries(options)) {
      if (!supported_options.includes(key)) {
        console.warn(
          `Maplibre FitBoundsOptions key ${key} is not yet supported for Openlayers`
        );
      } else if (key == "padding") {
        if (typeof value == "number") {
          openlayers_fit_options[key] = [value, value, value, value];
        } else {
          openlayers_fit_options[key] = [
            value.top,
            value.right,
            value.bottom,
            value.left,
          ];
        }
      } else {
        openlayers_fit_options[key] = value;
      }
    }

    this._map
      .getView()
      .fit(
        bounds._sw.toArray().concat(bounds._ne.toArray()),
        openlayers_fit_options
      );
  }

  /**
   *
   * @param {import("maplibre-gl").AnimationOptions|CenterZoomBearing} options
   * @param {*} eventData
   */
  easeTo(options, eventData) {
    // Fix LngLat center object for openlayers
    options.center = options.center.toArray();
    this._map.getView().animate(options);
  }

  /**
   *
   * @param {import("maplibre-gl").JumpToOptions} options
   * @param {*} eventData
   */
  jumpTo(options, eventData) {
    options.center = [options.center.lng, options.center.lat];
    options.duration = 0;
    this._map.getView().animate(options);
  }

  /**
   * resets pitch and rotation of the map
   */
  resetNorthPitch() {
    this._map.animate({ rotation: 0 });
  }

  /**
   * @returns {LngLatBounds}
   */
  getBounds() {
    let extent = this._map.getView().calculateExtent(this._map.getSize());
    return new LngLatBounds(extent);
  }

  /**
   * @returns {number}
   */
  getZoom() {
    return this._map.getView().getZoom();
  }

  /**
   * @returns {LngLat}
   */
  getCenter() {
    let center_pos = this._map.getView().getCenter();
    return new LngLat(center_pos[0], center_pos[1]);
  }

  /**
   * @returns {number}
   */
  getBearing() {
    return radiansToDegrees(this._map.getView().getRotation());
  }

  /**
   * Returns the map viewport height in pixels
   *
   * @returns {number}
   */
  getHeight() {
    return this._map.getSize()[1];
  }

  /**
   * Returns Pixel coordinates for specified geographic coordinates
   *
   * @param {LngLat} coordinates
   */
  project(coordinates) {
    return this._map.getPixelFromCoordinate([coordinates.lng, coordinates.lat]);
  }

  /**
   *
   * @param {import("maplibre-gl").LngLatBounds} bounds
   * @returns {CenterZoomBearing}
   */
  cameraForBounds(bounds) {
    bounds = bounds.toArray();
    bounds = bounds[0].concat(bounds[1]);
    let extent = bboxPolygon(bounds);

    let center_pos = center(extent);
    let resolution = this._map.getView().getResolutionForExtent(bounds);
    let zoom = this._map.getView().getZoomForResolution(resolution);
    return {
      center: new LngLat(
        center_pos.geometry.coordinates[0],
        center_pos.geometry.coordinates[1]
      ),
      zoom: zoom,
      bearing: 0,
    };
  }

  /**
   * Draws a Featurecollection onto the map under a given id
   * used to be able to remove those features from the map again
   *
   * @param {string} layer_id
   * @param {*} featureCollection
   * @param {RenderOptions} render_options
   */
  drawFeatures(layer_id, featureCollection, render_options) {
    // Convert Maplibre Styles to Openlayers ones
    let polygonStyleOptions = {};
    if (render_options.paint.hasOwnProperty("fill")) {
      let fillPaint = render_options.paint.fill;
      let fillStyle = {};
      if (fillPaint.hasOwnProperty("fill-color")) {
        fillStyle.color = fillPaint["fill-color"];
      }
      polygonStyleOptions.fill = new Fill(fillStyle);
      if (fillPaint.hasOwnProperty("fill-opacity")) {
        // Change the color to include opacity
        let colorArray = Color.asArray(
          polygonStyleOptions.fill.getColor()
        ).slice();
        colorArray[3] = fillPaint["fill-opacity"];
        polygonStyleOptions.fill.setColor(colorArray);
      }
      if (fillPaint.hasOwnProperty("fill-outline-color")) {
        polygonStyleOptions.stroke = new Stroke({
          color: fillPaint["fill-outline-color"],
          width: 1,
        });
      }
    }
    let lineStyleOptions = {};

    let line_color = "rgb(255,127,0)";
    let line_width = 1;

    if (render_options.paint.hasOwnProperty("line")) {
      let linePaint = render_options.paint.line;
      if (linePaint.hasOwnProperty("line-color")) {
        line_color = linePaint["line-color"];
      }
      if (linePaint.hasOwnProperty("line-width")) {
        line_width = linePaint["line-width"];
      }
    }
    lineStyleOptions.stroke = new Stroke({
      color: line_color,
      width: line_width,
    });

    let pointStyleOptions = {};
    if (
      render_options.layout.hasOwnProperty("point") &&
      render_options.layout.point.hasOwnProperty("icon-image")
    ) {
      if (render_options.layout.point["icon-image"] == "result-marker") {
        let iconStyle = {};
        iconStyle.src = "/img/marker-icon.png";
        iconStyle.anchor = [0.5, 1];
        pointStyleOptions.image = new Icon(iconStyle);

        let textStyle = {};
        textStyle.text = "A";
        textStyle.offsetY = -25;
        textStyle.fill = new Fill({ color: "#ffffff" });
        textStyle.font = "bold 14px Liberation Sans";
        pointStyleOptions.text = new Text(textStyle);
      } else if (render_options.layout.point["icon-image"] == "userposition") {
        let iconStyle = {};
        iconStyle.src = "/img/nav.png";
        pointStyleOptions.image = new Icon(iconStyle);
      }
    }

    let styles = {
      Point: new Style(pointStyleOptions),
      LineString: new Style(lineStyleOptions),
      MultiLineString: new Style(lineStyleOptions),
      Polygon: new Style(polygonStyleOptions),
      MultiPolygon: new Style(polygonStyleOptions),
    };

    if (!this._drawn_layers.includes(layer_id)) {
      let source = new VectorSource({
        features: new GeoJSON().readFeatures(featureCollection),
      });

      let layer = new VectorLayer({
        updateWhileInteracting: true,
        updateWhileAnimating: true,
        source: source,
        style: (feature) => {
          if (feature.getGeometry().getType() == "Point") {
            let style = styles[feature.getGeometry().getType()].clone();
            if (feature.get("text")) {
              let text = feature.get("text");
              style.getText().setText(text);
            }
            if (feature.get("bearing")) {
              let bearing = parseInt(feature.get("bearing"));
              let bearing_radians = degreesToRadians(bearing);
              style.getImage().setRotation(bearing_radians);
            }
            return style;
          } else {
            return styles[feature.getGeometry().getType()];
          }
        },
        properties: {
          id: layer_id,
        },
      });
      layer.set("id", layer_id);
      this._map.addLayer(layer);
      this._drawn_layers.push(layer_id);
    } else {
      /** @type {VectorLayer} */
      for (let layer of this._map.getAllLayers()) {
        if (layer.get("id") != layer_id) continue;
        /** @type {VectorSource} */
        let source = layer.getSource();
        let new_features = new GeoJSON().readFeatures(featureCollection);
        source.clear(true);
        source.addFeatures(new_features);
      }
    }
  }

  removeFeatures(id) {
    if (!this._drawn_layers.includes(id)) return;
    for (let layer of this._map.getAllLayers()) {
      if (layer.get("id") != id) continue;
      this._map.removeLayer(layer);
    }

    this._drawn_layers.splice(this._drawn_layers.indexOf(id), 1);
  }

  on(event_name, callback) {
    if (event_name == "drag") {
      event_name == "pointerdrag"; // Event Name changed between maplibre and openlayers
    }
    if (event_name == "click") {
      event_name = "singleclick";
    }
    this._map.on(event_name, callback);
  }

  once(event_name, callback) {
    this._map.once(event_name, callback);
  }

  /**
   * Removes a previously added event listener
   *
   * @param {*} event_name
   * @param {*} callback
   */
  off(event_name, callback) {
    this._map.un(event_name, callback);
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleDragpan(enabled) {
    let interactions = this._map.getInteractions();
    if (enabled == false) {
      for (let interaction of interactions.getArray()) {
        if (interaction instanceof DragPan) {
          this._map.removeInteraction(interaction);
        }
      }
    } else {
      this._map.addInteraction(new DragPan());
    }
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleDragRotate(enabled) {
    let interactions = this._map.getInteractions();
    if (enabled == false) {
      for (let interaction of interactions.getArray()) {
        if (interaction instanceof DragRotate) {
          this._map.removeInteraction(interaction);
        }
      }
    } else {
      this._map.addInteraction(new DragRotate());
    }
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleScrollzoom(enabled) {
    let interactions = this._map.getInteractions();
    if (enabled == false) {
      for (let interaction of interactions.getArray()) {
        if (interaction instanceof MouseWheelZoom) {
          this._map.removeInteraction(interaction);
        }
      }
    } else {
      this._map.addInteraction(new MouseWheelZoom());
    }
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleKeyboard(enabled) {
    let interactions = this._map.getInteractions();
    if (enabled == false) {
      for (let interaction of interactions.getArray()) {
        if (
          interaction instanceof KeyboardPan ||
          interaction instanceof KeyboardZoom
        ) {
          this._map.removeInteraction(interaction);
        }
      }
    } else {
      this._map.addInteraction(new KeyboardPan());
      this._map.addInteraction(new KeyboardZoom());
    }
  }

  /**
   *
   * @param {LngLat} point
   * @param {number} bearing
   * @param {number} accuracy
   */
  showUserPosition(user_point, bearing, accuracy) {
    // Detect if Layer already exists
    let layer_id = "userposition";
    let layer = null;
    for (let temp_layer of this._map.getAllLayers()) {
      if (temp_layer.get("id") == layer_id) {
        layer = temp_layer;
      }
    }

    let circle_polygon = circle(
      [user_point.lng, user_point.lat],
      accuracy / 1000.0,
      { properties: { class: layer_id } }
    );
    let user_marker = point([user_point.lng, user_point.lat], {
      class: layer_id,
      bearing: bearing,
    });

    let render_options = new RenderOptions(
      {
        fill: {
          "fill-color": "#593008",
          "fill-outline-color": "rgb(255,127,0)",
          "fill-opacity": 0.3,
        },
      },
      {
        point: {
          "icon-image": "userposition",
        },
      }
    );
    this.removeFeatures(layer_id);
    this.drawFeatures(
      layer_id,
      featureCollection([circle_polygon, user_marker]),
      render_options
    );
  }

  /**
   *
   * @param {LngLat} point
   * @param {number} bearing
   * @param {number} accuracy
   */
  hideUserPosition() {
    this.removeFeatures("userposition");
  }

  /**
   *
   * @param {HTMLDivElement} dom_element
   * @param {LngLat} position
   */
  createPopup(dom_element, position) {
    let overlay = new Popup();
    this._map.addOverlay(overlay);
    overlay.show([position.lng, position.lat], dom_element);

    return overlay;
  }

  /**
   * Removes a popup previously created by createPopup
   * the popup argument is the result of the createPopup Method
   * @param {Popup} popup
   */
  removePopup(popup) {
    this._map.removeOverlay(popup);
  }
}
