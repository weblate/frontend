import { degreesToRadians, radiansToDegrees } from "@turf/turf";

/**
 * Parses a duration in ms and converts it to a string
 * containing the remaining hours if greater than zero
 * and the remaining minutes
 *
 * @param {int} duration_ms
 * @returns
 */
export function formatDuration(duration_ms) {
  let negative = duration_ms < 0 ? true : false;
  duration_ms = Math.abs(duration_ms);
  let duration_minutes = Math.ceil(duration_ms / 1000 / 60);

  let duration_hours = Math.floor(duration_minutes / 60);
  duration_minutes -= duration_hours * 60;

  let parsed_string = duration_hours > 0 ? duration_hours + " h " : "";
  parsed_string += duration_minutes + " min";

  return parsed_string;
}

/**
 * Parses a distance in meters and converts it to a string
 * containing the distance in km and or m stripping and rounding as needed
 *
 * @param {int} distance_meters
 * @returns
 */
export function formatDistance(distance_meters) {
  let distance_kilometers = Math.floor(distance_meters / 1000);
  distance_meters -= distance_kilometers * 1000;

  if (distance_kilometers > 10) {
    return distance_kilometers + " km";
  } else if (distance_kilometers > 0) {
    return (
      distance_kilometers + "." + Math.round(distance_meters / 100) + " km"
    );
  } else if (distance_meters > 300) {
    return Math.round(distance_meters / 50) * 50 + " m";
  } else {
    return Math.round(distance_meters / 10) * 10 + " m";
  }
}

export function getBboxFromPoints(points) {
  let bbox = [
    [null, null],
    [null, null],
  ];
  for (const point of points) {
    if (bbox[0][0] == null || bbox[0][0] > point[0]) bbox[0][0] = point[0];
    if (bbox[0][1] == null || bbox[0][1] > point[1]) bbox[0][1] = point[1];
    if (bbox[1][0] == null || bbox[1][0] < point[0]) bbox[1][0] = point[0];
    if (bbox[1][1] == null || bbox[1][1] < point[1]) bbox[1][1] = point[1];
  }
  return bbox;
}

/**
 * Compares two geographical bearings and returns the shortest distance between them
 * Takes into account that the bearing can be changed in both directions
 *
 * @param {*} bearing_a
 * @param {*} bearing_b
 */
export function compareBearings(bearing_a, bearing_b) {
  // Convert all Bearings to radians
  bearing_a = degreesToRadians(turf.bearingToAzimuth(bearing_a));
  bearing_b = degreesToRadians(turf.bearingToAzimuth(bearing_b));

  const [ax, ay] = [Math.sin(bearing_a), Math.cos(bearing_a)];
  const [bx, by] = [Math.sin(bearing_b), Math.cos(bearing_b)];

  // Cross-product above zero ?
  const sign = ay * bx - by * ax > 0 ? +1 : -1;

  // Sign * dot-product.
  return radiansToDegrees(sign * Math.acos(ax * bx + ay * by));
}
