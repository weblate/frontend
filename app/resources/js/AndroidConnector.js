// Make sure we wait for Connection to Android app
let pre_connection = false;
await new Promise(resolve => {
  if (localStorage && localStorage.getItem("android") != null) {
    let timeout = setTimeout(() => {
      resolve();
    }, 10000);
    window.addEventListener("message", message => {
      pre_connection = true;
      if (timeout) clearTimeout(timeout);
      resolve();
    }, { once: true });
    // Make sure the signal from the app on connection was not already send
    window.postMessage({
      from: "web",
      type: "message"
    });
  } else {
    resolve();
  }
});

class AndroidConnector {
  is_connected = false;

  current_position_requests = [];
  watch_position_requests = [];

  constructor() {
    if (pre_connection) this.enableConnection();
    window.addEventListener("message", this.handleMessageEvent.bind(this));
    // To be sure request another connection ping
    window.postMessage({
      from: "web",
      type: "message"
    });
  }

  handleMessageEvent(event) {

    if (!event.data) return;
    let message = event.data;
    if (!message.from || message.from != "android") return;
    if (message.type == "message") {
      this.handleMessage(message.data);
    } else if (message.type == "geolocation_current_position") {
      this.handleGeolocationCurrentPosition(message.data);
    } else if (message.type == "geolocation_watch_position") {
      this.handleGeolocationWatchPosition(message.data);
    } else {
      console.log(JSON.stringify(event.data));
    }
  }

  enableConnection() {
    this.is_connected = true;
    if (localStorage) localStorage.setItem("android", "1");
  }

  handleMessage(message) {
    if (message == "Android App connected" && !this.is_connected) {
      this.enableConnection();
      console.log("Enabled Android connector");
    }
  }

  handleGeolocationWatchPosition(data) {
    // Check if there was an error with the request
    let request_id = data.request_id;
    if (
      this.watch_position_requests.length - 1 < request_id ||
      this.watch_position_requests[request_id] == null
    )
      return; // No such Request

    if (data.type == "error") {
      this.watch_position_requests[request_id].error_callback(data.error);
    } else if (data.type == "location") {
      let location = data.location;
      if (location.coords.heading == "NaN") {
        location.coords.heading = NaN;
      }
      if (location.coords.altitudeAccuracy == "null") {
        location.coords.altitudeAccuracy = null;
      }
      if (location.coords.heading == "null") {
        location.coords.heading = null;
      }
      if (location.coords.speed == "null") {
        location.coords.speed = null;
      }
      this.watch_position_requests[request_id].success_callback(location);
    }
  }

  handleGeolocationCurrentPosition(data) {
    // Check if there was an error with the request
    let request_id = data.request_id;
    if (
      this.current_position_requests.length - 1 < request_id ||
      this.current_position_requests[request_id] == null
    )
      return; // No such Request
    if (data.type == "error") {
      this.current_position_requests[request_id].error_callback(data.error);
    } else if (data.type == "location") {
      let location = data.location;
      if (location.coords.heading == "NaN") {
        location.coords.heading = NaN;
      }
      if (location.coords.altitudeAccuracy == "null") {
        location.coords.altitudeAccuracy = null;
      }
      if (location.coords.heading == "null") {
        location.coords.heading = null;
      }
      if (location.coords.speed == "null") {
        location.coords.speed = null;
      }
      this.current_position_requests[request_id].success_callback(location);
    }

    // Remove the request
    this.current_position_requests[request_id] = null;
    // Trim all null entries from the end of the array
    this.current_position_requests = this.current_position_requests.slice(
      0,
      this.current_position_requests.length -
      this.current_position_requests
        .reverse()
        .findIndex((element) => element != null)
    );
  }

  async hasGeolocationPermission() {
    return new Promise((resolve, reject) => {
      let timeout = setTimeout(() => {
        resolve(false);
      }, 1000);
      let handleResponse = data => {
        if (!data.data) return;
        else data = data.data;
        if (data.from != "android" || data.type != "permission_granted_geolocation") return;

        clearTimeout(timeout);
        window.removeEventListener("message", handleResponse);

        if (data.data && data.data.permission_granted) {
          resolve(true);
        } else {
          resolve(false);
        }
      }
      window.addEventListener("message", handleResponse);
      window.postMessage({
        from: "web",
        type: "permission_granted_geolocation"
      });
    });
  }

  getCurrentPosition(success_callback, error_callback, options) {
    if (typeof options == "undefined") {
      console.log("fix options");
      options = {};
    }
    let new_length = this.current_position_requests.push({
      success_callback: success_callback,
      error_callback: error_callback,
      options: options,
    });
    if (options.timeout == Infinity || options.timeout == null) {
      delete options.timeout;
    }
    options.request_id = new_length - 1;
    window.postMessage({
      from: "web",
      type: "geolocation_getcurrentposition",
      data: options,
    });
  }

  watchPosition(success_callback, error_callback, options) {
    let new_length = this.watch_position_requests.push({
      success_callback: success_callback,
      error_callback: error_callback,
      options: options,
    });
    if (options.timeout == Infinity || options.timeout == null) {
      delete options.timeout;
    }
    options.request_id = new_length - 1;
    window.postMessage({
      from: "web",
      type: "geolocation_watchposition",
      data: options,
    });
    return options.request_id;
  }
  clearWatch(id) {
    if (
      this.watch_position_requests.length - 1 < id ||
      this.watch_position_requests[id] == null
    )
      return; // No such Request
    window.postMessage({
      from: "web",
      type: "geolocation_clearwatch",
      data: id,
    });
    // Remove the request
    this.watch_position_requests[id] = null;
    // Trim all null entries from the end of the array
    this.watch_position_requests = this.watch_position_requests.slice(
      0,
      this.watch_position_requests.length -
      this.watch_position_requests
        .reverse()
        .findIndex((element) => element != null)
    );
  }

  /**
   * Function to disable display timeout
   * for example while navigation is on
   */
  toggleDisplayTimeout(enable) {
    window.postMessage({
      from: "web",
      type: "display_timeout_toggle",
      data: enable,
    });
  }
}

export const androidConnector = new AndroidConnector();
