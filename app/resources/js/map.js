import { GpsManager } from "./GpsManager";
import { AndroidConnector } from "./AndroidConnector";
import { NavbarControl } from "./NavbarControl";
export class Map {
  map = null; // Integration of the underlying Map (i.e. Openlayers)
  searchModule = null;
  routeModule = null;
  fakegpsModule = null;

  gpsManager = new GpsManager();
  androidConnector = new AndroidConnector();

  navigation_control = null;
  scale_control = new maplibregl.ScaleControl();
  attribution_control = new maplibregl.AttributionControl();

  user_marker = null;

  bbox = null;

  geolocateControl = new maplibregl.GeolocateControl({
    positionOptions: {
      enableHighAccuracy: true,
    },
    trackUserLocation: true,
  });
  navbarControl = new NavbarControl();

  // Map rendering layers
  sourcename = "user-position";
  accuracy_source = null;

  constructor(bbox = null, updateMapPositionOnGps = true) {
    this.bbox = bbox;
    // Remove downloaded areas from mobiles as the mechanic gets reworked
    this.removeDownloadedAreas();

    if (typeof FakeGPSModule == "function") {
      this.fakegpsModule = new FakeGPSModule();
    }
    // Initialize the Map With Controls to change the view
    this.module = null;
  }

  initMap() {
    if (this.isWebglSupported() && 1 == 0) {
      this.map = new maplibregl.Map(map_params);
      this.navigation_control = new maplibregl.NavigationControl();
      this.map.addControl(this.navigation_control, "bottom-left");
      this.map.addControl(this.geolocateControl, "bottom-left");
    } else {
      this.map = L.map("map", { zoomControl: false }).setView(
        map_params.center,
        map_params.zoom
      );
      // Receive Tile information
      fetch("https://tileserver.metager.de/styles/osm-bright.json")
        .then((response) => response.json())
        .then((response) => {
          L.tileLayer(response.tiles[0], {
            minZoom: response.minzoom,
            maxZoom: response.maxzoom,
            attribution: response.attribution,
          }).addTo(this.map);
        });
    }

    //this.map.addControl(this.geolocateControl, "bottom-left");
    //this.map.addControl(this.navbarControl, "top-right");
    //this.map.addControl(this.attribution_control, "bottom-right");
    //this.map.addControl(this.scale_control, "bottom-right");

    // Initialize Modules
    /*
    this.searchModule = new SearchModule();
    this.routeModule = new RouteFinder();
    this.navigationModule = new NavigationModule();*/
    return map;
  }

  updateUserPosition(point, bearing = null, accuracy = 0) {
    this.user_marker.setLngLat(point.geometry.coordinates);
    if (bearing) {
      this.user_marker.setRotation(-90 + bearing);
      // Calculate offset
      let abs_bearing = Math.abs(bearing);
      let x_offset = 0;
      if (abs_bearing / 90 < 1) {
        x_offset = 5 * (1 - abs_bearing / 90);
      } else {
        x_offset = -5 * (abs_bearing / 90 - 1);
      }

      let y_bearing = Math.min(Math.max(bearing / 90, -1), 1);
      let y_offset = y_bearing * 5;
      this.user_marker.setOffset([x_offset, y_offset]);
    } else {
      this.user_marker.setRotation(-90);
      this.user_marker.setOffset([5, 0]);
    }
    if (bearing == null) {
      this.user_marker.remove();
    } else {
      this.user_marker.addTo(this.map);
    }

    // Update accuracy
    if (isNaN(accuracy)) {
      accuracy = 0;
    }
    accuracy = Math.round(accuracy);

    if (accuracy > 0) {
      this.accuracy_source.data.features = [
        turf.circle(point, accuracy / 1000),
      ];
    } else {
      this.accuracy_source.data.features = [];
    }
    map.map
      .getSource(`${this.sourcename}-accuracy`)
      .setData(this.accuracy_source.data);
  }

  showUserPosition(point, bearing = null, accuracy = 0) {
    if (this.user_marker == null) {
      let marker = document.createElement("div");
      marker.id = "user-marker";
      this.user_marker = new maplibregl.Marker({
        element: marker,
        rotationAlignment: "map",
      }).setOffset([5, 0]);
      this.addLayers();
    }
    this.updateUserPosition(point, bearing, accuracy);
  }

  hideUserPosition() {
    if (this.user_marker == null) return;
    this.user_marker.remove();
    this.user_marker = null;
    this.map.removeLayer(`${this.sourcename}-accuracy`);
    this.map.removeSource(`${this.sourcename}-accuracy`);
  }

  fitBbox(bbox, maxZoom = 16) {
    const vw = Math.max(
      document.documentElement.clientWidth || 0,
      window.innerWidth || 0
    );
    let paddingInline = 150;
    if (vw < 800) {
      paddingInline = Math.round(vw / 16);
    }
    map.map.fitBounds(bbox, {
      padding: {
        top: paddingInline,
        bottom: paddingInline,
        right: paddingInline,
        left: paddingInline,
      },
      maxZoom: maxZoom,
      pitch: 0,
    });
  }

  addLayers() {
    this.accuracy_source = {
      type: "geojson",
      data: {
        type: "FeatureCollection",
        features: [],
      },
    };
    let accuracy_layer = {
      id: `${this.sourcename}-accuracy`,
      type: "fill",
      source: `${this.sourcename}-accuracy`,
      paint: {
        "fill-color": "rgb(255,127,0)",
        "fill-opacity": 0.3,
      },
    };
    let addLayers = () => {
      map.map.addSource(`${this.sourcename}-accuracy`, this.accuracy_source);
      map.map.addLayer(accuracy_layer);
    };
    addLayers();
  }

  removeDownloadedAreas() {
    setTimeout(() => {
      /**
       * Removes Downloaded areas as we will not support offline maps for a while
       */
      if (
        typeof android != "undefined" &&
        typeof android.getExtractList == "function" &&
        typeof android.removeExtract == "function"
      ) {
        let extractListJSON = offlineModule.androidConnector.getExtractList();
        let extracts = JSON.parse(extractListJSON);
        for (const extract of extracts) {
          android.removeExtract(extract.name);
        }
      }
    }, 0);
  }
}
