import { LngLat, LngLatBounds } from "maplibre-gl";
import { map } from "./maps/MetaGerMapModule";
import { Result } from "./Result";
import { RenderOptions } from "./maps/RenderOptions";
import {
  bbox,
  bboxPolygon,
  feature,
  featureCollection,
  point,
  polygonToLine
} from "@turf/turf";
import { switchModule } from "./app";
import { Search } from "./Search";
import { routeModule } from "./RouteFinder";
import { config } from "./Config";

class SearchModule {
  htmlmodule = document.getElementById("search-addon");
  searchinput = this.htmlmodule.querySelector(".search-input input[name=q]");
  resultscontainer = this.htmlmodule.querySelector(".results");
  searchresultscontainer =
    this.resultscontainer.querySelector(".results-container");
  expandSearchContainer = this.htmlmodule.querySelector(
    ".bounded-actions > .enlarge"
  );
  worldwideSearchContainer = this.htmlmodule.querySelector(
    ".bounded-actions > .worldwide"
  );
  researchContainer = this.htmlmodule.querySelector(
    ".bounded-actions > .research"
  );

  // Callback functions
  moveend_callback = this.updateState.bind(this);
  search_timeout = null;
  stateupdates = true;
  exit_callback = null;

  query = "";
  viewbox = null;
  results = [];
  bounded = null; // Can be enabled|disabled|auto|null  -> indicating whether bounded search was manually enabled/disabled, automatically enabled or automatically disabled
  reverse = null;

  map_layer_id = "searchmodule"; // Layer ID under which Features are drawn on the map

  markers = [];

  reverseCallback = this.handleReverseClick.bind(this);

  constructor() {
    // Add exit button functionality
    this.htmlmodule.querySelector(".exit-search").onclick = (e) => {
      e.preventDefault();
      e.stopPropagation();
      this.clearResults();
      this.searchinput.value = "";
      this.query = "";
      this.viewbox = null;
      this.bounded = null;
      this.updateInterface();
      this.updateMap();
      this.updateState();
      if (this.exit_callback) {
        this.exit_callback();
      }
    };

    // Add Waypoint Search function
    let typedsearch = () => {
      // Check if input element is now empty
      if (this.searchinput.value.trim().length == 0) {
        this.searchinput.value = "";
        this.clearResults();
        this.query = "";
        this.viewbox = null;
        this.bounded = null;
        this.updateInterface();
        this.updateMap();
        this.updateState();
        return;
      }
      let local_search_timeout = null;
      if (this.search_timeout != null) {
        clearTimeout(this.search_timeout);
      }
      this.search_timeout = setTimeout(() => {
        this.bounded = null;
        this.viewbox = map.getBounds();
        this.search()
          .then(() => {
            if (this.search_timeout != local_search_timeout) {
              return Promise.reject("Another search was already started");
            } else {
              this.search_timeout = null;
              return;
            }
          })
          .then(() => this.updateInterface())
          .then(() => this.updateMap())
          .then(() => this.updateState())
          .catch((error) => {
            if (
              !error.includes("Another search was already started") &&
              !error.includes("Query unchanged")
            ) {
              console.trace(error);
            }
          });
      }, 550);
      local_search_timeout = this.search_timeout;
    };
    this.searchinput.onkeyup = typedsearch;

    this.searchinput.addEventListener("focusin", (e) => {
      this.htmlmodule.classList.add("search");
      this.searchresultscontainer.classList.add("search");
      this.searchinput.select();
      if (this.reverse) {
        this.searchinput.value = "";
      }
    });
    this.searchinput.addEventListener("focusout", (e) => {
      if (this.reverse) {
        this.searchinput.value =
          this.reverse.lngLat.lng + " " + this.reverse.lngLat.lat;
      } else if (this.searchinput.value.trim().length == 0) {
        this.htmlmodule.classList.remove("search");
        this.searchresultscontainer.classList.remove("search");
      }
    });

    this.htmlmodule
      .querySelector("#focus-search")
      .addEventListener("submit", (e) => {
        e.preventDefault();
        this.fitResultsOnMap();
      });

    this.expandSearchContainer.onclick = (e) => {
      e.preventDefault();
      e.stopPropagation();
      let new_zoom = Math.max(map.getZoom() - 1, 1);
      map.once("zoomend", (e) => {
        this.query = ""; // To Force a new search
        this.viewbox = map.map.getBounds();
        return this.search()
          .then(() => this.updateInterface())
          .then(() => this.updateMap())
          .then(() => this.updateState());
      });
      map.map.easeTo({ zoom: new_zoom }, { user: false });
    };
    this.worldwideSearchContainer.onclick = async (e) => {
      e.preventDefault();
      e.stopPropagation();
      this.bounded = "disabled";
      this.query = "";
      return this.search()
        .then(() => this.updateInterface())
        .then(() => this.updateMap())
        .then(() => this.fitResultsOnMap())
        .then(() => this.updateState());
    };
    this.researchContainer.onclick = async (e) => {
      e.preventDefault();
      e.stopPropagation();
      this.query = "";
      this.viewbox = map.map.getBounds();
      return this.search()
        .then(() => this.updateInterface())
        .then(() => this.updateMap())
        .then(() => this.updateState());
    };
  }

  async handleReverseClick(e) {
    if (e.coordinate) {
      e.lngLat = new LngLat(e.coordinate[0], e.coordinate[1]);
    }
    return this.handleReverse({
      center: e.lngLat,
      zoom: Math.round(map.getZoom()),
    })
      .then(() => this.updateInterface())
      .then(() => this.updateMap())
      .then(() => this.updateState());
  }

  /**
   *
   * @param {import("maplibre-gl").CenterZoomBearing} camera
   * @returns
   */
  async handleReverse(camera) {
    return Search.REVERSE(camera.center, camera.zoom).then((result) => {
      if (result == null) return;
      this.clearResults();
      this.results = [result];
      this.reverse = camera;
      this.query = camera.center.lng + " " + camera.center.lat;
      this.viewbox = null;
      this.bounded = null;
      this.searchinput.value = camera.center.lng + " " + camera.center.lat;
    });
  }

  async enable(
    mapposition = null,
    query,
    viewbox,
    results,
    reverse,
    bounded,
    search = false,
    stateupdates = true,
    exit_callback = null
  ) {
    if (query) {
      if (query.trim().length == 0) this.query = query;
      this.searchinput.value = query;
      this.htmlmodule.classList.add("search");
    } else {
      this.searchinput.value = "";
    }
    this.stateupdates = stateupdates;
    this.exit_callback = exit_callback;

    if (results) {
      this.clearResults();
      for (const [index, result] of results.entries()) {
        result.rank = index;
        let new_result = new Result(result, true, result.source);
        await new_result.load();
        this.results.push(new_result);
      }
    }

    if (viewbox) {
      this.viewbox = viewbox;
    }

    this.reverse = reverse;

    if (bounded) {
      this.bounded = bounded;
    }

    let init_promise = Promise.resolve();
    if (query && query.length > 0 && (!results || results.length == 0)) {
      this.htmlmodule.classList.add("active");
      if (!viewbox) this.viewbox = map.getBounds();
      this.bounded = null;
      if (search) {
        // Target is search execution (probably for route finder)
        // searchinput should be focussed
        this.searchinput.focus();
        this.searchinput.selectionStart = this.searchinput.selectionEnd;
        this.searchinput.dispatchEvent(new Event("keyup"));
      } else {
        init_promise = this.search().then(() => { if (!mapposition) this.fitResultsOnMap() });
      }
    } else if (this.reverse && this.results.length == 0) {
      init_promise = this.handleReverse(this.reverse);
    }
    return init_promise
      .then(() => this.updateInterface())
      .then(() => this.updateMap())
      .then(() => {
        this.htmlmodule.classList.add("active");
        if (mapposition) {
          let pos = map.cameraForBounds(mapposition);

          map.easeTo(
            {
              center: pos.center,
              zoom: pos.zoom,
            },
            { user: false }
          );
        }
        this.checkViewbox();
        map.off("click", this.reverseCallback);
        map.on("click", this.reverseCallback);
        map.off("moveend", this.moveend_callback);
        return map.on("moveend", this.moveend_callback);
      });
  }

  async search(key_event) {
    let query = this.searchinput.value;

    if (query.trim() == this.query.trim()) {
      return Promise.reject(`Query unchanged: ${this.query}`);
    } else {
      this.query = query;
    }

    this.htmlmodule.querySelector(".results").classList.add("searching");
    this.reverse = null;
    this.clearResults(); // Removes all results from the local array / and removes the markers on the way

    return Search.SEARCH(query, this.viewbox)
      .then((results) => {
        this.results = results;
      })
      .catch((error) => {
        this.results = [];
        return true;
      })
      .finally(() => {
        this.htmlmodule.querySelector(".results").classList.remove("searching");
        return true;
      });
  }

  clearResults() {
    map.removeFeatures(this.map_layer_id);
    map.removeFeatures(this.map_layer_id + "_point");
    this.results = [];
    this.reverse = null;
  }

  /**
   * Shows all results stored in this object
   *
   */
  updateInterface() {
    if (this.searchinput.value.length > 0) {
      this.htmlmodule.classList.add("search");
    } else if (document.activeElement != this.searchinput) {
      this.htmlmodule.classList.remove("search");
    }

    if (["auto", "enabled"].includes(this.bounded)) {
      this.resultscontainer.classList.add("bounded");
    } else {
      this.resultscontainer.classList.remove("bounded");
    }

    if (this.results.length == 0 && this.query.length > 0) {
      this.resultscontainer.classList.add("no-results");
    } else {
      this.resultscontainer.classList.remove("no-results");
    }

    this.searchresultscontainer.textContent = "";

    if (this.results.length > 0) {
      let label = document.createElement("label");
      label.textContent = "Suchergebnisse:";
      this.searchresultscontainer.appendChild(label);
    }
    for (const [index, result] of this.results.entries()) {
      let add_waypoint_button =
        result.dom_element.querySelector(".add-waypoint");
      if (add_waypoint_button) {
        if (routeModule.waypoints.length > 0) {
          add_waypoint_button.textContent = "+ Wegpunkt hinzufügen";
        } else {
          add_waypoint_button.textContent = "Route berechnen";
        }
      }

      result.addMarkerToDom(index);

      // Add result container
      this.searchresultscontainer.appendChild(result.dom_element);

      // Add Click listener
      result.dom_element.onclick = (e) => {
        let paddingInline = 250;
        if (window.screen.width < 800) {
          paddingInline = 100;
        }
        map.fitBounds(new LngLatBounds(result.bbox), {
          duration: 350,
          padding: {
            top: paddingInline,
            bottom: paddingInline,
            right: paddingInline,
            left: paddingInline,
          },
          maxZoom: 16,
        });
      };
      result.dom_element.querySelector(".open-details").onclick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.searchinput.value = result.id;
        this.bounded = null;
        this.viewbox = map.getBounds();
        this.search()
          .then(() => this.updateInterface())
          .then(() => this.updateMap())
          .then(() => this.updateState());
      };
      result.dom_element.querySelector(".add-waypoint").onclick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        let new_result = new Result(result.load_data, false, result.source);
        new_result.load().then(() => {
          switchModule("route-finding", {
            waypoints: [new_result],
          });
        });
      };
    }

    return Promise.resolve();
  }

  updateMap() {
    map.removeFeatures(this.map_layer_id);

    // Print Geojson Objects on the Map
    let geojson_features = [];
    let geojson_point_features = [];
    for (const [index, result] of this.results.entries()) {
      if (!result.loaded) continue;
      let chr = String.fromCharCode(65 + index);
      let new_feature = feature(result.geojson, { text: chr });
      if (!["Point", "MultiPoint"].includes(new_feature.geometry.type)) {
        // Add A marker
        let new_point = point(result.location.toArray(), { text: chr });
        geojson_point_features.push(new_point);
        geojson_features.push(new_feature);
      } else {
        geojson_point_features.push(new_feature);
      }
    }
    // Render styles go here
    let render_options = new RenderOptions(
      {
        point: {
          "text-translate": [0, -19],
          "text-color": "#ffffff",
        },
        line: {
          "line-color": "rgb(255,127,0)",
          "line-width": 2,
        },
        fill: {
          "fill-color": "#593008",
          "fill-outline-color": "rgb(255,127,0)",
          "fill-opacity": 0.3,
        },
      },
      {
        point: {
          "icon-image": "result-marker",
          "icon-size": 1,
          "icon-anchor": "bottom",
          "icon-ignore-placement": true,
          "text-field": ["get", "text"],
          "text-line-height": 1,
          "text-padding": 0,
          "text-anchor": "bottom",
          "text-justify": "center",
          "text-ignore-placement": true,
        },
      },
      true
    );
    map.drawFeatures(
      this.map_layer_id,
      featureCollection(geojson_features),
      render_options,
      false
    );

    map.drawFeatures(
      this.map_layer_id + "_point",
      featureCollection(geojson_point_features),
      render_options,
      true
    );
  }

  fitResultsOnMap(all_results = false) {
    if (this.results.length == 0) {
      return;
    }
    let bbox_polygon = null;
    for (const result of this.results) {
      if (bbox_polygon == null) {
        bbox_polygon = bboxPolygon(result.bbox);
      } else {
        let result_point = point(result.location.toArray());
        let distance = turf.pointToLineDistance(
          result_point,
          polygonToLine(bbox_polygon)
        );
        if (distance < 100) {
          bbox_polygon = bboxPolygon(
            bbox(featureCollection([bbox_polygon, bboxPolygon(result.bbox)]))
          );
        }
      }
    }
    let bbox_array = bbox(bbox_polygon);
    const vw = Math.max(
      document.documentElement.clientWidth || 0,
      window.innerWidth || 0
    );
    let paddingInline = 250;
    if (vw < 800) {
      paddingInline = Math.round(vw / 10);
    }
    map.fitBounds(new LngLatBounds(bbox_array), {
      duration: 0,
      padding: {
        top: paddingInline,
        bottom: paddingInline,
        right: paddingInline,
        left: paddingInline,
      },
      maxZoom: 16,
    });
  }

  addMapLayers() {
    // Variables for map features
    let lineSource = {
      type: "geojson",
      data: {
        type: "FeatureCollection",
        features: [],
      },
    };
    let lineLayer = {
      id: `${this.linesourcename}`,
      type: "line",
      source: `${this.linesourcename}`,
      paint: {
        "line-color": "rgb(255,127,0)",
        "line-width": 5,
        "line-opacity": 0.6,
      },
    };

    let fillSource = {
      type: "geojson",
      data: {
        type: "FeatureCollection",
        features: [],
      },
    };
    let fillLayer = {
      id: `${this.fillsourcename}`,
      type: "fill",
      source: `${this.fillsourcename}`,
      paint: {
        "fill-color": "rgb(255,127,0)",
        "fill-outline-color": "rgb(255,127,0)",
        "fill-opacity": 0.3,
      },
    };

    let addLayers = () => {
      map.map.addSource(this.linesourcename, lineSource);
      map.map.addLayer(lineLayer);
      map.map.addSource(this.fillsourcename, fillSource);
      map.map.addLayer(fillLayer);
    };
    if (map.map.isStyleLoaded()) {
      addLayers();
    } else {
      map.map.on("load", addLayers);
    }
  }

  /**
   * Function to tell if the viewbox is close enough to be the same as the current view
   * */
  checkViewbox() {
    if (this.viewbox) {
      let viewboxCamera = map.cameraForBounds(this.viewbox);
      if (
        viewboxCamera.center.distanceTo(map.getCenter()) > 250 ||
        Math.abs(viewboxCamera.zoom - map.getZoom()) >= 0.5
      ) {
        this.htmlmodule
          .querySelector(".bounded-actions")
          .classList.add("research");
        return true;
      } else {
        this.htmlmodule
          .querySelector(".bounded-actions")
          .classList.remove("research");
        return false;
      }
    }
    return false;
  }
  /**
   * Function called when map is loaded and GPS Features become available
   */
  enableGps() { }

  /**
   * Cleanup Code when module is closed. Unregister callbacks etc.
   */
  exit() {
    this.clearResults();
    this.htmlmodule.classList.remove("active");
    map.off("click", this.reverseCallback);
    map.off("moveend", this.moveend_callback);
  }

  updateState(event) {
    if (
      event &&
      ((event.hasOwnProperty("user") && event.user == false) ||
        (event.hasOwnProperty("0") &&
          ResizeObserverEntry.prototype.isPrototypeOf(event[0])))
    ) {
      return;
    }

    let current_pos = map.getBounds();

    this.checkViewbox();

    if (!this.stateupdates) {
      return;
    }

    // Check if current_pos changed enough
    if (
      window.history &&
      window.history.state &&
      window.history.state.module == "search"
    ) {
      let old_pos = new LngLatBounds(
        window.history.state.mapposition._sw,
        window.history.state.mapposition._ne
      );
      let old_pos_camera = map.cameraForBounds(old_pos);
      let new_pos_camera = map.cameraForBounds(current_pos);
      if (
        old_pos_camera.center.distanceTo(new_pos_camera.center) <= 250 &&
        Math.abs(old_pos_camera.zoom - new_pos_camera.zoom) < 0.5
      ) {
        current_pos = old_pos;
      }
    }

    let url = "";
    if (this.reverse == null) {
      let query =
        this.query.length > 0 ? `${encodeURIComponent(this.query)}/` : "";

      let viewbox = this.viewbox
        ? `${JSON.stringify(this.viewbox.toArray())}/`
        : "";

      url =
        window.location.origin +
        `/${config.localization.current_locale}/${query}${viewbox}${JSON.stringify(current_pos.toArray())}`;
    } else {
      url = `/${config.localization.current_locale}/reverse/${this.reverse.center.lng},${this.reverse.center.lat},${this.reverse.zoom
        }/${JSON.stringify(current_pos.toArray())}`;
    }

    let results = [];
    for (const result of this.results) {
      results.push({ source: result.source, ...result.load_data });
    }

    if (["enabled", "disabled"].includes(this.bounded)) {
      url += "?bounded=" + this.bounded;
    }

    let state = {
      module: "search",
      mapposition: current_pos,
      query: this.query,
      viewbox: this.viewbox,
      results: results,
      reverse: this.reverse,
      bounded: this.bounded,
      timestamp: Date.now(),
    };

    if (
      window.history.state == undefined ||
      window.location.href == url ||
      Date.now() - window.history.state.timestamp < 100
    ) {
      window.history.replaceState(state, "", url);
    } else {
      window.history.pushState(state, "", url);
    }
  }
}

export const searchModule = new SearchModule();
