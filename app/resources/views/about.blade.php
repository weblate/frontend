<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport"/>
    <title>
        Maps - MetaGer
    </title>
    <style>
        @font-face {
            font-family: "Liberation Sans";
            src: url(/fonts/LiberationSans-Regular.ttf);
        }

        * {
            font-family: "Liberation Sans", Verdana, Tahoma;
        }

        .panel-heading {
            font-weight: bold;
            font-size: 120%;
        }

        .panel-heading.big-header {
            font-size: 180%;
            padding: 7px 15px;
            margin: 15px 0;
            font-weight: normal;
            background-color: #ff9517;
            color: white;
            border-radius: 20px;
        }

        .panel-heading:not(.big-header), .panel-body {
            padding: 0px 15px;
        }

        .content {
            max-widtH: 900px;
            margin: 0 auto;
            padding: 20px;
        }

        .panel {
            line-height: 1.4;
        }

        @media (max-width: 900px) {
            body {
                margin: 0;
            }

            .content {
                padding: 0;
            }

            .panel-heading.big-header {
                border-radius: 0;
            }

            ul {
                padding-left: 20px;
            }
        }
    </style>
</head>
<body>
<div class="content">
    <div class="panel-heading big-header">MetaGer Maps</div>
    <div class="panel">
        <div class="panel-body">
            <p>MetaGer Maps ist ein <a href="https://gitlab.metager3.de/open-source/MetaGerMaps" target="_blank">freier Kartendienst</a> basierend auf <a href="https://www.openstreetmap.org" target="_blank">Openstreetmap</a>. </p>
            <p>Wir versuchen die Vielfalt an Kartendaten so anzubieten, dass Sie für jedermann auf allen Endgeräten zur Verfügung stehen kann.</p>
            <p>Um dies zu erreichen verwenden wir folgendene Softwareprojekte in Kombination mit unserem eigenen <a href="https://gitlab.metager3.de/open-source/MetaGerMaps" target="_blank">Quellcode</a>:</p>
            <ul>
                <li><a href="https://www.openstreetmap.org" target="_blank">Openstreetmap</a> - als Datengrundlage</li>
                <li><a href="https://www.maptiler.com/copyright/" target="_blank">Maptiler</a> - um die Karte zu rendern.</li>
                <li><a href="https://nominatim.org/" target="_blank">Nominatim</a> - um die Daten durchsuchbar zu machen</li>
                <li><a href="http://project-osrm.org/" target="_blank">OSRM</a> - um Routen zu berechnen</li>
            </ul>
            <p>Selbstverständlich versuchen wir die Daten nach Möglichkeit aktuell zu halten. Nachfolgend finden Sie das letzte Aktualisierungsdatum der Kartendaten:</p>
            <ul>
                <li>Maptiler: {{ $lmTiles->format('d.m.Y') }}</li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>
