<!DOCTYPE html>
<html lang="{{ App::currentLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport" />
    <meta name="color-scheme" content="light dark">
    <title>
        Maps - MetaGer
    </title>
    @if (isset($css))
        @foreach ($css as $el)
            <link href="{{ $el }}" rel="stylesheet" type="text/css" />
        @endforeach
    @endif
    <meta name="tileserverhost" content="{{ config('maps.tileserver.host') }}">
    <meta name="supported-locales"
        content="{{ base64_encode(json_encode(App\Http\Middleware\Localization::SUPPORTED_LOCALES)) }}">
    @if(isset($geoip))
        <meta name="start-geoip" content="{{ json_encode($geoip) }}">
    @endif
    @if (isset($bbox))
        <meta name="start-bbox" content="{{ $bbox }}">
    @endif
    @if (isset($module))
        <meta name="module" content="{{ $module }}">
    @endif
    @if (isset($query))
        <meta name="start-query" content="{{ $query }}">
    @endif
    @if (isset($viewbox))
        <meta name="start-viewbox" content="{{ $viewbox }}">
    @endif
    @if (isset($reverse_lon) && isset($reverse_lat) && isset($reverse_zoom))
        <meta name="start-reverselon" content="{{ $reverse_lon }}">
        <meta name="start-reverselat" content="{{ $reverse_lat }}">
        <meta name="start-reversezoom" content="{{ $reverse_zoom }}">
    @endif
    @if (isset($waypoints) && isset($vehicle))
        <meta name="start-vehicle" content="{{ $vehicle }}">
        <meta name="start-waypoints" content="{{ $waypoints }}">
    @endif
    @if (Request::filled('bounded') && in_array(Request::input('bounded'), ['enabled', 'disabled']))
        <meta name="start-bounded" content="{{ Request::input('bounded') }}">
    @endif
    @if (isset($navigation_active) && $navigation_active === true)
        <meta name="navigation_active" content="true">
    @endif

    <link rel="stylesheet" href="{{ mix('/css/map.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/result.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/nav-arrows.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/navbar.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ mix('/css/addons.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/searchaddon.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/routefinder.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/navigation.css') }}" rel="stylesheet" type="text/css">
    @if (!\App::environment('production'))
        <link rel="stylesheet" href="{{ mix('/css/fakegps.css') }}" type="text/css">
    @endif
</head>

<body>
    <div id="page">
        <div class="close-container"><button class="close" aria-label="@lang('close sub page')"
                title="@lang('close sub page')">❌</button></div>
        <iframe src="" frameborder="0"></iframe>
    </div>
    <main>

        @include('addons/search')
        @include('addons/navigation')
        @include('addons/route')
        @include('addons/offline')
        @if (!\App::environment('production'))
            @include('addons/fakegps')
        @endif

        <div class="map" id="map" tabindex="0">
        </div>
        <div id="gps-error" class="hidden">
            <span>@lang('MetaGer could not find your exact location')</span> <a href="/hilfe"
                target="_blank">@lang('why?')</a>
        </div>
    </main>
    <div id="nav-menu">
        <input type="checkbox" name="nav-opener" id="nav-opener">
        <label for="nav-opener">≡</label>
        <nav>
            <div class="logo"><img src="/img/metager.svg" alt="MetaGer Logo"></div>
            <ul class="nav-items">
                <li><a href="/hilfe">@lang('help')</a></li>
                <li><a href="https://metager.de/datenschutz" target="_blank">@lang('privacy')</a></li>
                <li><a href="https://metager.de/impressum" target="_blank">@lang('impress')</a></li>
                @if (!\App::environment('production'))
                    <li><a href="#" id="nav-fakegps">Fake GPS</a></li>
                @endif
                <li id="theme-switch" class="hidden">
                    <a id="theme-dark" href="#">@lang('dark mode')</a>
                    <a id="theme-light" href="#">@lang('light mode')</a>
                </li>
            </ul>
            <div class="updates">
                <label>@lang('last openstreetmap update')</label>
                <div>@lang('timezone'): <span class="data-date-timezone"></span></div>
                <div>@lang('map'): <span class="data-date-tiles"><img src="/img/ajax-loader.gif" alt="@lang('loading data...')"></span>
                </div>
                <div>@lang('search'): <span class="data-date-search"><img src="/img/ajax-loader.gif" alt="@lang('loading data...')"></span>
                </div>
                <div>@lang('routes'): <span class="data-date-routing"><img src="/img/ajax-loader.gif"
                            alt="@lang('loading data...')"></span></div>
            </div>
        </nav>
    </div>
    <script src="/js/turf.min.js" type="text/javascript" defer></script>
    <script src="{{ mix('/js/modules.js') }}" type="text/javascript" defer></script>
    <script src="{{ mix('/js/lib.js') }}" type="text/javascript" defer></script>
    <script src="{{ mix('js/map.js') }}" type="text/javascript" defer></script>
</body>

</html>