<figure id="fakegps-addon" class="addon">
    <div class="exit"><button type="button" class="close" aria-label="Close" title="Fake GPS beenden">
            &#8676;
        </button></div>
    <div class="profile-choser">
        <label for="">Fortbewegungsmittel</label>
        <div class="input-group">
            <input type="radio" name="profile" id="car" value="car" checked="checked">
            <label for="car">Auto</label>
        </div>
        <div class="input-group">
            <input type="radio" name="profile" id="bike" value="bike">
            <label for="bike">Fahrrad</label>
        </div>
        <div class="input-group">
            <input type="radio" name="profile" id="foot" value="foot">
            <label for="foot">Fußgänger</label>
        </div>
    </div>
    <label for="speed">Genauigkeit der Position (m)</label>
    <input type="number" name="accuracy" id="accuracy" value="5">
    <label for="speed">Geschwindigkeit (km/h)</label>
    <input type="number" name="speed" id="speed">
    <div class="enable-updates">
        <input type="checkbox" name="enable-updates" id="enable-updates" checked>
        <label for="enable-updates">GPS Position übertragen</label>
    </div>
</figure>
