<figure id="route-finder-addon" class="addon">
    <div id="vehicle-chooser">
        <label class="radio-inline" title="Fußgänger">
            <input type="radio" name="vehicle" value="foot">
            <div><img src="/img/silhouette-walk.png" height="20px" /></div>
        </label>
        <label class="radio-inline" title="Fahrrad">
            <input type="radio" name="vehicle" value="bike">
            <div><img src="/img/bike.png" height="20px" /></div>
        </label>
        <label class="radio-inline" title="Auto">
            <input type="radio" name="vehicle" value="car">
            <div><img src="/img/car.png" height="20px" /></div>
        </label>
        <button type="button" class="btn btn-sm btn-success start-navigation inactive">@lang('start navigation')</button>
        <button type="button" class="close" aria-label="Close" title="@lang('exit route finder')">
            &#8676;
        </button>
    </div>
    <input type="text" name="search-waypoint" id="search-waypoint-input" placeholder="@lang('search another waypoint...')">
    <button id="start-navigation">@lang('start navigation')</button>
    <div class="total-info">
        <img src="/img/lupe.svg" alt="@lang('focus route on map')" class="focus">
        <div class="duration"></div>
        <div class="distance"></div>
    </div>
    <div id="waypoint-list-container">

    </div>
</figure>
