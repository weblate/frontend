<figure id="navigation" class="addon">
    <div id="recalculating"><img src="/img/ajax-loader.gif" alt="@lang('loading...')"><div>@lang('route is being calculated')</div></div>
    <div id="gps-lost"><img src="/img/ajax-loader.gif" alt="Loading image"><div>@lang('gps position not available')</div></div>

    <div id="next-step">
        <div class="direction">
        </div>
        <div class="instruction">
        </div>
        <div class="distance">
        </div>
    </div>
    <div id="additional-step">
        <div class="direction">
        </div>
        <div class="instruction">
        </div>
        <div class="distance">
        </div>
    </div>
    <div id="general-information">
        <div class="time"></div>
        <div class="distance"></div>
        <div class="arrival"></div>
        <div class="exit" title="@lang('exit navigation')">&#8676;</div>
    </div>
    <button id="finish-navigation">@lang('exit navigation')</button>
    <button id="next-waypoint">@lang('to the next waypoint')</button>
    <button id="center-position">@lang('lock position')</button>
</figure>
