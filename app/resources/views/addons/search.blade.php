<figure id="search-addon" class="addon">
    <div class="search-input">
        <div class="logo">
            <img src="/img/metager-schloss-weiß.svg" alt="MetaGer Logo">
        </div>
        <form action="#" id="focus-search">
            <input class="" name="q" placeholder="@lang('search maps...')" type="text" autocomplete="off"
                form="focus-search" value="@if (isset($search)) {{ urldecode($search) }} @endif">
            <button type="submit" form="focus-search" class="focus-search"><img src="/img/lupe.svg"
                    alt="@lang('search')" /></button>
        </form>
        <a href="#" class="exit-search" title="@lang('exit search')">⇤</a>
    </div>
    <div class="results">
        <div class="container-fluid wait-for-search">
            <img src="/img/ajax-loader.gif" alt="loading..." id="loading-search-results" />
            <div>@lang("results are loading")</div>
        </div>
        <div class="no-results">
            <div class="text bounded">@lang('no results were found in the current map view')</div>
            <div class="text unbounded">@lang('no results could be found!')</div>
        </div>
        <div class="bounded-actions">
            <a href="#" class="button enlarge">@lang('enlarge search area')</a>
            <a href="#" class="button research">@lang('search again in this area')</a>
            <a href="#" class="button worldwide">@lang('search worldwide')</a>
        </div>
        <div class="results-container">

        </div>

    </div>
</figure>