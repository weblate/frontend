const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

var mapJsFiles = [
  "resources/js/app.js",
];
if (!mix.inProduction()) {
  mapJsFiles.push("resources/js/FakeGPSModule.js");
}

mix
  .js("resources/js/modules.js", "public/js")
  .babel(
    [
      "resources/js/md5.js",
      "resources/js/jquery.min.js",
      "resources/js/jquery-ui.min.js",
      "resources/js/jquery.ui.touch-punch.min.js",
      "resources/js/bootstrap.min.js",
    ],
    "public/js/lib.js"
  );
mix.js("resources/js/static.js", "public/js");
mix.js(mapJsFiles, "public/js/map.js");
mix.js("resources/js/mapFrame.js", "public/js/mapFrame.js");

mix.scripts(["resources/js/turf.min.js"], "public/js/turf.min.js");
mix.less("resources/less/map.less", "public/css/map.css");
mix.less("resources/less/navbar.less", "public/css/navbar.css");
mix.less("resources/less/result.less", "public/css/result.css");
mix.less("resources/less/nav-arrows.less", "public/css/nav-arrows.css");
mix.less("resources/less/addons.less", "public/css/addons.css");
mix.less("resources/less/searchaddon.less", "public/css/searchaddon.css");
mix.less("resources/less/routefinder.less", "public/css/routefinder.css");
mix.less("resources/less/navigation.less", "public/css/navigation.css");
if (!mix.inProduction()) {
  mix.less("resources/less/fakegps.less", "public/css/fakegps.css");
}
mix
  .less(
    "resources/less/staticPages.less",
    "public/css/staticPages.css"
  )
  .sass(
    "resources/sass/offline-module.scss",
    "../resources/css/offline-module.css"
  )
  .less("resources/less/general.less", "public/css/general.css")
  .styles(["resources/css/mapSearch.css"], "public/css/mapSearch.css")
  .styles(
    [
      "resources/css/bootstrap.min.css",
      "resources/css/style.css",
      "resources/css/iframeSearch.css",
    ],
    "public/css/iframeSearch.css"
  )
  .styles(["resources/css/routing.css"], "public/css/routing.css")
  .version();