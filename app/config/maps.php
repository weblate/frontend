<?php

return [
    "nominatim" => [
        "host" => env("NOMINATIM_HOST", ""),
    ],
    "photon" => [
        "host" => env("PHOTON_HOST", "")
    ],
    "tileserver" => [
        "host" => env("TILESERVER_HOST", ""),
    ],
    "routingserver" => [
        "host" => env("GRAPHHOPPER_HOST", ""),
    ],
    "maxmind" => [
        "enabled" => env("MAXMIND_ENABLED", false),
        "update_interval_days" => 7,
        "update_url" => env("MAXMIND_UPDATE_URL"),  // Update URL for ZIP Archive
        "account_id" => env("MAXMIND_ACCOUNT_ID"),
        "license_key" => env("MAXMIND_LICENSE_KEY")
    ]
];