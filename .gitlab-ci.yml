variables:
  DOCKER_HOST: "tcp://docker-dind.gitlab-suma:2375"
  BUILD_DOCKER_IMAGE: docker:25.0.3
  DEPLOY_KUBERNETES_IMAGE: alpine/k8s:1.27.8
  KUBE_NAMESPACE: maps
  IMAGE_TAG_PREFIX: master
  FPM_HOST: localhost:9000

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      variables:
        APP_URL: https://maps.metager.de
        APP_HOST: maps.metager.de
        HELM_VALUES_FILE: .gitlab/master.yaml
        ENVIRONMENT: production
        IMAGE_NAME: $CI_REGISTRY_IMAGE
        IMAGE_TAG: $IMAGE_TAG_PREFIX-$CI_COMMIT_SHA
        HELM_RELEASE_NAME: $DOCKER_IMAGE_TAG_PREFIX

stages:
  - assets
  - build
  - deploy

build_asset_images:
  image: $BUILD_DOCKER_IMAGE
  stage: assets
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker compose build composer assets
    - docker compose push composer assets
  after_script:
    - docker logout $CI_REGISTRY
    - source .env
    - echo "COMPOSER_VERSION=$COMPOSER_VERSION" > .composerversion.env
    - echo "NODE_TAG=$NODE_TAG" >> .composerversion.env
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  artifacts:
    reports:
      dotenv: .composerversion.env

composer_install:
  image: $IMAGE_NAME/composer:$COMPOSER_VERSION # Composer version variable was defined in build artifact dotenv from previous job
  stage: assets
  before_script:
    - export COMPOSER_HOME=$(pwd)/app/.composer
    - mkdir app/bootstrap/cache # Create missing boostrap/cache directory
  script:
    - cd app
    - composer install --no-dev
  needs:
    - build_asset_images
  artifacts:
    paths:
      - app/vendor
      - app/bootstrap/cache
    expire_in: 2 hrs
  cache:
    paths:
      - app/.composer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

npm_install:
  image: $IMAGE_NAME/node:$NODE_TAG # Node Tag variable was defined in build artifact dotenv from previous job
  stage: assets
  script:
    - cd app
    - npm install --cache .npm --prefer-offline --no-audit
    - npm run prod
  needs:
    - build_asset_images
  artifacts:
    paths:
      - app/public
    expire_in: 2 hrs
  cache:
    paths:
      - app/.npm
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

build_images:
  image: $BUILD_DOCKER_IMAGE
  stage: build
  needs:
    - npm_install
    - composer_install
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker compose build nginx fpm redis
    - docker compose push nginx fpm redis
  after_script:
    - docker logout $CI_REGISTRY
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

deploy:
  stage: deploy
  image: $DEPLOY_KUBERNETES_IMAGE
  variables:
    DEPLOYMENT_CHART_NAME: ${CI_COMMIT_REF_SLUG}
  before_script:
    - kubectl config use-context $CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:maps-deployment
  script:
    - kubectl -n $KUBE_NAMESPACE create secret generic $DEPLOYMENT_CHART_NAME --from-file=.env=$ENV_PRODUCTION --dry-run=client --save-config -o yaml | kubectl apply -f -
    - helm -n $KUBE_NAMESPACE upgrade --install $DEPLOYMENT_CHART_NAME maps/ -f $HELM_VALUES_FILE --set nginx.image.repository=$CI_REGISTRY_IMAGE/nginx --set secretName=$DEPLOYMENT_CHART_NAME --set nginx.image.tag=$IMAGE_TAG --set fpm.image.repository=$CI_REGISTRY_IMAGE/fpm --set fpm.image.tag=$IMAGE_TAG --set redis.image.repository=$CI_REGISTRY_IMAGE/redis --set redis.image.tag=$IMAGE_TAG
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
